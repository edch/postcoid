<style>
	.bgform{background:#156492;padding-bottom:10px;width:215px;}
	.fontinput1{
		font:13px CenturyGothicRegular, tahoma;
		width:130px; height:20px; background:transparent; border:1px solid #fff; color:#ffffff;
	}
	.fontinput2{margin:10px 0 10px 15px;font:13px CenturyGothicRegular, tahoma; width:170px; height:20px; background:transparent; border:1px solid #fff; color:#ffffff;}
	.fontinput3{margin:10px 0 10px 15px;width:179px;height:79px;}
	.spacespan{padding-left:10px;}
	.spacespan2{padding-left:14px;}
	.typenum{color:#fff;font:13px CenturyGothicRegular, tahoma;padding:10px 0 0 15px;}
	.textforgot{padding:10px 0 0 10px;font:12px CenturyGothicRegular, tahoma;}
	.link{color:#fff;border:0;cursor:pointer;}
	.msgerr{padding:15px 0 0 10px;font:11px CenturyGothicRegular, tahoma;color:#f7941d;font-weight:bold;text-transform:uppercase;}
</style>
<script type="text/javascript">
function doSubmit()
{
	var _password = document.getElementById("password").value;
	var _token = document.getElementById("j_token").value;
	document.getElementById("j_password").value = "<%= session.getId() %>#" + _token + "#<%= request.getRemoteAddr() %>#<%= request.getRemotePort() %>#" + _password;
	return true;
}
</script>
<form class="bgform" method="POST" action="j_security_check" onSubmit="return doSubmit();" target="_parent">
	<div style="padding-top:10px;">
		<span class="spacespan"><img src="icon_1.png"></span>
		<span class="spacespan"><input type="text" class="fontinput1" placeholder="Username" name="j_username" id="j_username"></span><br>
		<span class="spacespan"><img src="icon_2.png"></span>
		<span class="spacespan2"><input type="password" class="fontinput1" placeholder="Password" name="password" id="password"></span>
	</div>
	<div class="typenum">
		Type the number below
	</div>
	<div><input type="text" class="fontinput2" name="j_token" id="j_token"></div>
	<div class="fontinput3">
		<img src="./publicservlets/AuthToken" width="178" height="50">
		<input type="hidden" name="j_password" id="j_password" value="">
	</div>
	<div style="margin:0 0 0 45px;"><input type="image" src="login_button.png"></div>
	<div class="msgerr"><%= com.piwi.web.login.OnlineTradingMessage.getMessage ( session.getId() ) %></div>
	<div class="textforgot">
		<a href="/root/public/forget.faces" class="link">Forgot your passwords?</a><br>
		<a href="#" class="link">Read the Terms & Condition</a>
	</div>
</form>
