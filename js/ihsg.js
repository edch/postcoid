var _ihsgPrev = 0;
var ihsgHandler = 
{
	_ihsgReceived: function(message)
	{
		if (message != null)
		{
			// console.log ( message );
			try
			{
				var _stock = message.getAttribute ('_c').toUpperCase();
				if ( _stock != 'COMPOSITE' ) return;


				var _t = writeAttrHtml (message, '_p', '_ihsg_prev', 3, 'T1_col_2');
				if (_t) _ihsgPrev=_t;

				var _t = message.getAttribute ('_l');
				if (_t)
				{
					var _last = parseFloat(_t);
					var _col = getColor(_ihsgPrev,_last);

					if ( _last > 0 )
					{
						writeIdHtml ('_ihsg_ihsg', _last, 3, _col);
						writeIdHtml ('_ihsg_chg', (_last-_ihsgPrev), 3, _col);
						writeIdHtml ('_ihsg_persenchg', ((_last-_ihsgPrev)*100/_ihsgPrev), 3, _col);

						if ( parseFloat(_ihsgPrev) > _last )
						{
							//alert ('Prev > [' + _ihsgPrev + ']');
							//alert ('Last > [' + _last + ']');
							document.getElementById('_ihsg_img').src = '../images/down.png';
						}
						else
						{
							//alert ('Prev < [' + _ihsgPrev + ']');
							//alert ('Last < [' + _last + ']');
							document.getElementById('_ihsg_img').src = '../images/up.png';
						}
					}
					else
					{
						writeIdHtml ('_ihsg_ihsg', _ihsgPrev, 3, _col);
						writeIdHtmlString ('_ihsg_chg', '0.000', _col);
						writeIdHtmlString ('_ihsg_persenchg', '0.000', _col);
					}
				}

				_t = message.getAttribute ('_o');
				if (_t) writeIdHtml ('_ihsg_open', _t, 3, getColor(_ihsgPrev,_t));
				_t = message.getAttribute ('_hi');
				if (_t) writeIdHtml ('_ihsg_high', _t, 3, getColor(_ihsgPrev,_t));
				_t = message.getAttribute ('_lo');
				if (_t) writeIdHtml ('_ihsg_low', _t, 3, getColor(_ihsgPrev,_t));

				var _infos = message.getAttribute('_i');
				if (_infos)
				{
					var _info = _infos.split('|');
					//writeIdHtml ('_ihsg_up', _info[0], 0, 'T1_col_3');
					//writeIdHtml ('_ihsg_down', _info[1], 0, 'T1_col_1');
					//writeIdHtml ('_ihsg_unchange', _info[2], 0, 'T1_col_2');
					//writeIdHtml ('_ihsg_untrade', _info[3], 0, 'T1_col_2');
					//writeIdHtml ('_ihsg_suspend', _info[4], 0, 'T1_col_2');
				}

				var _infos = message.getAttribute('_to');
				if (_infos)
				{
					var _info = _infos.split('|');
					writeIdHtml ('_ihsg_tfreq', _info[0], 0, 'T1_col_2');
					writeIdHtml ('_ihsg_tvol', _info[1], 0, 'T1_col_2');

					var _value = parseFloat(_info[2]);
					if (_value<1000000)
					{
						writeIdHtmlString ('_ihsg_tval', (_value/1000).formatMoney(2) + ' K', 'T1_col_2');
					}
					else if (_value<1000000000)
					{
						writeIdHtmlString ('_ihsg_tval', (_value/1000000).formatMoney(2) + ' M', 'T1_col_2');
					}
					else if (_value<1000000000000)
					{
						writeIdHtmlString ('_ihsg_tval', (_value/1000000000).formatMoney(2) + ' B', 'T1_col_2');
					}
					else
					{
						writeIdHtmlString ('_ihsg_tval', (_value/1000000000000).formatMoney(2) + ' T', 'T1_col_2');
					}
				}

				var _infos = message.getAttribute('_tf');
				if (_infos)
				{
					var _info = _infos.split('|');
					// writeIdHtml ('_ihsg_ffreq', _info[0], 0, 'T1_col_2');
					writeIdHtml ('_ihsg_fvol', _info[1], 0, 'T1_col_2');

					var _value = parseFloat(_info[2]);
					if (_value<1000000)
					{
						writeIdHtmlString ('_ihsg_fval', (_value/1000).formatMoney(2) + ' K', 'T1_col_2');
					}
					else if (_value<1000000000)
					{
						writeIdHtmlString ('_ihsg_fval', (_value/1000000).formatMoney(2) + ' M', 'T1_col_2');
					}
					else if (_value<1000000000000)
					{
						writeIdHtmlString ('_ihsg_fval', (_value/1000000000).formatMoney(2) + ' B', 'T1_col_2');
					}
					else
					{
						writeIdHtmlString ('_ihsg_fval', (_value/1000000000000).formatMoney(2) + ' T', 'T1_col_2');
					}
				}
			}
			catch (err)
			{
			}
		}
	},
	startIhsg: function()
	{
		try
		{
			amq.addListener('in', 'topic://INDICES.COMPOSITE', ihsgHandler._ihsgReceived);
			amq.sendMessage('topic://REQUEST.FIRST', '<c s=COMPOSITE>');
		}
		catch (err){}
	},
	stopIhsg: function()
	{
		amq.removeListener ('in', 'topic://INDICES.COMPOSITE');
	}
};
function unloadIhsg()
{
	amq.removeListener ('in', 'topic://INDICES.COMPOSITE');
}
