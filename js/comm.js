var APP_CONTEXT_PATH = '/onlineTrading';

var amq =
{
	MAP: {},

	addListener: function ( header, topic, func )
	{
		// alert ('addlistener start');

		var xml = '_packet=<register>';
		xml += '<topic>' + topic + '</topic>';
		xml += '</register>';
		amq.doSendPacket(xml);

		// console.log ('addlistener ' + header + ' ' + func);
		amq.MAP [ header ] = func;
	},
	removeListener: function ( header, topic )
	{
		var xml = '_packet=<unregister>';
		xml += '<topic>' + topic + '</topic>';
		xml += '</unregister>';
		amq.doSendPacket(xml);
		// console.log ( 'remove listener ' + topic );
	},
	sendMessage: function ( topic, message )
	{
		var xml = '_packet=<message>';
		xml += '<topic>' + topic + '</topic>';
		xml += '<msg>' + message + '</msg>';
		xml += '</message>';
		amq.doSendPacket(xml);
	},

	addPollHandler: function ( func )
	{
	},
	removeAllListener: function()
	{
		var xml = '_packet=<unregisterall>';
		// xml += '<topic>' + topic + '</topic>';
		xml += '</unregisterall>';
		amq.doSendPacket(xml);
	},

	doHandleError: function ( errorNo )
	{
	},
	doSessionTimeout: function()
	{
		// alert ('Your Session Timeout. Please Re-login.');
		window.location = '../logout.jsp';
	},

	doSendPacket: function ( packet )
	{
		// xmlHttp.open('POST', '../servlets/WebtradingServlet?'+(new Date().getTime()), true); // anti cache
		// var _data = 'action=kendaraanlist';
		$.ajax ({
			type: "POST",
			url: APP_CONTEXT_PATH + "/servlets/WebtradingServlet?" + (new Date().getTime()),
			data: packet,
			error: function ( jqXHR, textStatus, errorThrown )
			{
				// amq.doSessionTimeout();
			},
			success: function ( data )
			{
				amq.doHandleMessage ( data );
			}
		});
	},

	doSendPacketzzz: function ( packet )
	{
		var xmlHttp;
		try
		{
			xmlHttp = new XMLHttpRequest();
		}
		catch (e)
		{
			try
			{
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				try
				{
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e)
				{
					alert("Your browser does not support AJAX!");
				}
			}
		}
		xmlHttp.onreadystatechange = function()
		{
			if ( xmlHttp.readyState == 4 )
			{
				if ( xmlHttp.status == 200 )
				{
					// amq.doHandlePacket ( xmlHttp.responseText );
					amq.doHandleMessage ( xmlHttp.responseText );
				}
				else if ( xmlHttp.status == 403 )
				{
					// amq.doSessionTimeout();
				}
			}
		}

		xmlHttp.open('POST', '../servlets/WebtradingServlet?'+(new Date().getTime()), true); // anti cache
		xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlHttp.setRequestHeader("Content-length", packet.length);
		xmlHttp.setRequestHeader("Connection", "close");
		xmlHttp.send ( packet );
	},
	//doHandlePacket: function ( packet )
	//{
		// pasti ping
	//},

	doGetMessage: function ()
	{
		amq.doGetMessageAjax();
		// setTimeout ('amq.doGetMessage()', 1000);
		setTimeout ('amq.doGetMessage()', 999);
	},

	doGetMessageAjax: function ()
	{
		$.ajax ({
			type: "GET",
			url: APP_CONTEXT_PATH + "/servlets/WebtradingServlet?" + (new Date().getTime()),
			error: function ( jqXHR, textStatus, errorThrown )
			{
				// amq.doSessionTimeout();
			},
			success: function ( data )
			{
				amq.doHandleMessage ( data );
			}
		});
	},

	doGetMessageAjaxzzz: function ()
	{
		var xmlHttp;
		try
		{
			xmlHttp = new XMLHttpRequest();
		}
		catch (e)
		{
			try
			{
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				try
				{
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e)
				{
					alert("Your browser does not support AJAX!");
				}
			}
		}
		xmlHttp.onreadystatechange = function()
		{
			if ( xmlHttp.readyState == 4 )
			{
				if ( xmlHttp.status == 200 )
				{
					amq.doHandleMessage ( xmlHttp.responseText );
				}
				else if ( xmlHttp.status == 403 )
				{
					// amq.doSessionTimeout();
				}
			}
		}

		xmlHttp.open('GET', '../servlets/WebtradingServlet?'+(new Date().getTime()), true); // anti cache
		xmlHttp.send ( null );
	},


	doHandleMessage: function ( packet )
	{
		try
		{
			// alert ('handle message ' + packet);
			// console.log ('handle message ' + packet);

			var _data = packet.split('\n');
			for ( var i = 0; i < _data.length; ++i )
			{
				// var s = _data[i].trim();
				var s = _data[i];
				if ( s < 3 ) continue;
				if ( s == '<ping/>' ) continue;

				// alert ( 'Data [' + s + ']');
				amq.doProcessMessage ( s );
			}
		}
		catch (e)
		{
		}
	},
	doProcessMessage: function ( packet )
	{
		try
		{
			// console.log ( 'process message ' + packet );

			var index = packet.indexOf (' ');
			var key = packet.substring ( 1, index );

			//if ( key.startsWith ('<ob') )
			//{
			//	console.log ( packet );
			//}

			// var func = amq.MAP [ key ] ( packet );
			var func = amq.MAP [ key ];
			// console.log ( key + ' ' + func + ' ' + packet );
			func ( packet );
		}
		catch (e)
		{
		}
	},

}

amq.doGetMessage();

