var http_request = false;
function makeRequest(url,callerId)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}
	if (!http_request)
	{
		return false;
	}
	if (callerId == 'loadAccount')
	{
		http_request.onreadystatechange = loadAccountResult;
	}
	else if(callerId == 'loadOrder')
	{
		http_request.onreadystatechange = loadOrderResult;
	}
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadAccountResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadAccountResult(http_request.responseText);
		}
	}
}
function parseLoadAccountResult(result)
{
	var _account = document.getElementById('_account');
	var acc = result.split('#');
	_account.options.length = 0;
	for (i=0; i<acc.length; ++i)
	{
		if ( acc[i].length <= 0 )
		{
			continue;
		}
		try
		{
			_account.options[i] = new Option(acc[i], acc[i], false, false);
		}
		catch (err){}
	}
}
function loadAccount()
{
	// makeRequest('../servlets/AccountServlet?_a=acclist','loadAccount');
	parseLoadAccountResult ( _accountList );
}

function loadStockPosition()
{
	document.getElementById('_enter').value = 'Load';
	document.getElementById('_enter').disabled = true;
	var pin =  document.getElementById("_pin").value;
	var account =  document.getElementById("_account").value;
	makeRequest('../servlets/AccountServlet?_a=stockpos&_account=' + account + '&_pin=' + pin,'loadOrder');
}
function loadOrderResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadOrderResult(http_request.responseText);
		}
		else if ( http_request.status == 403 )
		{
			invalidPin();
		}
		else if ( http_request.status == 404 )
		{
			invalidPin();
		}
	}
}
function invalidPin()
{
	document.getElementById('sp_post').innerHTML = 'Wrong PIN';
	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
	// document.getElementById("_currentStockPosition").innerHTML = '';
	clearTable();
}
function parseLoadOrderResult(result)
{
	var _data = result.split('|');
	document.getElementById('sp_post').innerHTML = _data[0];

	// var _currentStock = '';
	clearTable();
	var _currentStock = document.getElementById("_currentStockPosition");
	for(var i=1;i<_data.length;++i)
	{
		if (_data[i].length<=3) continue;
		_subdata = _data[i].split('#');

		if (_subdata[0]=='S')
		{
			// if (i%2 == 0) _currentStock += '<tr class="stock_running_detail_grey">';
			// else _currentStock += '<tr class="stock_running_detail_white">';
			_tr = _currentStock.insertRow(_currentStock.rows.length);
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = _subdata[1];
			// _currentStock += '<td>'+_subdata[1]+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = parseInt(_subdata[2]).formatMoney(0);
			// _currentStock += '<td>'+parseInt(_subdata[2]).formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = parseInt(_subdata[4]).formatMoney(0);
			// _currentStock += '<td>'+parseInt(_subdata[4]).formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = '&nbsp;';
			// _currentStock += '<td></td>';
			//_currentStock += '</tr>';
		}
	}

	// document.getElementById("_currentStockPosition").innerHTML = _currentStock;

	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
}
function clearTable()
{
	var tbody = document.getElementById("_currentStockPosition");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};
function handleKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		loadStockPosition();
		return false;
	}
}
