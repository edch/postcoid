var _tradingLimit = 0;
var _buyFee = 0;
var _sellFee = 0;
var _stockPosition = new Array();

var http_request = false;
function makePOSTRequest(url, parameters)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			// set type accordingly to anticipated content type
			//http_request.overrideMimeType('text/xml');
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	http_request.onreadystatechange = alertContents;
	http_request.open('POST', url, true);
	http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_request.setRequestHeader("Content-length", parameters.length);
	http_request.setRequestHeader("Connection", "close");
	http_request.send(parameters);
}
function backToFade()
{
	var t = document.getElementById('quick_order_tab').tabber; /* Tabber object */
	t.tabShow(0);
}
function alertContents()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			//alert(http_request.responseText);
			result = http_request.responseText;
			if ( _action == 'b' || _action == 's' )
			{
				document.getElementById(_action + 'myspan').innerHTML = result;
				document.getElementById('_' + _action + 'send').disabled = false
			
				document.getElementById("_" + _action + "pin").value = ""
				document.getElementById("_" + _action + "stock").value = ""
				document.getElementById("_" + _action + "price").value = ""
				document.getElementById("_" + _action + "volume").value = ""
				// document.getElementById("_action").value = ""

				if (result.indexOf('Order Sent.') != -1) setTimeout ( "backToFade()", 3500 );
			}
			else if ( _action == 'lw' )
			{
				// alert ( result );
				var orderList = result.split ('|');
				var tableData = '<table width="100%" border="0" cellpadding="2px" cellspacing="0">';
				for (i=0; i<orderList.length; ++i)
				{
					var order = orderList[i].split(",");
					if ( order.length != 7 ) continue;

					if ( i % 2 == 0 )
					{
						tableData += '<tr style="font-weight:bold;background-color:#CFCFCF;">';
					}
					else
					{
						tableData += '<tr style="font-weight:bold;">';
					}

					tableData += '<td width="5%">&nbsp;</td>';
					tableData += '<td width="25%" align="left" class="col_black"><a href="" onclick="return getWithdraw(\'' + order[0] + '\',\'' + order[1] + '\',\'' + order[2] + '\',\'' + order[3] + '\',\'' + order[4] + '\',\'' + order[5] + '\');">' + order[2] + '</a></td>';
					tableData += '<td width="10%" align="center" class="col_black">' + order[3] + '</td>';
					tableData += '<td width="20%" align="right" class="col_black">' + order[4] + '</td>';
					tableData += '<td width="20%" align="right" class="col_black">' + order[6] + '</td>';
					tableData += '<td width="20%" align="right" class="col_black">' + (order[6]-order[5]) + '</td>';
					tableData += '<td width="0%">&nbsp;</td>';
					tableData += '</tr>';
				}
				tableData += '</table>';

				document.getElementById('withdrawOrderListDiv').innerHTML = tableData;
			}
			else if ( _action == 'w' )
			{
				document.getElementById("_" + _action + "pin").value = ""

				document.getElementById('wmyspan').innerHTML = result;
				loadOrderList('lw');
			}
			else if ( _action == 'la' )
			{
				var orderList = result.split ('|');
				var tableData = '<table width="100%" border="0" cellpadding="2px" cellspacing="0">';
				for (i=0; i<orderList.length; ++i)
				{
					var order = orderList[i].split(",");
					if ( order.length != 7 ) continue;

					if ( i % 2 == 0 )
					{
						tableData += '<tr style="font-weight:bold;background-color:#CFCFCF;">';
					}
					else
					{
						tableData += '<tr style="font-weight:bold;">';
					}

					tableData += '<td width="5%">&nbsp;</td>';
					// tableData += '<td width="25%" align="left" class="col_black"><a href="" onclick="return drawAmend(\'' + order[0] + '\',\'' + order[1] + '\',\'' + order[2] + '\',\'' + order[3] + '\',\'' + order[4] + '\',\'' + order[5] + '\');">' + order[2] + '</a></td>';
					tableData += '<td width="25%" align="left" class="col_black"><a href="" onclick="return drawAmend(\'' + order[0] + '\',\'' + order[1] + '\',\'' + order[2] + '\',\'' + order[3] + '\',\'' + order[4] + '\',\'' + order[6] + '\',\'' + order[5] + '\');">' + order[2] + '</a></td>';
					tableData += '<td width="10%" align="center" class="col_black">' + order[3] + '</td>';
					tableData += '<td width="20%" align="right" class="col_black">' + order[4] + '</td>';
					// tableData += '<td width="20%" align="right" class="col_black">' + order[5] + '</td>';
					tableData += '<td width="20%" align="right" class="col_black">' + order[6] + '</td>';
					tableData += '<td width="20%" align="right" class="col_black">' + (order[6]-order[5]) + '</td>';
					tableData += '<td width="0%">&nbsp;</td>';
					tableData += '</tr>';
				}
				tableData += '</table>';

				document.getElementById('amendOrderListDiv').innerHTML = tableData;
			}
			else if ( _action == 'a' )
			{
				document.getElementById("_" + _action + "pin").value = ""

				document.getElementById('amyspan').innerHTML = result;
				setTimeout("loadOrderList('la')", 1500);
			}
		}
		else if ( http_request.status == 403 )
		{
			document.getElementById(_action + 'myspan').innerHTML = 'Invalid PIN.';
		}
		else if ( http_request.status == 404 )
		{
			document.getElementById(_action + 'myspan').innerHTML = 'Invalid PIN.';
		}
	}
}

function loadOrderList(clickedAction)
{
	_action = clickedAction;
	var poststr  = "_a=qoorderlist"
	makePOSTRequest('../servlets/AccountServlet', poststr);
}

var _action='';
function getBuy(obj)
{
	var _stock = document.getElementById("_bstock").value;
	var _price = document.getElementById("_bprice").value;
	var _lot = document.getElementById("_bvolume").value;
	document.getElementById('_bstock').focus();
	var poststr  = "pin=" + encodeURI( document.getElementById("_bpin").value )
	poststr += "&stock=" + encodeURI( _stock )
	poststr += "&price=" + encodeURI( _price )
	poststr += "&volume=" + encodeURI( _lot )
	poststr += "&account=" + encodeURI( document.getElementById("_bacc").value )
	poststr += "&action=B"
	var _question = "BUY " + _stock + " " + _lot + " Lot[s] @" + _price + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		_action = 'b';
		document.getElementById('bmyspan').innerHTML = "Please Wait ...";
		document.getElementById('_bsend').disabled = true

		//makePOSTRequest('../servlets/OrderServlet', poststr);
		makePOSTRequest('../servlets/OrderServlet', poststr);
	}
}
function getSell(obj)
{
	var _stock = document.getElementById("_sstock").value;
	var _price = document.getElementById("_sprice").value;
	var _lot = document.getElementById("_svolume").value;
	document.getElementById('_sstock').focus();
	var poststr  = "pin=" + encodeURI( document.getElementById("_spin").value )
	poststr += "&stock=" + encodeURI( _stock )
	poststr += "&price=" + encodeURI( _price )
	poststr += "&volume=" + encodeURI( _lot )
	poststr += "&account=" + encodeURI( document.getElementById("_sacc").value )
	poststr += "&action=S"
	var _question = "SELL " + _stock + " " + _lot + " Lot[s] @" + _price + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		_action = 's';
		document.getElementById('smyspan').innerHTML = "Please Wait ...";
		document.getElementById('_ssend').disabled = true

		// makePOSTRequest('../servlets/OrderServlet', poststr);
		makePOSTRequest('../servlets/OrderServlet', poststr);
	}
}
function getWithdraw (orderId, jsxid, stockCode, command, stockPrice, stockVolume)
{
	var _question = "WITHDRAW " + stockCode.toUpperCase() + " " + stockVolume + " Lot[s] @" + stockPrice + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		document.getElementById('wmyspan').innerHTML = "Please Wait ...";

		var poststr  = "pin=" + encodeURI( document.getElementById("_wpin").value )
		poststr += "&stock=" + stockCode
		poststr += "&price=" + stockPrice
		poststr += "&volume=" + stockVolume
		poststr += "&jsxid=" + jsxid
		poststr += "&orderid=" + orderId
		poststr += "&action=W"

		_action = 'w';
		makePOSTRequest('../servlets/OrderServlet', poststr);
	}

	return false;
}
function getAmend (obj)
{
	var _stock = document.getElementById("_astock").value;
	var _price = document.getElementById("_aprice").value;
	var _lot = document.getElementById("_avolume").value;
	
	var poststr  = "pin=" + encodeURI( document.getElementById("_apin").value )
	poststr += "&stock=" + encodeURI( _stock )
	poststr += "&price=" + encodeURI( _price )
	poststr += "&volume=" + encodeURI( _lot )
	poststr += "&action=A"
	poststr += "&jsxid=" + encodeURI( document.getElementById("_ajsxid").value )
	poststr += "&orderid=" + encodeURI( document.getElementById("_aorderid").value )

	var _question = "AMEND " + _stock + " " + _lot + " Lot[s] @" + _price + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		_action = 'a';
		document.getElementById('amyspan').innerHTML = "Please Wait ...";
		document.getElementById('_asend').disabled = true

		// setTimeout ("makePOSTRequest('../servlets/OrderServlet', '" + poststr + "')", 2500);
		makePOSTRequest('../servlets/OrderServlet', poststr);
	}
}
function drawAmend (orderId, jsxid, stockCode, command, stockPrice, stockVolume, remainVolume)
{
	var tableData = '<div align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="color:#FFFFFF;font-weight:bold">';
	tableData += '<tr>';
	tableData += '<td class="col_black">Pin</td>';
	tableData += '<td>: <input type="password" name="_apin" id="_apin" maxlength="6" class="inp_code2"/></td>';
	tableData += '</tr>';
	tableData += '<tr>';
	tableData += '<td class="col_black">Stock</td>';
	tableData += '<td>: <input type="text" name="_astock" id="_astock" maxlength="8" class="inp_code2" value="' + stockCode + '" disabled/></td>';
	tableData += '</tr>';
	tableData += '<tr>';
	tableData += '<td class="col_black">Price</td>';
	tableData += '<td>: <input type="text" name="_aprice" id="_aprice" maxlength="8" class="inp_code2" value="' + stockPrice + '"/></td>';
	tableData += '</tr>';
	tableData += '<tr>';
	tableData += '<td class="col_black">Match Vol</td>';
	tableData += '<td>: <input type="text" name="_mvolume" id="_mvolume" maxlength="6" class="inp_code2" value="' + (stockVolume-remainVolume) + '" disabled="disabled" /></td>';
	tableData += '</tr>';
	tableData += '<td class="col_black">Total Vol</td>';
	tableData += '<td>: <input type="text" name="_avolume" id="_avolume" maxlength="6" class="inp_code2" value="' + stockVolume + '"/></td>';
	tableData += '</tr>';
	tableData += '<tr>';
	tableData += '<td height="5" colspan="2">&nbsp;</td>';
	tableData += '</tr>';
	tableData += '<tr>';
	tableData += '<td colspan="2"><label>';
	tableData += '<div align="center">';
	tableData += '<input type="button" class="btnQuick" name="_asend" id="_asend" value="Execute Amend" onClick="javascript:getAmend(this.parentNode);"/>';
	tableData += '</div>';
	tableData += '</label>';
	tableData += '</td>';
	tableData += '</tr>';
	tableData += '</table></div>';
	tableData += '<input type="hidden" name="_aorderid" id="_aorderid" value="' + orderId + '"/></td>';
	tableData += '<input type="hidden" name="_ajsxid" id="_ajsxid" value="' + jsxid + '"/></td>';
	document.getElementById('amendOrderListDiv').innerHTML = tableData;

	return false;
}
function loadAccount()
{
	var bAcc = document.getElementById('_bacc');
	var sAcc = document.getElementById('_sacc');

	var acc = _accountList.split('#');
	for (i=0; i<acc.length; ++i)
	{
		if ( acc[i].length <= 0 )
		{
			continue;
		}

		try
		{
			bAcc.options[i] = new Option(acc[i], acc[i], false, false);
		}
		catch (err){}

		try
		{
			sAcc.options[i] = new Option(acc[i], acc[i], false, false);
		}
		catch (err){}
	}
}



function calculateBuyLot()
{
	try
	{
		var _price = parseInt(document.getElementById('_bprice').value);
		// var maxLot = Math.floor ( _tradingLimit / ( _price * _LOT_SIZE ) );
		var maxLot = Math.floor ( _tradingLimit / ( ( _price * _LOT_SIZE ) + ( _price * _LOT_SIZE * _buyFee ) ) );

		if ( maxLot < 0 ) maxLot = 0;
		// document.getElementById('_blotinfo').innerHTML = (_tradingLimit/(_price*_LOT_SIZE)).formatMoney(0) + ' Lot&nbsp;';
		document.getElementById('_blotinfo').innerHTML = maxLot.formatMoney(0) + ' Lot&nbsp;';
	}
	catch (err) {};
}
function calculateSellLot()
{
	try
	{
		var _stock = document.getElementById('_sstock').value.toUpperCase();
		var _f = false;
		for (var i=1; i<_stockPosition.length; ++i)
		{
			var _info = _stockPosition[i].split('#');
			if ( _info[1] == _stock )
			{
				_f = true;
				document.getElementById('_slotinfo').innerHTML = parseFloat(_info[4]).formatMoney(0) + ' Lot&nbsp;';
				break;
			}
		}

		if (!_f)
		{
			document.getElementById('_slotinfo').innerHTML = '0 Lot&nbsp;';
		}
	}
	catch (err) {};
}

var http_get = false;
function getInformation(_acc,_pin)
{
	makeRequest ('../servlets/AccountServlet?_a=quickorder&_pin='+_pin+'&_account='+_acc);
}
function getInformationWithStock(_acc,_pin,_stock)
{
	// alert ( _stock );
	makeRequest ('../servlets/AccountServlet?_a=quickorder&_pin='+_pin+'&_account='+_acc+'&_stock='+_stock);
}
function loadInformation()
{
	if (http_get.readyState == 4)
	{
		if (http_get.status == 200)
		{
			var _s = http_get.responseText;
			_stockPosition = _s.split('|');
			var _info = _stockPosition[0].split('#');
			_tradingLimit = parseFloat(_info[0]);
			_buyFee = parseFloat(_info[5]);
			_sellFee = parseFloat(_info[6]);
		}
	}
}
function makeRequest(url)
{
	http_get = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_get = new XMLHttpRequest();
		if (http_get.overrideMimeType)
		{
			// set type accordingly to anticipated content type
			http_get.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_get = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_get = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}

	if (!http_get)
	{
		return false;
	}

	http_get.onreadystatechange = loadInformation;
	http_get.open('GET', url, true);
	http_get.send(null);
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};
function handleQuickOrderKeyPress(e, obj, oId)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		if (oId == 1) getBuy();
		else if (oId == 2) getSell();
		return false;
	}
}
