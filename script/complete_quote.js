
var priceHandler = 
{
	_price: function(message) 
	{
		if (message != null)
		{
			processQuotes ( message );
		}
	}
};

var ihsgHandler = 
{
	_ihsg: function(message)
	{
		if (message != null) {
			var _composite = parseFloat ( message.getAttribute ( '_c' ) )
			var _prev = parseFloat ( message.getAttribute ( '_p' ) )
			var _freq = parseInt ( message.getAttribute ( '_f' ) )
			var _volume = parseFloat ( message.getAttribute ( '_vo' ) )
			var _value = parseFloat ( message.getAttribute ( '_va' ) ) / 1000000
			var _chg = Math.abs ( _composite - _prev );
			var _persenchg = (_chg / _prev) * 100;
			
			var _data = document.getElementById('_ihsg_ihsg')
			_data.innerHTML = _composite.formatMoney (2);

			var _ihsg_title = document.getElementById('header_IHSG_Title');
			var _data_chg = document.getElementById('_chg_ihsg')
			var _data_persen_chg = document.getElementById('_persen_ihsg')
			_data_chg.innerHTML = _chg.toFixed(2);
			_data_persen_chg.innerHTML = _persenchg.toFixed(2) + " %";
			if ( _composite < _prev )
			{
				_data_chg.className = "col_red";
				_data_persen_chg.className = "col_red";
				_ihsg_title.style.backgroundColor = '#e54044';
			}
			else if (_composite > _prev  )
			{
				_data_chg.className = "col_green";
				_data_persen_chg.className = "col_green";
				_ihsg_title.style.backgroundColor = '#01ba3a';
			}
			else if ( _composite == _prev  )
			{
				_data_chg.className = "col_yellow";
				_data_persen_chg.className = "col_yellow";
				_ihsg_title.style.backgroundColor = '#01ba3a';
			}

			_data = document.getElementById('_freq_ihsg')
			_data.innerHTML = '<font color="#1055cf">Freq</font> ' + _freq.formatMoney (0);
			_data = document.getElementById('_volume_ihsg')
			_data.innerHTML = '<font color="#1055cf">Vol</font> ' + _volume.formatMoney (0);
			_data = document.getElementById('_value_ihsg')
			
			if ( _value < 100000 )
			{
				_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " M";
			}
			else
			{
				_value = _value / 1000;
				_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " B";
			}
		}
	}
};

var _maxRow = 20;
function processQuotes ( message )
{
	if ( message != null )
	{
		var _row = 0;
		var _stock = message.getAttribute ( '_s' ).toUpperCase();
		var _time = message.getAttribute ( '_t' );
		var _last = parseInt ( message.getAttribute ( '_l' ) )
		var _prev = parseInt ( message.getAttribute ( '_p' ) )
		var _open = parseInt ( message.getAttribute ( '_o' ) )
		var _high = parseInt ( message.getAttribute ( '_hi' ) )
		var _low = parseInt ( message.getAttribute ( '_lo' ) )
		var _freq = parseInt ( message.getAttribute ( '_f' ) )
		var _vol = parseInt ( message.getAttribute ( '_vo' ) ) / _LOT_SIZE;
		var _val = parseInt ( message.getAttribute ( '_va' ) ) / 1000000

		var _bid = parseInt ( message.getAttribute ( '_ba' ) )
		var _bidVol = parseInt ( message.getAttribute ( '_bo' ) ) / _LOT_SIZE;
		var _offer = parseInt ( message.getAttribute ( '_oa' ) )
		var _offerVol = parseInt ( message.getAttribute ( '_oo' ) ) / _LOT_SIZE;


		var _chg = Math.abs ( _last - _prev )
		var _persenchg = (_chg / _prev) * 100;

		var _eStyle = "col_green";
		if ( _last < _prev )
		{
			_eStyle = "col_red";
		}
		else if ( _last > _prev )
		{
			_eStyle = "col_green";
		}
		else if ( _last == _prev )
		{
			_eStyle = "col_yellow";
		}

		for ( _row = 0; _row < _maxRow; ++_row )
		{
			var _codeRow = document.getElementById ("_code_row" + _row ).innerHTML;
			if ( _codeRow == _stock )
			{
				document.getElementById ("_last_row" + _row).innerHTML = _last.formatMoney(0);
				document.getElementById ("_last_row" + _row).className = _eStyle;

				document.getElementById ("_chg_row" + _row).innerHTML = _chg.formatMoney(0);
				document.getElementById ("_chg_row" + _row).className = _eStyle;

				document.getElementById ("_persenchg_row" + _row).innerHTML = _persenchg.formatMoney(2);
				document.getElementById ("_persenchg_row" + _row).className = _eStyle;

				document.getElementById ("_time_row" + _row).innerHTML = _time;

				if ( _prev < _open )
				{
					document.getElementById ("_open_row" + _row).className = "col_green";
				}
				else if ( _prev > _open )
				{
					document.getElementById ("_open_row" + _row).className = "col_red";
				}
				else if ( _prev == _open )
				{
					document.getElementById ("_open_row" + _row).className = "col_yellow";
				}

				document.getElementById ("_open_row" + _row).innerHTML = _open.formatMoney(0);



				if ( _prev < _high )
				{
					document.getElementById ("_high_row" + _row).className = "col_green";
				}
				else if ( _prev > _high )
				{
					document.getElementById ("_high_row" + _row).className = "col_red";
				}
				else if ( _prev == _high )
				{
					document.getElementById ("_high_row" + _row).className = "col_yellow";
				}

				document.getElementById ("_high_row" + _row).innerHTML = _high.formatMoney(0);



				if ( _prev < _low )
				{
					document.getElementById ("_low_row" + _row).className = "col_green";
				}
				else if ( _prev > _low )
				{
					document.getElementById ("_low_row" + _row).className = "col_red";
				}
				else if ( _prev == _low )
				{
					document.getElementById ("_low_row" + _row).className = "col_yellow";
				}

				document.getElementById ("_low_row" + _row).innerHTML = _low.formatMoney(0);


				// added
				if ( _prev < _bid )
				{
					document.getElementById ("_bidvol_row" + _row).className = "col_green";
					document.getElementById ("_bid_row" + _row).className = "col_green";
				}
				else if ( _prev > _bid )
				{
					document.getElementById ("_bidvol_row" + _row).className = "col_red";
					document.getElementById ("_bid_row" + _row).className = "col_red";
				}
				else if ( _prev == _bid )
				{
					document.getElementById ("_bidvol_row" + _row).className = "col_yellow";
					document.getElementById ("_bid_row" + _row).className = "col_yellow";
				}

				document.getElementById ("_bidvol_row" + _row).innerHTML = _bidVol.formatMoney(0);
				document.getElementById ("_bid_row" + _row).innerHTML = _bid.formatMoney(0);


				if ( _prev < _offer )
				{
					document.getElementById ("_offervol_row" + _row).className = "col_green";
					document.getElementById ("_offer_row" + _row).className = "col_green";
				}
				else if ( _prev > _offer )
				{
					document.getElementById ("_offervol_row" + _row).className = "col_red";
					document.getElementById ("_offer_row" + _row).className = "col_red";
				}
				else if ( _prev == _offer )
				{
					document.getElementById ("_offervol_row" + _row).className = "col_yellow";
					document.getElementById ("_offer_row" + _row).className = "col_yellow";
				}

				document.getElementById ("_offer_row" + _row).innerHTML = _offer.formatMoney(0);
				document.getElementById ("_offervol_row" + _row).innerHTML = _offerVol.formatMoney(0);

				break;
			}
		}
	}
}

function portfolioPoll(first)
{
	if (first)
	{
		amq.addListener('index','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
		amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=COMPOSITE>' );
	}
}

//amq.uri='../amq';
//amq.addPollHandler(portfolioPoll);

function unloadPage (unloadIndex)
{
	if (unloadIndex == 'yes')
	{
		amq.removeListener ( 'index', 'topic://QUOTES.COMPOSITE.INDICES');
		amq.removeAllListener();
	}
	
	var i = 0;
	for (i = 0; i < _rowQuotes; ++i)
	{
		var _stockCode = document.getElementById("_code_row" + i).innerHTML;
		if ( _stockCode != '')
		{
			amq.removeListener ('qt', 'topic://QUOTES.' + _stockCode + '.RG');
		}
	}
}
function processStockList(stockList)
{
	unloadPage('no');
	var stocks = stockList.split(":");
	_rowQuotes = 0;
	for (i = 0; i < stocks.length-1; ++i)
	{
		document.getElementById("_code_row" + _rowQuotes).innerHTML = stocks[i];
		document.getElementById("_last_row" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_chg_row" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_persenchg_row" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_open_row" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_high_row" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_low_row" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_bidvol_row" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_bid_row" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_offer_row" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_offervol_row" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_time_row" + _rowQuotes).innerHTML = '&nbsp';

		// amq.addListener('quotes' + stocks[i] + '.RG','topic://QUOTES.' + stocks[i] + '.RG', priceHandler._price);
		amq.addListener('qt','topic://QUOTES.' + stocks[i] + '.RG', priceHandler._price);
		amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + stocks[i] + '>' );
		++_rowQuotes;
	}
	for (; i < _maxRow; ++i)
	{
		document.getElementById("_code_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_last_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_chg_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_persenchg_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_open_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_high_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_low_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_bidvol_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_bid_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_offer_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_offervol_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_time_row" + i).innerHTML = '&nbsp';
	}
}

var _rowQuotes = 0;
function handleQuoteKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		obj.value = obj.value.toUpperCase();
		if ( _allStocksCode.indexOf ( ':' + obj.value + ':' ) < 0 )
		{
			alert ( 'Invalid Stock Code.' );
			obj.value = '';
			obj.focus();
			return false;
		}

		var i = 0;
		for (i = 0; i < _maxRow; ++i)
		{
			if ( document.getElementById("_code_row" + i).innerHTML == obj.value )
			{
				obj.value = '';
				obj.focus();
				return false;
			}
		}

		document.getElementById ("_code_row" + _rowQuotes ).innerHTML = obj.value;
		// amq.addListener('quotes' + obj.value + '.RG','topic://QUOTES.' + obj.value + '.RG', priceHandler._price);
		amq.addListener('qt','topic://QUOTES.' + obj.value + '.RG', priceHandler._price);
		amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + obj.value + '>' );

		if ( _rowQuotes < _maxRow )
		{
			++_rowQuotes;
		}

		obj.value = '';
		obj.focus();
		return false;
	}
}
function deleteQuote(num)
{
	var _stockCode = document.getElementById ("_code_row" + num).innerHTML;
	if ( _stockCode == '' )
	{
		return false;
	}

	try
	{
		// amq.removeListener ('quotes' + _stockCode + '.RG', 'topic://QUOTES.' + _stockCode + '.RG');
		amq.removeListener ('qt', 'topic://QUOTES.' + _stockCode + '.RG');

		var i = 0;
		var j = 0;
		_rowQuotes--;

		for ( i = num; i < _rowQuotes; ++i )
		{
			j = i+1;
			document.getElementById("_code_row" + i).innerHTML = document.getElementById("_code_row" + j).innerHTML;
			document.getElementById("_last_row" + i).innerHTML = document.getElementById("_last_row" + j).innerHTML;
			document.getElementById("_chg_row" + i).innerHTML = document.getElementById("_chg_row" + j).innerHTML;
			document.getElementById("_persenchg_row" + i).innerHTML = document.getElementById("_persenchg_row" + j).innerHTML;
			document.getElementById("_open_row" + i).innerHTML = document.getElementById("_open_row" + j).innerHTML;
			document.getElementById("_high_row" + i).innerHTML = document.getElementById("_high_row" + j).innerHTML;
			document.getElementById("_low_row" + i).innerHTML = document.getElementById("_low_row" + j).innerHTML;
			document.getElementById("_bidvol_row" + i).innerHTML = document.getElementById("_bidvol_row" + j).innerHTML;
			document.getElementById("_bid_row" + i).innerHTML = document.getElementById("_bid_row" + j).innerHTML;
			document.getElementById("_offer_row" + i).innerHTML = document.getElementById("_offer_row" + j).innerHTML;
			document.getElementById("_offervol_row" + i).innerHTML = document.getElementById("_offervol_row" + j).innerHTML;
			document.getElementById("_time_row" + i).innerHTML = document.getElementById("_time_row" + j).innerHTML;
		}

		document.getElementById("_code_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_last_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_chg_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_persenchg_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_open_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_high_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_low_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_bidvol_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_bid_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_offer_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_offervol_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_time_row" + i).innerHTML = '&nbsp';
	}
	catch (Err) {}

	return false;
}

Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};


function startScreen()
{
	amq.addListener('ihsg','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
	amq.sendMessage('topic://REQUEST.FIRST', '<c s=COMPOSITE>');
}
function stopScreen()
{
	try
	{
		amq.removeAllListener();
		// ihsgHandler.stopIhsg();
	}
	catch (err) {}
}
