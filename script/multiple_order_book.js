
var currentStockCode1 = 'xxxx'
var currentStockCode2 = 'xxxx'
var currentStockCode3 = 'xxxx'
var _prev = 0

var priceHandler = 
{
	_price: function(message) 
	{
		if (message != null)
		{
			// quotes
			var _stock = message.getAttribute ( '_s' ).toUpperCase();

			var _last = parseInt ( message.getAttribute ( '_l' ) )
			_prev = parseInt ( message.getAttribute ( '_p' ) )
			var _open = parseInt ( message.getAttribute ( '_o' ) )
			var _high = parseInt ( message.getAttribute ( '_hi' ) )
			var _low = parseInt ( message.getAttribute ( '_lo' ) )
			var _freq = parseInt ( message.getAttribute ( '_f' ) )
			var _vol = parseInt ( message.getAttribute ( '_vo' ) ) / _LOT_SIZE;
			var _val = parseInt ( message.getAttribute ( '_va' ) ) / 1000000
			var _chg = Math.abs ( _last - _prev )
			var _persenchg = (_chg / _prev) * 100;
			var _stockName = message.getAttribute ( '_n' )

			var _bookNo = '1';
			if ( _stock == currentStockCode1 )
			{
				_bookNo = '1';
			}
			else if ( _stock == currentStockCode2 )
			{
				_bookNo = '2';
			}
			else if ( _stock == currentStockCode3 )
			{
				_bookNo = '3';
			}
			else
			{
				return false;
			}

			if ( _stockName != null )
			{
				document.getElementById ("_stockName" + _bookNo).innerHTML = _stockName;
			}

			var _eStyle = "col_yellow";

			if ( _last < _prev )
			{
				_eStyle = "col_red";
			}
			else if ( _last > _prev )
			{
				_eStyle = "col_green";
			}
			else if ( _last == _prev )
			{
				_eStyle = "col_yellow";
			}

			var _element = document.getElementById ("_last" + _bookNo);
			_element.innerHTML = _last.formatMoney (0);
			_element.className = _eStyle;
			
			_element = document.getElementById ("_chg" + _bookNo);
			_element.innerHTML = _chg.formatMoney (0);
			_element.className = _eStyle;
			
			_element = document.getElementById ("_prev" + _bookNo);
			_element.innerHTML = _prev.formatMoney (0);
			_element.className = "col_yellow";
			
			_element = document.getElementById ("_persenchg" + _bookNo);
			_element.innerHTML = _persenchg.formatMoney (2);
			_element.className = _eStyle;
			
			_element = document.getElementById ("_open" + _bookNo);
			_element.innerHTML = _open.formatMoney (0);			
			if ( _open < _prev ) _element.className = "col_red";
			else if ( _open > _prev ) _element.className = "col_green";
			else if ( _open == _prev ) _element.className = "col_yellow";
			
			_element = document.getElementById ("_high" + _bookNo);
			_element.innerHTML = _high.formatMoney (0);			
			if ( _high < _prev ) _element.className = "col_red";
			else if ( _high > _prev ) _element.className = "col_green";
			else if ( _high == _prev ) _element.className = "col_yellow";

			_element = document.getElementById ("_low" + _bookNo);
			_element.innerHTML = _low.formatMoney (0);			
			if ( _low < _prev ) _element.className = "col_red";
			else if ( _low > _prev ) _element.className = "col_green";
			else if ( _low == _prev ) _element.className = "col_yellow";

			_element = document.getElementById ("_freq" + _bookNo);
			_element.innerHTML = _freq.formatMoney (0);
			_element.className = "col_black";

			_element = document.getElementById ("_vol" + _bookNo);
			_element.innerHTML = _vol.formatMoney (0);
			_element.className = "col_black";

			_element = document.getElementById ("_val" + _bookNo);
			
			if ( _val < 100000 )
			{
				_element.innerHTML = _val.formatMoney(2) + " M";
			}
			else			
			{
				_val /= 1000;
				_element.innerHTML = _val.formatMoney(2) + " B";
			}
			_element.className = "col_black";
		}
	}
};

var orderBookHandler =
{
	_orderBookReceived: function(message)
	{
		if (message != null)
		{
			var _stock = message.getAttribute ( '_s' ).toUpperCase();
			
			var _prev = parseInt ( message.getAttribute ( '_p' ) )
			// var _stockCode1 = document.getElementById ("_stockCode1").value.toUpperCase();
			// var _stockCode2 = document.getElementById ("_stockCode2").value.toUpperCase();
			// var _stockCode3 = document.getElementById ("_stockCode3").value.toUpperCase();
			var _bookNo = '1';

			if ( _stock == currentStockCode1 )
			{
				_bookNo = '1';
			}
			else if ( _stock == currentStockCode2 )
			{
				_bookNo = '2';
			}
			else if ( _stock == currentStockCode3 )
			{
				_bookNo = '3';
			}
			else
			{
				return false;
			}

			// order book
			var i = 0
			var _eStyle = "col_yellow";
			for (i = 0; i < 5; ++i)
			{			
				var _bidValMsg = parseInt ( message.getAttribute ( '_ba' + i ) )
				var _bidVolMsg = parseInt ( message.getAttribute ( '_bo' + i ) )
				
				var _bidVol = document.getElementById ("_bidVol" + _bookNo + "" + i);
				var _bidVal = document.getElementById ("_bidVal" + _bookNo + "" + i);
				if ( _bidVolMsg != null && _bidValMsg != null )
				{
					if ( _bidValMsg < _prev ) _eStyle = "col_red";
					else if ( _bidValMsg > _prev ) _eStyle = "col_green";
					else if ( _bidValMsg == _prev ) _eStyle = "col_yellow";

					_bidVol.className = _eStyle;
					_bidVal.className = _eStyle;
					_bidVol.innerHTML = _bidVolMsg.formatMoney (0);
					_bidVal.innerHTML = _bidValMsg.formatMoney (0);
				}

				var _offerValMsg = parseInt ( message.getAttribute ( '_oa' + i ) )
				var _offerVolMsg = parseInt ( message.getAttribute ( '_oo' + i ) )

				var _offerVol = document.getElementById ("_offerVol" + _bookNo + "" + i);
				var _offerVal = document.getElementById ("_offerVal" + _bookNo + "" + i);
				if ( _offerVolMsg != null && _offerValMsg != null )
				{
					if ( _offerValMsg < _prev ) _eStyle = "col_red";
					else if ( _offerValMsg > _prev ) _eStyle = "col_green";
					else if ( _offerValMsg == _prev ) _eStyle = "col_yellow";

					_offerVol.className = _eStyle;
					_offerVal.className = _eStyle;
					_offerVol.innerHTML = _offerVolMsg.formatMoney (0);
					_offerVal.innerHTML = _offerValMsg.formatMoney (0);
				}
			}
		}
	}
};

var tradeBookHandler =
{
	_tradeBookReceived: function(message)
	{
		if (message != null)
		{
			var _stock = message.getAttribute ( '_s' ).toUpperCase();
			var _bookNo = '1';
			if ( _stock == currentStockCode1 )
			{
				_bookNo = '1';
			}
			else if ( _stock == currentStockCode2 )
			{
				_bookNo = '2';
			}
			else if ( _stock == currentStockCode3 )
			{
				_bookNo = '3';
			}
			else
			{
				return false;
			}

			var _prev = parseInt ( message.getAttribute ( '_p' ) )
			var i = 0;
			var innerCode = '<table class="stock_detail_line2" cellpadding="2" cellspacing="0" width="218" border="0">';
			innerCode += '<tr><td width="55"></td><td width="40"></td><td width="54"></td><td width="68"></td><td width="1"></td></tr>';
			for (i = 0;; ++i)
			{
				try
				{
					var _price = message.getAttribute ( '_p' + i );
					if ( _price == null )
					{
						break;
					}

					_price = parseInt ( _price );
					var eStyle = 'col_yellow';
					if ( _price > _prev )
					{
						eStyle = 'col_green';
					}
					else if ( _price < _prev )
					{
						eStyle = 'col_red';
					}

					var _volume = parseInt ( message.getAttribute ( '_v' + i ) ) / _LOT_SIZE;
					var _freq = parseInt ( message.getAttribute ( '_f' + i ) );
				}
				catch ( err )
				{
					break;
				}

				//if (i%2 == 0) innerCode += '<tr class="stock_running_detail_grey">';
				//else innerCode += '<tr class="stock_running_detail_white">';
				if (i%2 == 0) innerCode += '<tr style="background-color:#e5e5e5;">';
				else innerCode += '<tr>';
				// innerCode += '<tr>';
				innerCode += '<td class="' + eStyle + '">' + _price.formatMoney(0) + '</td>';
				innerCode += '<td class="col_black">' + _freq + '</td>';
				innerCode += '<td class="col_black">' + _volume.formatMoney(0) + '</td>';
				var _value = _price * _volume * _LOT_SIZE;
				if ( _value < 100000 )
				{
					_value /= 1000;
					innerCode += '<td class="col_black">' + _value.formatMoney (2) + ' K' + '</td>';
				}
				else
				{
					_value = _value / 1000000;
					innerCode += '<td class="col_black">' + _value.formatMoney (2) + ' M' + '</td>';
				}

				innerCode += '<td></td></tr>';
			}
			innerCode += '</table>';
			document.getElementById( '_tradeBook' + _bookNo ).innerHTML = innerCode;
		}
	}
};


var ihsgHandler = 
{
	_ihsg: function(message)
	{
		if (message != null) {
			var _composite = parseFloat ( message.getAttribute ( '_c' ) )
			var _prev = parseFloat ( message.getAttribute ( '_p' ) )
			var _freq = parseInt ( message.getAttribute ( '_f' ) )
			var _volume = parseFloat ( message.getAttribute ( '_vo' ) )
			var _value = parseFloat ( message.getAttribute ( '_va' ) ) / 1000000
			var _chg = Math.abs ( _composite - _prev );
			var _persenchg = (_chg / _prev) * 100;
			
			var _ihsg_title = document.getElementById('header_IHSG_Title');
			var _data = document.getElementById('_ihsg_ihsg')
			_data.className = "col_black";
			_data.innerHTML = _composite.formatMoney (2);

			var _data_chg = document.getElementById('_chg_ihsg')
			var _data_persen_chg = document.getElementById('_persen_ihsg')
			_data_chg.innerHTML = _chg.toFixed(2);
			_data_persen_chg.innerHTML = _persenchg.toFixed(2) + " %";
			if ( _composite < _prev )
			{
				_data_chg.className = "col_red";
				_data_persen_chg.className = "col_red";
				_ihsg_title.style.backgroundColor = '#e54044';
			}
			else if (_composite > _prev  )
			{
				_data_chg.className = "col_green";
				_data_persen_chg.className = "col_green";
				_ihsg_title.style.backgroundColor = '#01ba3a';
			}
			else if ( _composite == _prev  )
			{
				_data_chg.className = "col_yellow";
				_data_persen_chg.className = "col_yellow";
				_ihsg_title.style.backgroundColor = '#01ba3a';
			}

			_data = document.getElementById('_freq_ihsg')
			_data.innerHTML = '<font color="#1055cf">Freq</font> ' + _freq.formatMoney (0);
			_data = document.getElementById('_volume_ihsg')
			_data.innerHTML = '<font color="#1055cf">Vol</font> ' + _volume.formatMoney (0);
			_data = document.getElementById('_value_ihsg')
			
			if ( _value < 100000 )
			{
				_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " M";
			}
			else
			{
				_value = _value / 1000;
				_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " B";
			}
		}
	}
};

function portfolioPoll(first)
{
	if (first)
	{
		amq.addListener('index','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
		amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=COMPOSITE>' );
	}
}

// amq.uri='../amq';
// amq.addPollHandler(portfolioPoll);

function changeStock (obj)
{
	if ( (
			document.getElementById('_stockCode1').value != '' &&
			document.getElementById('_stockCode2').value != '' &&
			document.getElementById('_stockCode1').value == document.getElementById('_stockCode2').value
		 )
		||
		 (
			document.getElementById('_stockCode1').value != '' &&
			document.getElementById('_stockCode3').value != '' &&
		 	document.getElementById('_stockCode1').value == document.getElementById('_stockCode3').value
		 )
		||
		 (
			document.getElementById('_stockCode2').value != '' &&
			document.getElementById('_stockCode3').value != '' &&
			document.getElementById('_stockCode2').value == document.getElementById('_stockCode3').value
		 )
	)
	{
		obj.value = '';
	}
	else
	{
		if ( obj == document.getElementById('_stockCode1') )
		{
			if ( currentStockCode1.indexOf('.') == -1 )
			{
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode1 + '.RG' );
				amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode1 + '.RG' );
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode1 + '.RG' );
			}
			else
			{
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode1 );
				amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode1 );
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode1 );
			}

			currentStockCode1 = obj.value.toUpperCase();

			if ( currentStockCode1.indexOf('.') == -1 )
			{
				amq.addListener('ob', 'topic://ORDERBOOK.' + currentStockCode1 + '.RG', orderBookHandler._orderBookReceived);
				amq.addListener('tb', 'topic://TRADEBOOK.' + currentStockCode1 + '.RG', tradeBookHandler._tradeBookReceived);
				amq.addListener('qt', 'topic://QUOTES.' + currentStockCode1 + '.RG', priceHandler._price);
			}
			else
			{
				amq.addListener('ob', 'topic://ORDERBOOK.' + currentStockCode1 , orderBookHandler._orderBookReceived);
				amq.addListener('tb', 'topic://TRADEBOOK.' + currentStockCode1 , tradeBookHandler._tradeBookReceived);
				amq.addListener('qt', 'topic://QUOTES.' + currentStockCode1 , priceHandler._price);
			}

			amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + currentStockCode1 + '>' );
		}
		else if ( obj == document.getElementById('_stockCode2') )
		{
			if ( currentStockCode2.indexOf('.') == -1 )
			{
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode2 + '.RG' );
				amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode2 + '.RG' );
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode2 + '.RG' );
			}
			else
			{
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode2 );
				amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode2 );
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode2 );
			}

			currentStockCode2 = obj.value.toUpperCase();

			if ( currentStockCode2.indexOf('.') == -1 )
			{
				amq.addListener('ob','topic://ORDERBOOK.' + currentStockCode2 + '.RG', orderBookHandler._orderBookReceived);
				amq.addListener('tb','topic://TRADEBOOK.' + currentStockCode2 + '.RG', tradeBookHandler._tradeBookReceived);
				amq.addListener('qt','topic://QUOTES.' + currentStockCode2 + '.RG', priceHandler._price);
			}
			else
			{
				amq.addListener('ob','topic://ORDERBOOK.' + currentStockCode2 , orderBookHandler._orderBookReceived);
				amq.addListener('tb','topic://TRADEBOOK.' + currentStockCode2 , tradeBookHandler._tradeBookReceived);
				amq.addListener('qt','topic://QUOTES.' + currentStockCode2 , priceHandler._price);
			}

			amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + currentStockCode2 + '>' );		
		}
		else if ( obj == document.getElementById('_stockCode3') )
		{
			if ( currentStockCode3.indexOf('.') == -1 )
			{
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode3 + '.RG' );
				amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode3 + '.RG' );
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode3 + '.RG' );
			}
			else
			{
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode3 );
				amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode3 );
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode3 );
			}

			currentStockCode3 = obj.value.toUpperCase();

			if ( currentStockCode3.indexOf('.') == -1 )
			{
				amq.addListener('ob','topic://ORDERBOOK.' + currentStockCode3 + '.RG', orderBookHandler._orderBookReceived);
				amq.addListener('tb','topic://TRADEBOOK.' + currentStockCode3 + '.RG', tradeBookHandler._tradeBookReceived);
				amq.addListener('qt','topic://QUOTES.' + currentStockCode3 + '.RG', priceHandler._price);
			}
			else
			{
				amq.addListener('ob','topic://ORDERBOOK.' + currentStockCode3 , orderBookHandler._orderBookReceived);
				amq.addListener('tb','topic://TRADEBOOK.' + currentStockCode3 , tradeBookHandler._tradeBookReceived);
				amq.addListener('qt','topic://QUOTES.' + currentStockCode3 , priceHandler._price);
			}

			amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + currentStockCode3 + '>' );		
		}
	}
}

function removeStockFromListener ( stockCode )
{
	if ( stockCode.indexOf('.') == -1 )
	{
		amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + stockCode + '.RG' );
		amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + stockCode + '.RG' );
		amq.removeListener ( 'qt', 'topic://QUOTES.' + stockCode + '.RG' );
	}
	else
	{
		amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + stockCode );
		amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + stockCode );
		amq.removeListener ( 'qt', 'topic://QUOTES.' + stockCode );
	}
}

function unloadPage ()
{
	try
	{
		amq.removeAllListener();

		if ( currentStockCode1.indexOf('.') == -1 )
		{
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode1 + '.RG' );
			amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode1 + '.RG' );
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode1 + '.RG' );
		}
		else
		{
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode1 );
			amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode1 );
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode1 );
		}

		if ( currentStockCode2.indexOf('.') == -1 )
		{
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode2 + '.RG' );
			amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode2 + '.RG' );
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode2 + '.RG' );
		}
		else
		{
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode2 );
			amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode2 );
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode2 );
		}

		if ( currentStockCode3.indexOf('.') == -1 )
		{
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode3 + '.RG' );
			amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode3 + '.RG' );
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode3 + '.RG' );
		}
		else
		{
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode3 );
			amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode3 );
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode3 );
		}

		amq.removeListener ( 'ihsg', 'topic://QUOTES.COMPOSITE.INDICES');
	}
	catch (err) {}
}

function handleKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		obj.value = obj.value.toUpperCase();
		if ( obj.value.indexOf('.') == -1 )
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value + ':' ) >= 0 )
			{
				changeStock ( obj );
			}
			else
			{
				alertInvalid(obj);
			}
		}
		else
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value.substring ( 0, obj.value.indexOf('.') )  + ':' ) >= 0 )
			{
				var _board = obj.value.substring (obj.value.indexOf('.'));
				if ( _board == '.TN' || _board == '.NG' )
				{
					changeStock ( obj );
				}
				else
				{
					alertInvalid(obj);
				}
			}
			else
			{
				alertInvalid(obj);
			}
		}

		obj.select();
		obj.focus();
		return false;
	}
}
function alertInvalid(obj)
{
	if ( obj.name == '_stockCode1' )
	{
		document.getElementById('_stockName1').innerHTML = 'Invalid Stock Code';
	}
	else if ( obj.name == '_stockCode2' )
	{
		document.getElementById('_stockName2').innerHTML = 'Invalid Stock Code';
	}
	else if ( obj.name == '_stockCode3' )
	{
		document.getElementById('_stockName3').innerHTML = 'Invalid Stock Code';
	}
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};


function startScreen()
{
	amq.addListener('ihsg','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
	amq.sendMessage('topic://REQUEST.FIRST', '<c s=COMPOSITE>');
}
function stopScreen()
{
	try
	{
		amq.removeAllListener();
		// ihsgHandler.stopIhsg();
	}
	catch (err) {}
}
