function loadData()
{
	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'Loading..';
	_viewButton.disabled = true;

	var _dateFrom = document.getElementById('popupDatepicker').value;
	var _dateTo = document.getElementById('popupDatepicker2').value;
	var _stock = document.getElementById('_stockCode').value;
	makeRequest('../servlets/SummaryServlet?_a=dt_&_df=' + _dateFrom + '&_dt=' + _dateTo + '&_st=' + _stock, false);
}
function receivedData(result)
{
	var _data = result.split('|');
	var i;
	var bFreq=0,bVol=0,bVal=0,sFreq=0,sVol=0,sVal=0;
	var bTotal=1,sTotal=1;
	var _subdata = _data[0].split('#');
	document.getElementById('_stockName').innerHTML = _subdata[0];
	document.getElementById('_current').innerHTML = _subdata[1];

	var _last = parseInt(_subdata[2]);
	var _prev = parseInt(_subdata[3]);
	document.getElementById('_last').innerHTML = parseInt(_subdata[2]).formatMoney(0);

	var _c=getColor(_prev,_last);
	if (_prev<=_last)
	{
		document.getElementById('_chg').innerHTML = '<span class="'+_c+'">'+(_last-_prev).formatMoney(0)+'</span>';
		document.getElementById('_persenchg').innerHTML = '<span class="'+_c+'">'+((_last-_prev)*100/_prev).formatMoney(2)+'</span>';
	}
	else
	{
		document.getElementById('_chg').innerHTML = '<span class="col_red">-'+(_prev-_last).formatMoney(0)+'</span>';
		document.getElementById('_persenchg').innerHTML = '<span class="col_red">-'+((_prev-_last)*100/_prev).formatMoney(2)+'</span>';
	}

	var t = parseInt(_subdata[4]);
	document.getElementById('_open').innerHTML = '<span class="'+getColor(_prev,t)+'">'+t.formatMoney(0)+'</span>';
	t = parseInt(_subdata[5]);
	document.getElementById('_high').innerHTML = '<span class="'+getColor(_prev,t)+'">'+t.formatMoney(0)+'</span>';
	t = parseInt(_subdata[6]);
	document.getElementById('_low').innerHTML = '<span class="'+getColor(_prev,t)+'">'+t.formatMoney(0)+'</span>';
	t = parseFloat(_subdata[7]);
	document.getElementById('_avg').innerHTML = '<span class="'+getColor(_prev,t)+'">'+t.formatMoney(2)+'</span>';
	t = parseInt(_subdata[8]);
	document.getElementById('_freq').innerHTML = '<span class="col_grey">'+t.formatMoney(0)+'</span>';
	bTotal = parseInt(_subdata[9]);
	sTotal = parseInt(_subdata[10]);

	clearTable();
	// var _tsorter = '';
	var _tsorter = document.getElementById("tsorter");
	for(i=1;i<_data.length;++i)
	{
		try
		{
			if (_data[i].length <= 3) continue;
			_subdata = _data[i].split('#');
			//if (i%2==0) _tsorter += '<tr class="stock_running_detail_white">';
			//else _tsorter += '<tr class="stock_running_detail_grey">';
			_tr = _tsorter.insertRow(_tsorter.rows.length);
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			// _tsorter += '<td>&nbsp;</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = "<div align='center'>"+_subdata[0]+"</div>";
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><div align="center">'+_subdata[0]+'</div></td>';
			bFreq = parseInt(_subdata[1]);
			bVol = parseInt(_subdata[2]);
			bVal = parseInt(_subdata[3]);
			sFreq = parseInt(_subdata[4]);
			sVol = parseInt(_subdata[5]);
			sVal = parseInt(_subdata[6]);

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = bFreq.formatMoney(0);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+bFreq.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = bVol.formatMoney(0);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+bVol.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = (bVal/(bVol*_LOT_SIZE)).formatMoney(2);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+(bVal/(bVol*_LOT_SIZE)).formatMoney(2)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = (bVol*100/bTotal).formatMoney(3);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+(bVol*100/bTotal).formatMoney(3)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = sFreq.formatMoney(0);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+sFreq.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = sVol.formatMoney(0);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+sVol.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = (sVal/(sVol*_LOT_SIZE)).formatMoney(2);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+(sVal/(sVol*_LOT_SIZE)).formatMoney(2)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = (sVol*100/sTotal).formatMoney(3);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+(sVol*100/sTotal).formatMoney(3)+'</td>';
			if (bVol>=sVol)
			{
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = '<span class="col_green">'+(bVol-sVol).formatMoney(0)+'</span>';
				// _tsorter += '<td><span class="col_green">'+(bVol-sVol).formatMoney(0)+'</span></td>';
			}
			else
			{
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = '<span class="col_red">-'+(sVol-bVol).formatMoney(0)+'</span>';
				// _tsorter += '<td><span class="col_red">-'+(sVol-bVol).formatMoney(0)+'</span></td>';
			}
			// _tsorter += '</tr>';
		}
		catch(e){}
	}
	// document.getElementById('tsorter').innerHTML = _tsorter;
	$("table").trigger("update");

	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'View';
	_viewButton.disabled = false;
}
function clearTable()
{
	var tbody = document.getElementById("tsorter");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
function makeRequest(url)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			// set type accordingly to anticipated content type
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	http_request.onreadystatechange = alertContents;
	http_request.open('GET', url, true);
	http_request.send(null);
}
function alertContents()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			receivedData(http_request.responseText);
		}
	}
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};
function getColor(prev,last)
{
	if (prev>last) return 'col_red';
	else if (prev<last) return 'col_green';
	return 'col_yellow';
}
function loadTodayString()
{
	var d = new Date();
	var _todayString = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
	document.getElementById("popupDatepicker").value = _todayString;
	document.getElementById("popupDatepicker2").value = _todayString;
}
