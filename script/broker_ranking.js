function loadData()
{
	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'Load';
	_viewButton.disabled = true;

	var _date = document.getElementById('popupDatepicker').value;
	makeRequest('../servlets/SummaryServlet?_a=br&_d=' + _date);
}
function receivedData(result)
{
	var _data = result.split('|');
	var i;
	clearTable();
	var _tsorter = document.getElementById("tsorter");
	document.getElementById('_current').innerHTML = _data[0];
	var bVol=0,bVal=0,sVol=0,sVal=0;
	for(i=1;i<_data.length;++i)
	{
		try
		{
			if (_data[i].length <= 3) continue;
			_subdata = _data[i].split('#');
			//if (i%2==0) _tsorter += '<tr class="stock_running_detail_white">';
			//else _tsorter += '<tr class="stock_running_detail_grey">';

			_tr = _tsorter.insertRow(_tsorter.rows.length);
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = '&nbsp;';
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">&nbsp;</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;vertical-align:top;');
			_td.innerHTML = "<div align='center'>"+_subdata[0]+"</div>";
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><div align="center">'+_subdata[0]+'</div></td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;padding-left:2px;');
			_td.innerHTML = "<div align='left'>"+_subdata[1]+"</div>";
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;padding-left:2px;"><div align="left">'+_subdata[1]+'</div></td>';

			bVol = parseInt(_subdata[2]);
			bVal = parseInt(_subdata[3]);
			sVol = parseInt(_subdata[4]);
			sVal = parseInt(_subdata[5]);

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = (bVol+sVol).formatMoney(0);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+(bVol+sVol).formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = ((bVal+sVal)/1000000).formatMoney(2);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+((bVal+sVal)/1000000).formatMoney(2)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = bVol.formatMoney(0);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+bVol.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = (bVal/1000000).formatMoney(2);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+(bVal/1000000).formatMoney(2)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = sVol.formatMoney(0);
			//_tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+sVol.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = (sVal/1000000).formatMoney(2);
			// _tsorter += '<td>'+(sVal/1000000).formatMoney(2)+'</td>';
			// _tsorter += '</tr>';
		}
		catch(e){}
	}
	// document.getElementById('tsorter').innerHTML = _tsorter;
	$("table").trigger("update");

	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'View';
	_viewButton.disabled = false;
}
function clearTable()
{
	var tbody = document.getElementById("tsorter");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
function makeRequest(url)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			// set type accordingly to anticipated content type
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	http_request.onreadystatechange = alertContents;
	http_request.open('GET', url, true);
	http_request.send(null);
}
function alertContents()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			// sortReady();
			receivedData(http_request.responseText);
		}
		else if ( http_request.status == 302 )
		{
			//alert ('New Location [' + http_request.getResponseHeader('Location') + ']');
		}
		else
		{
			//alert('There was a problem with the request. [' + http_request.status + ']');
		}
	}
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};
function loadTodayString()
{
	var d = new Date();
	var _todayString = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
	document.getElementById("popupDatepicker").value = _todayString;
}
