var currentStockCode1 = '-'
var currentStockCode2 = '-'
var currentStockCode3 = '-'
var _prev = 0

var priceHandler = 
{
	_price: function(message) 
	{
		if (message != null)
		{
			// console.log ( message );
			try
			{
				// quotes
				var _stock = message.getAttribute ( '_s' ).toUpperCase();
				var _last = parseInt ( message.getAttribute ( '_l' ) )
				_prev = parseInt ( message.getAttribute ( '_p' ) )
				var _open = parseInt ( message.getAttribute ( '_o' ) )
				var _high = parseInt ( message.getAttribute ( '_hi' ) )
				var _low = parseInt ( message.getAttribute ( '_lo' ) )
				var _freq = parseInt ( message.getAttribute ( '_f' ) )
				var _vol = parseInt ( message.getAttribute ( '_vo' ) ) / _LOT_SIZE;
				var _val = parseInt ( message.getAttribute ( '_va' ) ) / 1000000
				var _chg = Math.abs ( _last - _prev )
				if ( _last == 0 ) _chg = 0;
				var _persenchg = (_chg / _prev) * 100;
				var _stockName = message.getAttribute ( '_n' )

				var _bookNo = '1';
				if ( _stock == currentStockCode1 )
				{
					_bookNo = '1';
				}
				else if ( _stock == currentStockCode2 )
				{
					_bookNo = '2';
				}
				else if ( _stock == currentStockCode3 )
				{
					_bookNo = '3';
				}
				else
				{
					return false;
				}

				if ( _stockName != null )
				{
					document.getElementById ("_stockName" + _bookNo).innerHTML = _stockName;
				}

				var _eStyle = "col_yellow";
				var _eChg = "";

				if ( _last < _prev )
				{
					_eStyle = "col_red";
					_eChg = "&nbsp; ";
				}
				else if ( _last > _prev )
				{
					_eStyle = "col_green";
					_eChg = "&nbsp; ";
				}
				else if ( _last == _prev )
				{
					_eStyle = "col_yellow";
					_eChg = "&nbsp;";
				}

				var _element = document.getElementById ("_last" + _bookNo);
				_element.innerHTML = _last.formatMoney (0);
				_element.className = _eStyle;
			
				_element = document.getElementById ("_chg" + _bookNo);
				_element.innerHTML = _eChg + " " + _chg.formatMoney (0);
				_element.className = _eStyle;
			
				_element = document.getElementById ("_prev" + _bookNo);
				_element.innerHTML = _prev.formatMoney (0);
				_element.className = "col_yellow";
			
				_element = document.getElementById ("_persenchg" + _bookNo);
				_element.innerHTML = _persenchg.formatMoney (2);
				_element.className = _eStyle;
			
				_element = document.getElementById ("_open" + _bookNo);
				_element.innerHTML = _open.formatMoney (0);			
				if ( _open < _prev ) _element.className = "col_red";
				else if ( _open > _prev ) _element.className = "col_green";
				else if ( _open == _prev ) _element.className = "col_yellow";
			
				_element = document.getElementById ("_high" + _bookNo);
				_element.innerHTML = _high.formatMoney (0);			
				if ( _high < _prev ) _element.className = "col_red";
				else if ( _high > _prev ) _element.className = "col_green";
				else if ( _high == _prev ) _element.className = "col_yellow";

				_element = document.getElementById ("_low" + _bookNo);
				_element.innerHTML = _low.formatMoney (0);			
				if ( _low < _prev ) _element.className = "col_red";
				else if ( _low > _prev ) _element.className = "col_green";
				else if ( _low == _prev ) _element.className = "col_yellow";

				_element = document.getElementById ("_freq" + _bookNo);
				_element.innerHTML = _freq.formatMoney (0);
				//_element.className = "black_color";

				_element = document.getElementById ("_vol" + _bookNo);
				_element.innerHTML = _vol.formatMoney (0);
				//_element.className = "black_color";

				_element = document.getElementById ("_val" + _bookNo);
				if ( _val < 100000 )
				{
					_element.innerHTML = _val.formatMoney(2) + " M";
				}
				else			
				{
					_val /= 1000;
					_element.innerHTML = _val.formatMoney(2) + " B";
				}
				// _element.className = "black_color";
			}
			catch (err) {}
		}
	}
};

var orderBookHandler = 
{
	_orderBookReceived: function(message)
	{
		if (message != null)
		{
			// console.log ( message );
			try
			{
				// alert ( message );
				var _stock = message.getAttribute ( '_s' ).toUpperCase();
				var _prev = parseInt ( message.getAttribute ( '_p' ) )
				var _bookNo = '1';
				if ( _stock == currentStockCode1 )
				{
					_bookNo = '1';
				}
				else if ( _stock == currentStockCode2 )
				{
					_bookNo = '2';
				}
				else if ( _stock == currentStockCode3 )
				{
					_bookNo = '3';
				}
				else
				{
					removeStockFromListener ( _stock );
					return false;
				}

				// order book
				var i = 0
				var _eStyle = "col_yellow";
				for (i = 0; i < 5; ++i)
				{			
					var _bidValMsg = parseInt ( message.getAttribute ( '_ba' + i ) )
					var _bidVolMsg = parseInt ( message.getAttribute ( '_bo' + i ) )

					var _bidVol = document.getElementById ("_bidVol" + _bookNo + "" + i);
					var _bidVal = document.getElementById ("_bidVal" + _bookNo + "" + i);

					if ( _bidVolMsg != null && _bidValMsg != null )
					{
						if ( _bidValMsg < _prev ) _eStyle = "col_red";
						else if ( _bidValMsg > _prev ) _eStyle = "col_green";
						else if ( _bidValMsg == _prev ) _eStyle = "col_yellow";

						_bidVol.className = _eStyle;
						_bidVal.className = _eStyle;
						_bidVol.innerHTML = _bidVolMsg.formatMoney (0);
						_bidVal.innerHTML = _bidValMsg.formatMoney (0);
					}

					var _offerValMsg = parseInt ( message.getAttribute ( '_oa' + i ) )
					var _offerVolMsg = parseInt ( message.getAttribute ( '_oo' + i ) )

					var _offerVol = document.getElementById ("_offerVol" + _bookNo + "" + i);
					var _offerVal = document.getElementById ("_offerVal" + _bookNo + "" + i);

					if ( _offerVolMsg != null && _offerValMsg != null )
					{
						if ( _offerValMsg < _prev ) _eStyle = "col_red";
						else if ( _offerValMsg > _prev ) _eStyle = "col_green";
						else if ( _offerValMsg == _prev ) _eStyle = "col_yellow";

						_offerVol.className = _eStyle;
						_offerVal.className = _eStyle;
						_offerVol.innerHTML = _offerVolMsg.formatMoney (0);
						_offerVal.innerHTML = _offerValMsg.formatMoney (0);
					}
				}
			}
			catch (err) {}
		}
	}
};

var ihsgHandler = 
{
	_ihsg: function(message)
	{
		if (message != null) {
			//try
			{
				var _composite = parseFloat ( message.getAttribute ( '_c' ) )
				var _prev = parseFloat ( message.getAttribute ( '_p' ) )
				var _freq = parseInt ( message.getAttribute ( '_f' ) )
				var _volume = parseFloat ( message.getAttribute ( '_vo' ) )
				var _value = parseFloat ( message.getAttribute ( '_va' ) ) / 1000000
				var _chg = Math.abs ( _composite - _prev );
				var _persenchg = (_chg / _prev) * 100;
			
				var _data = document.getElementById('_ihsg_ihsg');
				_data.className = "col_black";
				_data.innerHTML = _composite.formatMoney (2);

				var _ihsg_title = document.getElementById('header_IHSG_Title');
				var _data_chg = document.getElementById('_chg_ihsg');
				var _data_persen_chg = document.getElementById('_persen_ihsg');
				_data_chg.innerHTML = _chg.toFixed(2);
				_data_persen_chg.innerHTML = _persenchg.toFixed(2) + " %";
				if ( _composite < _prev )
				{
					_data_chg.className = "col_red";
					_data_persen_chg.className = "col_red";
					_ihsg_title.style.backgroundColor = '#e54044';
				}
				else if (_composite > _prev  )
				{
					_data_chg.className = "col_green";
					_data_persen_chg.className = "col_green";
					_ihsg_title.style.backgroundColor = '#01ba3a';
				}
				else if ( _composite == _prev  )
				{
					_data_chg.className = "col_yellow";
					_data_persen_chg.className = "col_yellow";
					_ihsg_title.style.backgroundColor = '#01ba3a';
				}

				_data = document.getElementById('_freq_ihsg')
				_data.innerHTML = '<font color="#1055cf">Freq</font> ' + _freq.formatMoney (0);
				_data = document.getElementById('_volume_ihsg')
				_data.innerHTML = '<font color="#1055cf">Vol</font> ' + _volume.formatMoney (0);
				_data = document.getElementById('_value_ihsg')
			
				if ( _value < 100000 )
				{
					_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " M";
				}
				else
				{
					_value = _value / 1000;
					_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " B";
				}
			}
			// catch (err) {}
		}
	}
};

var _row = 0;
var _rowColor = "col_black";
var _maxRow = 21;

var runningTradeHandler = 
{
	_runningTrade: function(message) 
	{
		if (message != null) {
			try
			{
				var _time = message.getAttribute('_t')
				var _stock = message.getAttribute('_s')
				var _price = parseInt ( message.getAttribute('_p') )
				var _qty = parseInt ( message.getAttribute('_q') )
				var _last = parseInt ( message.getAttribute('_l') )
				var _chg = Math.abs ( _price - _last )
				if ( _last == 0 ) _chg = 0;
				var _persenchg = (_chg / _last) * 100;

				var _buyerCode = message.getAttribute('_bc')
				var _buyerType = message.getAttribute('_bt')
				var _sellerCode = message.getAttribute('_sc')
				var _sellerType = message.getAttribute('_st')

				previousRow = (_row - 1 + _maxRow) % _maxRow
				var prow = document.getElementById('row' + previousRow)

				if ( previousRow % 2 == 0 )
				{
					prow.className = 'stock_running_detail_grey';
				}
				else
				{
					prow.className = 'stock_running_detail_white';
				}

				var row = document.getElementById ('row' + _row);
				row.className = 'stock_running_detail_active';

				row = document.getElementById('_time_row' + _row)
				row.innerHTML = "<span class='" + _rowColor + "'>" + _time + "</span>";

				row = document.getElementById('_stock_row' + _row)
				row.innerHTML = "&nbsp;&nbsp;<span class='" + _rowColor + "'>" + _stock + "</span>";
				row.style.textAlign = 'left';

				var priceRow = document.getElementById('_price_row' + _row)
				// var imgChgRow = document.getElementById('_imgchg_row' + _row)
				var chgRow = document.getElementById('_chg_row' + _row)
				var persenchgRow = document.getElementById('_persenchg_row' + _row)
				// priceRow.innerHTML = "<span class='black_color'>" + _price.formatMoney (0) + "</span>";

				if ( _price < _last )
				{
					priceRow.innerHTML = "<span class='col_red'>" + _price.formatMoney (0) + "</span>";
					chgRow.innerHTML = "<span class='col_red'>" + _chg.formatMoney (0) + "</span>";
					persenchgRow.innerHTML = "<span class='col_red'>" + _persenchg.formatMoney (2) + "</span>";
				}
				else if ( _price > _last )
				{
					priceRow.innerHTML = "<span class='col_green'>" + _price.formatMoney (0) + "</span>";
					chgRow.innerHTML = "<span class='col_green'>" + _chg.formatMoney (0) + "</span>";
					persenchgRow.innerHTML = "<span class='col_green'>" + _persenchg.formatMoney (2) + "</span>";
				}
				else if ( _price == _last )
				{
					priceRow.innerHTML = "<span class='col_yellow'>" + _price.formatMoney (0) + "</span>";
					chgRow.innerHTML = "<span class='col_yellow'>" + _chg.formatMoney (0) + "</span>";
					persenchgRow.innerHTML = "<span class='col_yellow'>" + _persenchg.formatMoney (2) + "</span>";
				}


				row = document.getElementById('_qty_row' + _row)
				row.innerHTML = "<span class='col_black'>" + _qty.formatMoney(0) + "</span>";

				row = document.getElementById('_buyerType_row' + _row)
				if ( _buyerType == 'D' )
				{
					row.innerHTML = "<span class='col_green'>" + _buyerType + "</span>";
					row = document.getElementById('_buyerCode_row' + _row)
					row.innerHTML = "<span class='col_green'>" + _buyerCode + "</span>";
				}
				else
				{
					row.innerHTML = "<span class='col_red'>" + _buyerType + "</span>";
					row = document.getElementById('_buyerCode_row' + _row)
					row.innerHTML = "<span class='col_red'>" + _buyerCode + "</span>";
				}

				row = document.getElementById('_sellerType_row' + _row)
				if ( _sellerType == 'D' )
				{
					row.innerHTML = "<span class='col_green'>" + _sellerType + "</span>";
					row = document.getElementById('_sellerCode_row' + _row)
					row.innerHTML = "<span class='col_green'>" + _sellerCode + "</span>";
				}
				else
				{
					row.innerHTML = "<span class='col_red'>" + _sellerType + "</span>";
					row = document.getElementById('_sellerCode_row' + _row)
					row.innerHTML = "<span class='col_red'>" + _sellerCode + "</span>";
				}

				++_row;
				if ( _row >= _maxRow )
				{
					_row = 0;

					if ( _rowColor == 'col_black' ) _rowColor = 'col_blue';
					else _rowColor = 'col_black';
				}
			}
			catch (err) {}
		}
	}
};

function portfolioPoll(first)
{
	if (first)
	{
		try
		{
			//amq.addListener('index','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
			//amq.addListener('trade','topic://RUNNING.TRADE',runningTradeHandler._runningTrade);
			// amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=COMPOSITE>' );

			amq.addListener('in','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
			amq.addListener('rt','topic://RUNNING.TRADE',runningTradeHandler._runningTrade);
			amq.sendMessage('topic://REQUEST.FIRST', '<c s=COMPOSITE>');
		}
		catch (err){}
	}
}

// amq.uri='../amq';
// amq.addPollHandler(portfolioPoll);

function changeStock (obj)
{
	try
	{
		if ( obj.name == '_stockCode1' )
		{
			if ( obj.value == currentStockCode2 )
			{
				obj.value = '';
				return;
			}
			if ( obj.value == currentStockCode3 )
			{
				obj.value = '';
				return;
			}

			if ( currentStockCode1.indexOf('.') == -1 )
			{
				//amq.removeListener ( 'orderBook' + currentStockCode1 + '.RG', 'topic://ORDERBOOK.' + currentStockCode1 + '.RG' );
				//amq.removeListener ( 'quotes' + currentStockCode1 + '.RG', 'topic://QUOTES.' + currentStockCode1 + '.RG' );
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode1 + '.RG' );
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode1 + '.RG' );
			}
			else
			{
				//amq.removeListener ( 'orderBook' + currentStockCode1, 'topic://ORDERBOOK.' + currentStockCode1);
				//amq.removeListener ( 'quotes' + currentStockCode1, 'topic://QUOTES.' + currentStockCode1);
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode1);
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode1);
			}

			currentStockCode1 = obj.value.toUpperCase();
			if ( currentStockCode1.indexOf('.') == -1 )
			{
				//amq.addListener('orderBook' + currentStockCode1 + '.RG','topic://ORDERBOOK.' + currentStockCode1 + '.RG', orderBookHandler._orderBookReceived);
				//amq.addListener('quotes' + currentStockCode1 + '.RG','topic://QUOTES.' + currentStockCode1 + '.RG', priceHandler._price);
				amq.addListener('ob','topic://ORDERBOOK.' + currentStockCode1 + '.RG', orderBookHandler._orderBookReceived);
				amq.addListener('qt','topic://QUOTES.' + currentStockCode1 + '.RG', priceHandler._price);
			}
			else
			{
				//amq.addListener('orderBook' + currentStockCode1,'topic://ORDERBOOK.' + currentStockCode1, orderBookHandler._orderBookReceived);
				//amq.addListener('quotes' + currentStockCode1,'topic://QUOTES.' + currentStockCode1, priceHandler._price);
				amq.addListener('ob', 'topic://ORDERBOOK.' + currentStockCode1, orderBookHandler._orderBookReceived);
				amq.addListener('qt', 'topic://QUOTES.' + currentStockCode1, priceHandler._price);
			}
			
			amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + currentStockCode1 + '>' );
			clearOrderBook ('1');
		}
		// else if ( obj == document.getElementById('_stockCode2') )
		else if ( obj.name == '_stockCode2' )
		{
			if ( obj.value == currentStockCode1 )
			{
				obj.value = '';
				return;
			}
			if ( obj.value == currentStockCode3 )
			{
				obj.value = '';
				return;
			}

			if ( currentStockCode2.indexOf('.') == -1 )
			{
				//amq.removeListener ( 'orderBook' + currentStockCode2 + '.RG', 'topic://ORDERBOOK.' + currentStockCode2 + '.RG' );
				//amq.removeListener ( 'quotes' + currentStockCode2 + '.RG', 'topic://QUOTES.' + currentStockCode2 + '.RG' );
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode2 + '.RG' );
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode2 + '.RG' );
			}
			else
			{
				//amq.removeListener ( 'orderBook' + currentStockCode2, 'topic://ORDERBOOK.' + currentStockCode2);
				//amq.removeListener ( 'quotes' + currentStockCode2, 'topic://QUOTES.' + currentStockCode2);
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode2);
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode2);
			}

			currentStockCode2 = obj.value.toUpperCase();
			if ( currentStockCode2.indexOf('.') == -1 )
			{
				//amq.addListener('orderBook' + currentStockCode2 + '.RG','topic://ORDERBOOK.' + currentStockCode2 + '.RG', orderBookHandler._orderBookReceived);
				//amq.addListener('quotes' + currentStockCode2 + '.RG','topic://QUOTES.' + currentStockCode2 + '.RG', priceHandler._price);
				amq.addListener('ob','topic://ORDERBOOK.' + currentStockCode2 + '.RG', orderBookHandler._orderBookReceived);
				amq.addListener('qt','topic://QUOTES.' + currentStockCode2 + '.RG', priceHandler._price);
			}
			else
			{
				//amq.addListener('orderBook' + currentStockCode2,'topic://ORDERBOOK.' + currentStockCode2, orderBookHandler._orderBookReceived);
				//amq.addListener('quotes' + currentStockCode2,'topic://QUOTES.' + currentStockCode2, priceHandler._price);
				amq.addListener('ob' ,'topic://ORDERBOOK.' + currentStockCode2, orderBookHandler._orderBookReceived);
				amq.addListener('qt' ,'topic://QUOTES.' + currentStockCode2, priceHandler._price);
			}

			amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + currentStockCode2 + '>' );
			clearOrderBook ('2');
		}
		// else if ( obj == document.getElementById('_stockCode3') )
		else if ( obj.name = '_stockCode3' )
		{
			if ( obj.value == currentStockCode1 )
			{
				obj.value = '';
				return;
			}
			if ( obj.value == currentStockCode2 )
			{
				obj.value = '';
				return;
			}

			if ( currentStockCode3.indexOf('.') == -1 )
			{
				// amq.removeListener ( 'orderBook' + currentStockCode3 + '.RG', 'topic://ORDERBOOK.' + currentStockCode3 + '.RG' );
				// amq.removeListener ( 'quotes' + currentStockCode3 + '.RG', 'topic://QUOTES.' + currentStockCode3 + '.RG' );
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode3 + '.RG' );
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode3 + '.RG' );
			}
			else
			{
				//amq.removeListener ( 'orderBook' + currentStockCode3, 'topic://ORDERBOOK.' + currentStockCode3);
				//amq.removeListener ( 'quotes' + currentStockCode3, 'topic://QUOTES.' + currentStockCode3);
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode3);
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode3);
			}

			currentStockCode3 = obj.value.toUpperCase();
			if ( currentStockCode3.indexOf('.') == -1 )
			{
				//amq.addListener('orderBook' + currentStockCode3 + '.RG','topic://ORDERBOOK.' + currentStockCode3 + '.RG', orderBookHandler._orderBookReceived);
				//amq.addListener('quotes' + currentStockCode3 + '.RG','topic://QUOTES.' + currentStockCode3 + '.RG', priceHandler._price);
				amq.addListener('ob','topic://ORDERBOOK.' + currentStockCode3 + '.RG', orderBookHandler._orderBookReceived);
				amq.addListener('qt','topic://QUOTES.' + currentStockCode3 + '.RG', priceHandler._price);
			}
			else
			{
				//amq.addListener('orderBook' + currentStockCode3,'topic://ORDERBOOK.' + currentStockCode3, orderBookHandler._orderBookReceived);
				//amq.addListener('quotes' + currentStockCode3,'topic://QUOTES.' + currentStockCode3, priceHandler._price);
				amq.addListener('ob', 'topic://ORDERBOOK.' + currentStockCode3, orderBookHandler._orderBookReceived);
				amq.addListener('qt', 'topic://QUOTES.' + currentStockCode3, priceHandler._price);
			}

			amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + currentStockCode3 + '>' );
			clearOrderBook ('3');
		}

		var s1 = document.getElementById('_stockCode1').value;
		var s2 = document.getElementById('_stockCode2').value;
		var s3 = document.getElementById('_stockCode3').value;
		setCookie ('savestock', s1 + '|' + s2 + '|' + s3);
		// alert ('Save ' + getCookie('savestock'));
	}
	catch (err) {}
}

function removeStockFromListener ( stockCode )
{
	try
	{
		//amq.removeListener ( 'orderBook' + stockCode + '.RG', 'topic://ORDERBOOK.' + stockCode + '.RG' );
		//amq.removeListener ( 'quotes' + stockCode + '.RG', 'topic://QUOTES.' + stockCode + '.RG' );
		amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + stockCode + '.RG' );
		amq.removeListener ( 'qt', 'topic://QUOTES.' + stockCode + '.RG' );
	}
	catch (err) {}
}

function unloadPage ()
{
	try
	{
		amq.removeListener ( 'rt', 'topic://RUNNING.TRADE');
		amq.removeListener ( 'in', 'topic://QUOTES.COMPOSITE.INDICES');
		amq.removeAllListener();

		if ( currentStockCode1.indexOf('.') == -1 )
		{
			//amq.removeListener ( 'orderBook' + currentStockCode1 + '.RG', 'topic://ORDERBOOK.' + currentStockCode1 + '.RG' );
			//amq.removeListener ( 'quotes' + currentStockCode1 + '.RG', 'topic://QUOTES.' + currentStockCode1 + '.RG' );
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode1 + '.RG' );
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode1 + '.RG' );
		}
		else
		{
			//amq.removeListener ( 'orderBook' + currentStockCode1, 'topic://ORDERBOOK.' + currentStockCode1);
			//amq.removeListener ( 'quotes' + currentStockCode1, 'topic://QUOTES.' + currentStockCode1);
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode1);
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode1);
		}

		if ( currentStockCode2.indexOf('.') == -1 )
		{
			//amq.removeListener ( 'orderBook' + currentStockCode2 + '.RG', 'topic://ORDERBOOK.' + currentStockCode2 + '.RG' );
			//amq.removeListener ( 'quotes' + currentStockCode2 + '.RG', 'topic://QUOTES.' + currentStockCode2 + '.RG' );
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode2 + '.RG' );
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode2 + '.RG' );
		}
		else
		{
			//amq.removeListener ( 'orderBook' + currentStockCode2, 'topic://ORDERBOOK.' + currentStockCode2);
			//amq.removeListener ( 'quotes' + currentStockCode2, 'topic://QUOTES.' + currentStockCode2);
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode2);
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode2);
		}

		if ( currentStockCode3.indexOf('.') == -1 )
		{
			//amq.removeListener ( 'orderBook' + currentStockCode3 + '.RG', 'topic://ORDERBOOK.' + currentStockCode3 + '.RG' );
			//amq.removeListener ( 'quotes' + currentStockCode3 + '.RG', 'topic://QUOTES.' + currentStockCode3 + '.RG' );
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode3 + '.RG' );
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode3 + '.RG' );
		}
		else
		{
			//amq.removeListener ( 'orderBook' + currentStockCode3, 'topic://ORDERBOOK.' + currentStockCode3);
			//amq.removeListener ( 'quotes' + currentStockCode3, 'topic://QUOTES.' + currentStockCode3);
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode3);
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode3);
		}

		// amq.removeListener ( 'trade', 'topic://RUNNING.TRADE');
		// amq.removeListener ( 'index', 'topic://QUOTES.COMPOSITE.INDICES');

		amq.removeAllListener();
	}
	catch (err) {}
}

function handleKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		obj.value = obj.value.toUpperCase();
		if ( obj.value.indexOf('.') == -1 )
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value + ':' ) >= 0 )
			{
				changeStock ( obj );
			}
			else
			{
				alertInvalid (obj);
			}
		}
		else
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value.substring ( 0, obj.value.indexOf('.') )  + ':' ) >= 0 )
			{
				var _board = obj.value.substring (obj.value.indexOf('.'));
				if ( _board == '.TN' || _board == '.NG' )
				{
					changeStock ( obj );
				}
				else
				{
					alertInvalid (obj);
				}
			}
			else
			{
				alertInvalid (obj);
			}
		}

		obj.select();
		obj.focus();
		return false;
	}
}
function alertInvalid(obj)
{
	if ( obj.name == '_stockCode1' )
	{
		document.getElementById('_stockName1').innerHTML = 'Invalid Stock Code';
	}
	else if ( obj.name == '_stockCode2' )
	{
		document.getElementById('_stockName2').innerHTML = 'Invalid Stock Code';
	}
	else if ( obj.name == '_stockCode3' )
	{
		document.getElementById('_stockName3').innerHTML = 'Invalid Stock Code';
	}
}

Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};
function clearOrderBook(nomor)
{
	var i = 0;
	for (i = 0; i < 4; ++i)
	{
		document.getElementById ("_bidVol" + nomor + "" + i).innerHTML = "&nbsp;";
		document.getElementById ("_bidVal" + nomor + "" + i).innerHTML = "&nbsp;";
		document.getElementById ("_offerVol" + nomor + "" + i).innerHTML = "&nbsp;";
		document.getElementById ("_offerVal" + nomor + "" + i).innerHTML = "&nbsp;";
	}
}


function loadStockCodeCookies()
{
	try
	{
		// var _info=_b.split('|');
		var _info = getCookie ('savestock').split('|');
		if ( _info.length == 3 )
		{
			if ( _info[0].length > 1 )
			{
				//currentStockCode[0] = _info[0];
				document.getElementById('_stockCode1').value = _info[0];
				changeStock ( document.getElementById('_stockCode1') );
			}

			if ( _info[1].length > 1 )
			{
				//currentStockCode[1] = _info[1];
				document.getElementById('_stockCode2').value = _info[1];
				changeStock ( document.getElementById('_stockCode2') );
			}

			if ( _info[2].length > 1 )
			{
				//currentStockCode[2] = _info[2];
				document.getElementById('_stockCode3').value = _info[2];
				changeStock ( document.getElementById('_stockCode3') );
			}
		}

		
	}
	catch (err) {}
}


function startScreen()
{
			amq.addListener('ihsg','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
			amq.addListener('rt','topic://RUNNING.TRADE',runningTradeHandler._runningTrade);
			amq.sendMessage('topic://REQUEST.FIRST', '<c s=COMPOSITE>');

			loadStockCodeCookies();
}
function stopScreen()
{
	try
	{
		amq.removeAllListener();
		// ihsgHandler.stopIhsg();
	}
	catch (err) {}
}
