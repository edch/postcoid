function loadData()
{
	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'Loading..';
	_viewButton.disabled = true;
	var _date = document.getElementById('popupDatepicker').value;
	var _stock = document.getElementById('_stockCode').value;
	makeRequest('../servlets/SummaryServlet?_a=dt&_d=' + _date + '&_st=' + _stock);
}
function receivedData(result)
{
	var _data=result.split('|');
	var i,t=0;
	var _c='';
	var _subdata = _data[0].split('#');
	document.getElementById('_stockName').innerHTML = _subdata[0];
	document.getElementById('_current').innerHTML = _subdata[1];
	var _prev = parseInt(_data[1]);
	// var _tsorter='';
	// var _brokerSummary = '',_tradeBook = '';

	clearTable();
	var _tsorter = document.getElementById('tsorter');
	var _brokerSummary = document.getElementById('_brokerSummary');
	var _tradeBook = document.getElementById('_tradeBook');

	for(i=2;i<_data.length;++i)
	{
		try
		{
			if (_data[i].length <= 3) continue;
			_subdata = _data[i].split('#');

			// _tsorter += '<tr>';
			if ( _subdata[0] == 'D' )
			{
				// _tsorter += '<tr>';
				_tr = _tsorter.insertRow(_tsorter.rows.length);
				if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
				else _tr.setAttribute("class", "stock_running_detail_white");

				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				_td.innerHTML = "<div align='center'>"+_subdata[1]+"</div>";
				// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><div align="center">'+_subdata[1]+'</div></td>';

				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				_td.innerHTML = _subdata[2];
				// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+_subdata[2]+'</td>';

				t=parseInt(_subdata[3]);
				_c=getColor(_prev,t);

				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				// _td.setAttribute('class',_c);
				_td.innerHTML = '<div class="'+_c+'">' + t.formatMoney(0) + '</div>';
				// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="'+_c+'">'+t.formatMoney(0)+'</td>';
				if (_prev<=t)
				{
					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					// _td.setAttribute('class',_c);
					_td.innerHTML = '<div class="'+_c+'">' + (t-_prev).formatMoney(0) + '</div>';
					// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="'+_c+'">'+(t-_prev).formatMoney(0)+'</td>';

					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					// _td.setAttribute('class',_c);
					_td.innerHTML = '<div class="'+_c+'">' + ((t-_prev)*100/_prev).formatMoney(2) + '</div>';
					// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="'+_c+'">'+((t-_prev)*100/_prev).formatMoney(2)+'</td>'
				}
				else
				{
					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					// _td.setAttribute('class',_c);
					_td.innerHTML = '<div class="'+_c+'">' + '-' + (_prev-t).formatMoney(0) + '</div>';
					// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="'+_c+'">-'+(_prev-t).formatMoney(0)+'</td>';

					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					// _td.setAttribute('class',_c);
					_td.innerHTML = '<div class="'+_c+'">' + '-' + ((_prev-t)*100/_prev).formatMoney(2) + '</div>';
					// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="'+_c+'">-'+((_prev-t)*100/_prev).formatMoney(2)+'</td>'
				}

				t=parseInt(_subdata[4]);
				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				_td.innerHTML = t.formatMoney(0);
				// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" >'+t.formatMoney(0)+'</td>';

				if (_subdata[5] == 'D')
				{
					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					// _td.setAttribute('class','col_green');
					_td.innerHTML = '<div class="col_green">' + _subdata[5] + '</div>';

					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					// _td.setAttribute('class','col_green');
					_td.innerHTML = '<div class="col_green">' + _subdata[6] + '</div>';
					// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="col_green">'+_subdata[5]+'</td><td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="col_green">'+_subdata[6]+'</td>';
				}
				else
				{
					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					// _td.setAttribute('class','col_red');
					_td.innerHTML = '<div class="col_red">' + _subdata[5] + '</div>';

					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					// _td.setAttribute('class','col_red');
					_td.innerHTML = '<div class="col_red">' + _subdata[6] + '</div>';
					// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="col_red">'+_subdata[5]+'</td><td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="col_red">'+_subdata[6]+'</td>';
				}

				if (_subdata[8] == 'D')
				{
					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					//_td.setAttribute('class','col_green');
					_td.innerHTML = '<div class="col_green">' + _subdata[7] + '</div>';

					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					//_td.setAttribute('class','col_green');
					_td.innerHTML = '<div class="col_green">' + _subdata[8] + '</div>';
					// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="col_green">'+_subdata[7]+'</td><td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="col_green">'+_subdata[8]+'</td>';
				}
				else
				{
					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					//_td.setAttribute('class','col_red');
					_td.innerHTML = '<div class="col_red">' + _subdata[7] + '</div>';

					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					//_td.setAttribute('class','col_red');
					_td.innerHTML = '<div class="col_red">' + _subdata[8] + '</div>';
					// _tsorter += '<td class="col_red">'+_subdata[7]+'</td><td class="col_red">'+_subdata[8]+'</td>';
				}

				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = '&nbsp;';
				// _tsorter += '<td>&nbsp;</td></tr>';
			}
			else if (_subdata[0] == 'T')
			{
				//if (i%2 == 0) _tradeBook += '<tr class="stock_running_detail_grey">';
				//else _tradeBook += '<tr class="stock_running_detail_white">';
				_tr = _tradeBook.insertRow(_tradeBook.rows.length);
				if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
				else _tr.setAttribute("class", "stock_running_detail_white");

				t=parseInt(_subdata[1]);
				_c=getColor(_prev,t);

				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				_td.setAttribute('class',_c);
				_td.innerHTML = t.formatMoney(0);
				//_tradeBook += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="'+_c+'">'+t.formatMoney(0)+'</td>';

				t=parseInt(_subdata[2]);
				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				_td.setAttribute('class','col_grey');
				_td.innerHTML = t.formatMoney(0);
				//_tradeBook += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="col_grey">'+t.formatMoney(0)+'</td>';

				t=parseInt(_subdata[3])/_LOT_SIZE;
				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				_td.setAttribute('class','col_grey');
				_td.innerHTML = t.formatMoney(0);
				// _tradeBook += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="col_grey">'+t.formatMoney(0)+'</td>';

				t=parseInt(_subdata[4])/1000000;
				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				_td.setAttribute('class','col_grey');
				_td.innerHTML = t.formatMoney(0);
				// _tradeBook += '<td class="col_grey">'+t.formatMoney(2)+'</td>';

				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = '&nbsp;';
				// _tradeBook += '<td></td></tr>';
			}
			else if (_subdata[0] == 'B')
			{
				//if (i%2==0) _brokerSummary += '<tr class="stock_running_detail_grey">';
				//else _brokerSummary += '<tr class="stock_running_detail_white">';
				_tr = _brokerSummary.insertRow(_brokerSummary.rows.length);
				if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
				else _tr.setAttribute("class", "stock_running_detail_white");

				t=parseInt(_subdata[2]);
				if (t<=0)
				{
					_td = _tr.insertCell(_tr.cells.length);
					_td.innerHTML = '&nbsp;';
					//_brokerSummary += '<td>&nbsp;</td>';
					_td = _tr.insertCell(_tr.cells.length);
					_td.innerHTML = '&nbsp;';
					//_brokerSummary += '<td>&nbsp;</td>';
				}
				else
				{
					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					_td.setAttribute('class','col_grey');
					_td.innerHTML = t.formatMoney(0);
					// _brokerSummary += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="col_grey">'+t.formatMoney(0)+'</td>';
					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('class','col_grey');
					_td.setAttribute('align','center');
					_td.innerHTML = _subdata[1];
					// _brokerSummary += '<td class="col_grey" align="center">'+_subdata[1]+'</td>';
				}

				t=parseInt(_subdata[4]);
				if (t<=0)
				{
					_td = _tr.insertCell(_tr.cells.length);
					_td.innerHTML = '&nbsp;';
					//_brokerSummary += '<td>&nbsp;</td>';
					_td = _tr.insertCell(_tr.cells.length);
					_td.innerHTML = '&nbsp;';
					//_brokerSummary += '<td>&nbsp;</td>';
				}
				else
				{
					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
					_td.setAttribute('class','col_grey');
					_td.setAttribute('align','center');
					_td.innerHTML = _subdata[3];
					// _brokerSummary += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;" class="col_grey" align="center">'+_subdata[3]+'</td>';
					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute('class','col_grey');
					_td.innerHTML = t.formatMoney(0);
					// _brokerSummary += '<td class="col_grey">'+t.formatMoney(0)+'</td>';
				}
				_td = _tr.insertCell(_tr.cells.length);
				//_td.innerHTML = '&nbsp;';
				//_brokerSummary += '<td></td></tr>';
			}

			//_tsorter += '</tr>';
		}
		catch(e){}
	}
	//document.getElementById('tsorter').innerHTML = _tsorter;
	//document.getElementById('_brokerSummary').innerHTML = _brokerSummary;
	//document.getElementById('_tradeBook').innerHTML = _tradeBook;

	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'View';
	_viewButton.disabled = false;
}
function clearTable()
{
	var tbody = document.getElementById("tsorter");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}

	var tbody = document.getElementById("_brokerSummary");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}

	var tbody = document.getElementById("_tradeBook");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
function makeRequest(url)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			// set type accordingly to anticipated content type
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	http_request.onreadystatechange = alertContents;
	http_request.open('GET', url, true);
	http_request.send(null);
}
function alertContents()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			receivedData(http_request.responseText);
		}
		else if ( http_request.status == 302 )
		{
		}
		else
		{
		}
	}
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};
function getColor(prev,last)
{
	if (prev>last) return 'col_red';
	else if (prev<last) return 'col_green';
	return 'col_yellow';
}
function loadTodayString()
{
	var d = new Date();
	var _todayString = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
	document.getElementById("popupDatepicker").value = _todayString;
}
