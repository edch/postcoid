function loadData()
{
	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'Load';
	_viewButton.disabled = true;
	var _stock = document.getElementById('_stockCode').value.toUpperCase();

	// var img = new Image();
	// img.src = '../servlets/CandlestickServlet?v=0&h=250&w=741&s=' + _stock;
	document.getElementById('_chartImage').src = '../servlets/CandlestickServlet?v=0&h=250&w=741&s=' + _stock;

	makeRequest('../servlets/SummaryServlet?_a=hp&_st=' + _stock);
}
function receivedData(result)
{
	var _data = result.split('|');
	var i;
	var _subdata = _data[0].split('#');
	document.getElementById('_stockName').innerHTML = _subdata[0];
	document.getElementById('_current').innerHTML = _subdata[1];

	var _subdata = _data[1].split('#');
	var _last = parseInt(_subdata[0]);
	var _prev = parseInt(_subdata[1]);
	var _c=getColor(_prev,_last);

	if (_prev<=_last)
	{
		document.getElementById('_last').innerHTML = '<span class="'+_c+'">'+_last.formatMoney(0)+'</span>';
		document.getElementById('_chg').innerHTML = '<span class="'+_c+'">'+(_last-_prev).formatMoney(0)+'</span>';
		document.getElementById('_persenchg').innerHTML = '<span class="'+_c+'">'+((_last-_prev)*100/_prev).formatMoney(2)+'</span>';
	}
	else
	{
		document.getElementById('_last').innerHTML = '<span class="col_red">'+_last.formatMoney(0)+'</span>';
		document.getElementById('_chg').innerHTML = '<span class="col_red">-'+(_prev-_last).formatMoney(0)+'</span>';
		document.getElementById('_persenchg').innerHTML = '<span class="col_red">-'+((_prev-_last)*100/_prev).formatMoney(2)+'</span>';
	}

	var t = parseInt(_subdata[2]);
	document.getElementById('_open').innerHTML = '<span class="'+getColor(_prev,t)+'">'+t.formatMoney(0)+'</span>';
	t = parseInt(_subdata[3]);
	document.getElementById('_high').innerHTML = '<span class="'+getColor(_prev,t)+'">'+t.formatMoney(0)+'</span>';
	t = parseInt(_subdata[4]);
	document.getElementById('_low').innerHTML = '<span class="'+getColor(_prev,t)+'">'+t.formatMoney(0)+'</span>';
	t = parseFloat(_subdata[5]);
	document.getElementById('_avg').innerHTML = '<span class="'+getColor(_prev,t)+'">'+t.formatMoney(2)+'</span>';
	t = parseInt(_subdata[6]);
	document.getElementById('_freq').innerHTML = '<span class="col_grey">'+t.formatMoney(0)+'</span>';

	var p=0,l=0,t=0;
	// var _tsorter = '';
	clearTable();
	var _tsorter = document.getElementById("tsorter");
	for(i=2;i<_data.length;++i)
	{
		try
		{
			if (_data[i].length <= 3) continue;
			_subdata = _data[i].split('#');
			//if (i%2==0) _tsorter += '<tr class="stock_running_detail_white">';
			//else _tsorter += '<tr class="stock_running_detail_grey">';
			_tr = _tsorter.insertRow(_tsorter.rows.length);
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			p = parseInt(_subdata[4]); //prev

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = "<div align='center'>"+_subdata[0]+"</div>";
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><div align="center">'+_subdata[0]+'</div></td>';

			t = parseInt(_subdata[1]); //open
			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = '<span class="'+getColor(p,t)+'">'+t.formatMoney(0)+'</span>';
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><span class="'+getColor(p,t)+'">'+t.formatMoney(0)+'</span></td>';

			t = parseInt(_subdata[2]); //high
			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = '<span class="'+getColor(p,t)+'">'+t.formatMoney(0)+'</span>';
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><span class="'+getColor(p,t)+'">'+t.formatMoney(0)+'</span></td>';

			t = parseInt(_subdata[3]); //low
			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = '<span class="'+getColor(p,t)+'">'+t.formatMoney(0)+'</span>';
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><span class="'+getColor(p,t)+'">'+t.formatMoney(0)+'</span></td>';

			l = parseInt(_subdata[5]); //last
			var _c = getColor(p,l);
			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = '<span class="'+_c+'">'+l.formatMoney(0)+'</span>';
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><span class="'+_c+'">'+l.formatMoney(0)+'</span></td>';
			if (l>=p)
			{
				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				_td.innerHTML = '<span class="'+_c+'">'+(l-p).formatMoney(0)+'</span>';
				// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><span class="'+_c+'">'+(l-p).formatMoney(0)+'</span></td>';//chg

				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				_td.innerHTML = '<span class="'+_c+'">'+((l-p)*100/p).formatMoney(2)+'</span>';
				// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><span class="'+_c+'">'+((l-p)*100/p).formatMoney(2)+'</span></td>';//chg%
			}
			else
			{
				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				_td.innerHTML = '<span class="'+_c+'">-'+(p-l).formatMoney(0)+'</span>';
				// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><span class="'+_c+'">-'+(p-l).formatMoney(0)+'</span></td>';//chg

				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
				_td.innerHTML = '<span class="'+_c+'">-'+((p-l)*100/p).formatMoney(2)+'</span>';
				// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><span class="'+_c+'">-'+((p-l)*100/p).formatMoney(2)+'</span></td>';//chg%
			}

			t = parseInt(_subdata[6]); //vol
			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = t.formatMoney(0);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+t.formatMoney(0)+'</td>';


			//t = parseInt(_subdata[7]); //val
			//_td = _tr.insertCell(_tr.cells.length);
			//_td.innerHTML = (t/1000000).formatMoney(2);
		}
		catch(e){}
	}
	// document.getElementById('tsorter').innerHTML = _tsorter;
	$("table").trigger("update");

	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'View';
	_viewButton.disabled = false;
}
function clearTable()
{
	var tbody = document.getElementById("tsorter");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
function makeRequest(url)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			// set type accordingly to anticipated content type
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	http_request.onreadystatechange = alertContents;
	http_request.open('GET', url, true);
	http_request.send(null);
}
function alertContents()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			receivedData(http_request.responseText);
		}
		else if ( http_request.status == 302 )
		{
		}
		else
		{
		}
	}
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};
function getColor(prev,last)
{
	if (prev>last) return 'col_red';
	else if (prev<last) return 'col_green';
	return 'col_yellow';
}
function loadTodayString()
{
	var d = new Date();
	var _todayString = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
	document.getElementById("popupDatepicker").value = _todayString;
}
