function loadData()
{
	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'Load';
	_viewButton.disabled = true;
	var _date = document.getElementById('popupDatepicker').value;
	makeRequest('../servlets/SummaryServlet?_a=bs&_d=' + _date);
}
function receivedData(result)
{
	var _data = result.split('|');
	var i;
	document.getElementById('_current').innerHTML = _data[0];
	var t=0;
	var _tFreq=0,_tVol=0,_tVal=0;
	var _bFreq=0,_bVol=0,_bVal=0;
	var _sFreq=0,_sVol=0,_sVal=0;

	var _board = document.getElementById("_board");
	var _stockType = document.getElementById("_stockType");
	var _tr,_td;
	clearTable();
	for(i=1;i<_data.length;++i)
	{
		try
		{
			if (_data[i].length <= 3) continue;
			var _subdata = _data[i].split('#');
			if (_subdata[0]=='A')
			{
				_tr = _board.insertRow(_board.rows.length);
				if (i%2 == 1) _tr.setAttribute("bgcolor", "#f5f5f5");
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = "<div align='center'>"+_subdata[1]+"</div>";
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = "<div align='left'>"+_subdata[2]+"</div>";

				t=parseInt(_subdata[3]);
				_tFreq+=t;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[4]);
				_tVol+=t;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[5])/1000000;
				_tVal+=t;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[6]);
				_bFreq+=t;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[7]);
				_bVol+=t;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[8])/1000000;
				_bVal+=t;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[9]);
				_sFreq+=t;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[10]);
				_sVol+=t;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[11])/1000000;
				_sVal+=t;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);
			}
			else if (_subdata[0]=='B')
			{
				_tr = _stockType.insertRow(_stockType.rows.length);
				if (i%2 == 1) _tr.setAttribute("bgcolor", "#f5f5f5");
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = "<div align='center'>"+_subdata[1]+"</div>";

				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = "<div align='left'>"+_subdata[2]+"</div>";

				t=parseInt(_subdata[3]);
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[4]);
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[5])/1000000;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[6]);
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[7]);
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[8])/1000000;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[9]);
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[10]);
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);

				t=parseInt(_subdata[11])/1000000;
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = t.formatMoney(0);
			}
		}
		catch(e){}
	}

	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'View';
	_viewButton.disabled = false;
}
function clearTable()
{
	var tbody = document.getElementById("_board");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}

	var tbody = document.getElementById("_stockType");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}

function makeRequest(url)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			// set type accordingly to anticipated content type
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	http_request.onreadystatechange = alertContents;
	http_request.open('GET', url, true);
	http_request.send(null);
}
function alertContents()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			receivedData(http_request.responseText);
		}
		else if ( http_request.status == 302 )
		{
		}
		else
		{
		}
	}
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};
function getColor(prev,last)
{
	if (prev>last) return 'col_red';
	else if (prev<last) return 'col_green';
	return 'col_yellow';
}
function loadTodayString()
{
	var d = new Date();
	var _todayString = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
	document.getElementById("popupDatepicker").value = _todayString;
}
