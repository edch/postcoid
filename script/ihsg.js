function portfolioPoll(first)
{
	if (first)
	{
		try
		{
			amq.addListener('index','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
			amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=COMPOSITE>' );
		}
		catch (err){}
	}
}
var ihsgHandler = 
{
	_ihsg: function(message)
	{
		if (message != null) {
			try
			{
				var _composite = parseFloat ( message.getAttribute ( '_c' ) )
				var _prev = parseFloat ( message.getAttribute ( '_p' ) )
				var _freq = parseInt ( message.getAttribute ( '_f' ) )
				var _volume = parseFloat ( message.getAttribute ( '_vo' ) )
				var _value = parseFloat ( message.getAttribute ( '_va' ) ) / 1000000
				var _chg = Math.abs ( _composite - _prev );
				var _persenchg = (_chg / _prev) * 100;
			
				var _data = document.getElementById('_ihsg_ihsg')
				_data.className = "col_black";
				// _data.innerHTML = _composite.formatMoney (3);
				_data.innerHTML = _composite.formatMoney (2);

				var _ihsg_title = document.getElementById('_ihsg_title')
				var _data_chg = document.getElementById('_chg_ihsg')
				var _data_persen_chg = document.getElementById('_persen_ihsg')
				//_data_chg.innerHTML = _chg.toFixed(3);
				//_data_persen_chg.innerHTML = _persenchg.toFixed(3) + " %";
				_data_chg.innerHTML = _chg.toFixed(2);
				_data_persen_chg.innerHTML = _persenchg.toFixed(2) + " %";
				if ( _composite < _prev )
				{
					_data_chg.className = "col_red";
					_data_persen_chg.className = "col_red";
					_ihsg_title.className = "header_IHSG_Title_red";
				}
				else if (_composite > _prev  )
				{
					_data_chg.className = "col_green";
					_data_persen_chg.className = "col_green";
					_ihsg_title.className = "header_IHSG_Title_green";
				}
				else if ( _composite == _prev  )
				{
					_data_chg.className = "col_yellow";
					_data_persen_chg.className = "col_yellow";
					_ihsg_title.className = "header_IHSG_Title_green";
				}

				_data = document.getElementById('_freq_ihsg')
				_data.innerHTML = '<font color="#1055cf">Freq</font> ' + _freq.formatMoney (0);
				_data = document.getElementById('_volume_ihsg')
				_data.innerHTML = '<font color="#1055cf">Vol</font> ' + _volume.formatMoney (0);
				_data = document.getElementById('_value_ihsg')
			
				if ( _value < 100000 )
				{
					_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " M";
				}
				else
				{
					_value = _value / 1000;
					_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " B";
				}
			}
			catch (err) {}
		}
	}
};
amq.uri='../amq';
amq.addPollHandler(portfolioPoll);
function unloadPage ()
{
	try
	{
		amq.removeListener ( 'index', 'topic://QUOTES.COMPOSITE.INDICES');
	}
	catch (err){}
}
