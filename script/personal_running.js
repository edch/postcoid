var currentStockCode='';
var priceHandler = 
{
	_price: function(message) 
	{
		if (message != null)
		{
			processQuotes ( message );
		}
	}
};
var ihsgHandler = 
{
	_ihsg: function(message)
	{
		if (message != null) {
			try
			{
				var _composite = parseFloat ( message.getAttribute ( '_c' ) )
				var _prev = parseFloat ( message.getAttribute ( '_p' ) )
				var _freq = parseInt ( message.getAttribute ( '_f' ) )
				var _volume = parseFloat ( message.getAttribute ( '_vo' ) )
				var _value = parseFloat ( message.getAttribute ( '_va' ) ) / 1000000
				var _chg = Math.abs ( _composite - _prev );
				var _persenchg = (_chg / _prev) * 100;
			
				var _ihsg_title = document.getElementById('header_IHSG_Title');
				var _data = document.getElementById('_ihsg_ihsg')
				_data.className = "col_black";
				_data.innerHTML = _composite.formatMoney (2);

				var _data_chg = document.getElementById('_chg_ihsg')
				var _data_persen_chg = document.getElementById('_persen_ihsg')
				_data_chg.innerHTML = _chg.toFixed(2);
				_data_persen_chg.innerHTML = _persenchg.toFixed(2) + " %";
				if ( _composite < _prev )
				{
					_data_chg.className = "col_red";
					_data_persen_chg.className = "col_red";
					_ihsg_title.style.backgroundColor = '#e54044';
				}
				else if (_composite > _prev  )
				{
					_data_chg.className = "col_green";
					_data_persen_chg.className = "col_green";
					_ihsg_title.style.backgroundColor = '#01ba3a';
				}
				else if ( _composite == _prev  )
				{
					_data_chg.className = "col_yellow";
					_data_persen_chg.className = "col_yellow";
					_ihsg_title.style.backgroundColor = '#01ba3a';
				}

				_data = document.getElementById('_freq_ihsg')
				_data.innerHTML = '<font color="#1055cf">Freq</font> ' + _freq.formatMoney (0);
				_data = document.getElementById('_volume_ihsg')
				_data.innerHTML = '<font color="#1055cf">Vol</font> ' + _volume.formatMoney (0);
				_data = document.getElementById('_value_ihsg')
			
				if ( _value < 100000 )
				{
					_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " M";
				}
				else
				{
					_value = _value / 1000;
					_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " B";
				}
			}
			catch (err) {}
		}
	}
};
var _maxRow = 20;
function processQuotes ( message )
{
	if ( message != null )
	{
		try
		{
			var _row = 0;
			var _stock = message.getAttribute ( '_s' ).toUpperCase();
			var _last = parseInt ( message.getAttribute ( '_l' ) )
			var _prev = parseInt ( message.getAttribute ( '_p' ) )
			var _open = parseInt ( message.getAttribute ( '_o' ) )
			var _high = parseInt ( message.getAttribute ( '_hi' ) )
			var _low = parseInt ( message.getAttribute ( '_lo' ) )
			var _freq = parseInt ( message.getAttribute ( '_f' ) )
			var _vol = parseInt ( message.getAttribute ( '_vo' ) ) / _LOT_SIZE;
			var _val = parseInt ( message.getAttribute ( '_va' ) ) / 1000000

			var _bid = parseInt ( message.getAttribute ( '_ba' ) )
			var _bidVol = parseInt ( message.getAttribute ( '_bo' ) ) / _LOT_SIZE;
			var _offer = parseInt ( message.getAttribute ( '_oa' ) )
			var _offerVol = parseInt ( message.getAttribute ( '_oo' ) ) / _LOT_SIZE;

			var _chg = Math.abs ( _last - _prev )
			var _persenchg = (_chg / _prev) * 100;

			var _eStyle = "col_green";
			if ( _last < _prev )
			{
				_eStyle = "col_red";
			}
			else if ( _last > _prev )
			{
				_eStyle = "col_green";
			}
			else if ( _last == _prev )
			{
				_eStyle = "col_yellow";
			}
	
			for ( _row = 0; _row < _maxRow; ++_row )
			{
				var _codeRow = document.getElementById ("_code_row" + _row ).innerHTML;
				if ( _codeRow == _stock )
				{
					document.getElementById ("_last_qrow" + _row).innerHTML = _last.formatMoney(0);
					document.getElementById ("_last_qrow" + _row).className = _eStyle;
	
					document.getElementById ("_chg_qrow" + _row).innerHTML = _chg.formatMoney(0);
					document.getElementById ("_chg_qrow" + _row).className = _eStyle;
	
					document.getElementById ("_persenchg_qrow" + _row).innerHTML = _persenchg.formatMoney(2);
					document.getElementById ("_persenchg_qrow" + _row).className = _eStyle;
	
					// added
					if ( _prev < _bid )
					{
						document.getElementById ("_bidvol_qrow" + _row).className = "col_green";
						document.getElementById ("_bid_qrow" + _row).className = "col_green";
					}
					else if ( _prev > _bid )
					{
						document.getElementById ("_bidvol_qrow" + _row).className = "col_red";
						document.getElementById ("_bid_qrow" + _row).className = "col_red";
					}
					else if ( _prev == _bid )
					{
						document.getElementById ("_bidvol_qrow" + _row).className = "col_yellow";
						document.getElementById ("_bid_qrow" + _row).className = "col_yellow";
					}
	
					document.getElementById ("_bidvol_qrow" + _row).innerHTML = _bidVol.formatMoney(0);
					document.getElementById ("_bid_qrow" + _row).innerHTML = _bid.formatMoney(0);

					if ( _prev < _offer )
					{
						document.getElementById ("_offervol_qrow" + _row).className = "col_green";
						document.getElementById ("_offer_qrow" + _row).className = "col_green";
					}
					else if ( _prev > _offer )
					{
						document.getElementById ("_offervol_qrow" + _row).className = "col_red";
						document.getElementById ("_offer_qrow" + _row).className = "col_red";
					}
					else if ( _prev == _offer )
					{
						document.getElementById ("_offervol_qrow" + _row).className = "col_yellow";
						document.getElementById ("_offer_qrow" + _row).className = "col_yellow";
					}

					document.getElementById ("_offer_qrow" + _row).innerHTML = _offer.formatMoney(0);
					document.getElementById ("_offervol_qrow" + _row).innerHTML = _offerVol.formatMoney(0);

					break;
				}
			}
		}
		catch (err) {}
	}
}
var _row = 0;
var _rowColor = "col_black";
var runningTradeHandler = 
{
	_runningTrade: function(message) 
	{
		if (message != null) {
			//try
			{
				var _time = message.getAttribute('_t')
				var _stock = message.getAttribute('_s')
				var _price = parseInt ( message.getAttribute('_p') )
				var _qty = parseInt ( message.getAttribute('_q') )
				var _last = parseInt ( message.getAttribute('_l') )
				var _chg = Math.abs ( _price - _last )
				var _persenchg = (_chg / _price) * 100;

				previousRow = (_row - 1 + _maxRow+1) % (_maxRow+1)
				var prow = document.getElementById('row' + previousRow)
			
				if ( previousRow % 2 == 0 )
				{
					prow.className = 'stock_running_detail_grey';
				}
				else
				{
					prow.className = 'stock_running_detail_white';
				}

				var row = document.getElementById ('row' + _row);
				row.className = 'stock_running_detail_active';
			
				row = document.getElementById('_time_row' + _row)
				row.innerHTML = "<span class='" + _rowColor + "'>" + _time + "</span>";

				row = document.getElementById('_stock_row' + _row)
				row.innerHTML = "<span class='" + _rowColor + "'>" + _stock + "</span>";
				row.style.textAlign = 'left';

				var priceRow = document.getElementById('_price_row' + _row)
				var chgRow = document.getElementById('_chg_row' + _row)

				if ( _price < _last )
				{
					priceRow.innerHTML = "<span class='col_red'>" + _price.formatMoney (0) + "</span>";
					chgRow.innerHTML = "<span class='col_red'>" + _chg.formatMoney (0) + "</span>";
				}
				else if ( _price > _last )
				{
					priceRow.innerHTML = "<span class='col_green'>" + _price.formatMoney (0) + "</span>";
					chgRow.innerHTML = "<span class='col_green'>" + _chg.formatMoney (0) + "</span>";
				}
				else if ( _price == _last )
				{
					priceRow.innerHTML = "<span class='col_yellow'>" + _price.formatMoney (0) + "</span>";
					chgRow.innerHTML = "<span class='col_yellow'>" + _chg.formatMoney (0) + "</span>";
				}

				row = document.getElementById('_qty_row' + _row)
				row.innerHTML = "<span class='col_black'>" + _qty.formatMoney(0) + "</span>";

				++_row;
				if ( _row >= _maxRow+1 )
				{
					_row = 0;

					if ( _rowColor == 'col_black' ) _rowColor = 'col_blue';
					else _rowColor = 'col_black';
				}
			}
			// catch (err) {}
		}
	}
};
function portfolioPoll(first)
{
	if (first)
	{
		amq.addListener('index','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
		amq.addListener('trade','topic://RUNNING.TRADE',runningTradeHandler._runningTrade);
		amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=COMPOSITE>' );
	}
}

//amq.uri='../amq';
//amq.addPollHandler(portfolioPoll);
function unloadPage (unloadIndex)
{
	var i = 0;
	try
	{

		for (i = 0; i < _rowQuotes; ++i)
		{
			var _stockCode = document.getElementById("_code_row" + i).innerHTML;
			if ( _stockCode != '')
			{
				amq.removeListener ('qt', 'topic://QUOTES.' + _stockCode + '.RG');
			}
		}

		if ( unloadIndex == 'yes' )
		{
			amq.removeAllListener();
			amq.removeListener ( 'ihsg', 'topic://QUOTES.COMPOSITE.INDICES');
			amq.removeListener ( 'rt', 'topic://RUNNING.TRADE');
		}

		if ( currentStockCode.indexOf('.') == -1 )
		{
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode + '.RG' );
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode + '.RG' );
		}
		else
		{
			amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode);
			amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode);
		}
	}
	catch (err) {}
}
function processStockList(stockList)
{
	unloadPage('no');
	var stocks = stockList.split(":");
	_rowQuotes = 0;
	for (i = 0; i < stocks.length-1; ++i)
	{
		document.getElementById("_code_row" + _rowQuotes).innerHTML = stocks[i];
		document.getElementById("_last_qrow" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_chg_qrow" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_persenchg_qrow" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_bidvol_qrow" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_bid_qrow" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_offer_qrow" + _rowQuotes).innerHTML = '&nbsp;';
		document.getElementById("_offervol_qrow" + _rowQuotes).innerHTML = '&nbsp;';

		// amq.addListener('quotes' + stocks[i] + '.RG','topic://QUOTES.' + stocks[i] + '.RG', priceHandler._price);
		amq.addListener('qt','topic://QUOTES.' + stocks[i] + '.RG', priceHandler._price);
		amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + stocks[i] + '>' );
		++_rowQuotes;
	}
	for (; i < _maxRow; ++i)
	{
		document.getElementById("_code_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_last_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_chg_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_persenchg_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_bidvol_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_bid_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_offer_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_offervol_qrow" + i).innerHTML = '&nbsp;';
	}
}
var _rowQuotes = 0;
function handleQuoteKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		obj.value = obj.value.toUpperCase();
		if ( _allStocksCode.indexOf ( ':' + obj.value + ':' ) < 0 )
		{
			alert ( 'Invalid Stock Code.' );
			obj.value = '';
			obj.focus();
			return false;
		}

		var i = 0;
		for (i = 0; i < _maxRow; ++i)
		{
			if ( document.getElementById("_code_row" + i).innerHTML == obj.value )
			{
				obj.value = '';
				obj.focus();
				return false;
			}
		}

		document.getElementById ("_code_row" + _rowQuotes ).innerHTML = obj.value;
		// amq.addListener('quotes' + obj.value + '.RG','topic://QUOTES.' + obj.value + '.RG', priceHandler._price);
		amq.addListener('qt','topic://QUOTES.' + obj.value + '.RG', priceHandler._price);
		amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + obj.value + '>' );

		++_rowQuotes;

		obj.value = '';
		obj.focus();
		return false;
	}
}
function deleteQuote(num)
{
	var _stockCode = document.getElementById ("_code_row" + num).innerHTML;
	if ( _stockCode == '' )
	{
		return false;
	}

	try
	{
		// amq.removeListener ('quotes' + _stockCode + '.RG', 'topic://QUOTES.' + _stockCode + '.RG');
		amq.removeListener ('qt', 'topic://QUOTES.' + _stockCode + '.RG');

		var i = 0;
		var j = 0;
		_rowQuotes--;
		for ( i = num; i < _rowQuotes; ++i )
		{
			j = i+1;
			document.getElementById("_code_row" + i).innerHTML = document.getElementById("_code_row" + j).innerHTML;
			document.getElementById("_last_qrow" + i).innerHTML = document.getElementById("_last_qrow" + j).innerHTML;
			document.getElementById("_chg_qrow" + i).innerHTML = document.getElementById("_chg_qrow" + j).innerHTML;
			document.getElementById("_persenchg_qrow" + i).innerHTML = document.getElementById("_persenchg_qrow" + j).innerHTML;
			document.getElementById("_bidvol_qrow" + i).innerHTML = document.getElementById("_bidvol_qrow" + j).innerHTML;
			document.getElementById("_bid_qrow" + i).innerHTML = document.getElementById("_bid_qrow" + j).innerHTML;
			document.getElementById("_offer_qrow" + i).innerHTML = document.getElementById("_offer_qrow" + j).innerHTML;
			document.getElementById("_offervol_qrow" + i).innerHTML = document.getElementById("_offervol_qrow" + j).innerHTML;
		}

		document.getElementById("_code_row" + i).innerHTML = '&nbsp;';
		document.getElementById("_last_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_chg_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_persenchg_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_bidvol_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_bid_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_offer_qrow" + i).innerHTML = '&nbsp;';
		document.getElementById("_offervol_qrow" + i).innerHTML = '&nbsp;';
	}
	catch (Err) {}

	return false;
}

Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};
function handleKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		obj.value = obj.value.toUpperCase();
		if ( obj.value.indexOf('.') == -1 )
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value + ':' ) >= 0 )
			{
				changeStock ( obj );
			}
			else
			{
				alertInvalid(obj);
			}
		}
		else
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value.substring ( 0, obj.value.indexOf('.') )  + ':' ) >= 0 )
			{
				var _board = obj.value.substring (obj.value.indexOf('.'));
				if ( _board == '.TN' || _board == '.NG' )
				{
					changeStock ( obj );
				}
				else
				{
					alertInvalid(obj);
				}
			}
			else
			{
				alertInvalid(obj);
			}
		}

		obj.select();
		obj.focus();
		return false;
	}
}
function alertInvalid(obj)
{
	document.getElementById('_stockName').innerHTML = 'Invalid Stock Code';
}
function changeStock (obj)
{
	try
	{
		if ( obj.value == currentStockCode )
		{
			obj.value = '';
			return;
		}

		if ( currentStockCode.indexOf('.') == -1 )
		{
			amq.removeListener ( 'ob' , 'topic://ORDERBOOK.' + currentStockCode + '.RG' );
			amq.removeListener ( 'qt' , 'topic://QUOTES.' + currentStockCode + '.RG' );
		}
		else
		{
			amq.removeListener ( 'ob' , 'topic://ORDERBOOK.' + currentStockCode);
			amq.removeListener ( 'qt' , 'topic://QUOTES.' + currentStockCode);
		}

		currentStockCode = obj.value.toUpperCase();
		if ( currentStockCode.indexOf('.') == -1 )
		{
			amq.addListener('ob' ,'topic://ORDERBOOK.' + currentStockCode + '.RG', orderBookHandler._orderBookReceived);
			amq.addListener('qt' ,'topic://QUOTES.' + currentStockCode + '.RG', priceBookHandler._price);
		}
		else
		{
			amq.addListener('ob' ,'topic://ORDERBOOK.' + currentStockCode, orderBookHandler._orderBookReceived);
			amq.addListener('qt' ,'topic://QUOTES.' + currentStockCode, priceBookHandler._price);
		}

		amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + currentStockCode + '>' );
		clearOrderBook ();
	}
	catch (err) {}
}
function clearOrderBook()
{
	var i = 0;
	for (i = 0; i < 5; ++i)
	{
		document.getElementById ("_bidVol" + i).innerHTML = "&nbsp;";
		document.getElementById ("_bidVal" + i).innerHTML = "&nbsp;";
		document.getElementById ("_offerVol" + i).innerHTML = "&nbsp;";
		document.getElementById ("_offerVal" + i).innerHTML = "&nbsp;";
	}
}

var _prev = 0
var priceBookHandler = 
{
	_price: function(message) 
	{
		if (message != null)
		{
			try
			{
				// quotes
				var _stock = message.getAttribute ( '_s' ).toUpperCase();
				if ( _stock != currentStockCode )
				{
					return;
				}

				var _last = parseInt ( message.getAttribute ( '_l' ) )
				_prev = parseInt ( message.getAttribute ( '_p' ) )
				var _open = parseInt ( message.getAttribute ( '_o' ) )
				var _high = parseInt ( message.getAttribute ( '_hi' ) )
				var _low = parseInt ( message.getAttribute ( '_lo' ) )
				var _freq = parseInt ( message.getAttribute ( '_f' ) )
				var _vol = parseInt ( message.getAttribute ( '_vo' ) ) / _LOT_SIZE;
				var _val = parseInt ( message.getAttribute ( '_va' ) ) / 1000000
				var _chg = Math.abs ( _last - _prev )
				if ( _last == 0 ) _chg = 0;
				var _persenchg = (_chg / _prev) * 100;
				var _stockName = message.getAttribute ( '_n' )
				if ( _stockName ) document.getElementById ("_stockName").innerHTML = _stockName;

				var _eStyle = "col_yellow";
				var _eChg = "";
				if ( _last < _prev )
				{
					_eStyle = "col_red";
					_eChg = "&nbsp; ";
				}
				else if ( _last > _prev )
				{
					_eStyle = "col_green";
					_eChg = "&nbsp; ";
				}
				else if ( _last == _prev )
				{
					_eStyle = "col_yellow";
					_eChg = "&nbsp;";
				}

				var _element = document.getElementById ("_last");
				_element.innerHTML = _last.formatMoney (0);
				_element.className = _eStyle;
			
				_element = document.getElementById ("_chg");
				_element.innerHTML = _eChg + " " + _chg.formatMoney (0);
				_element.className = _eStyle;
			
				_element = document.getElementById ("_prev");
				_element.innerHTML = _prev.formatMoney (0);
				_element.className = "col_yellow";
			
				_element = document.getElementById ("_persenchg");
				_element.innerHTML = _persenchg.formatMoney (2);
				_element.className = _eStyle;
			
				_element = document.getElementById ("_open");
				_element.innerHTML = _open.formatMoney (0);			
				if ( _open < _prev ) _element.className = "col_red";
				else if ( _open > _prev ) _element.className = "col_green";
				else if ( _open == _prev ) _element.className = "col_yellow";
			
				_element = document.getElementById ("_high");
				_element.innerHTML = _high.formatMoney (0);			
				if ( _high < _prev ) _element.className = "col_red";
				else if ( _high > _prev ) _element.className = "col_green";
				else if ( _high == _prev ) _element.className = "col_yellow";

				_element = document.getElementById ("_low");
				_element.innerHTML = _low.formatMoney (0);			
				if ( _low < _prev ) _element.className = "col_red";
				else if ( _low > _prev ) _element.className = "col_green";
				else if ( _low == _prev ) _element.className = "col_yellow";

				_element = document.getElementById ("_freq");
				_element.innerHTML = _freq.formatMoney (0);

				_element = document.getElementById ("_vol");
				_element.innerHTML = _vol.formatMoney (0);

				_element = document.getElementById ("_val");
				if ( _val < 100000 )
				{
					_element.innerHTML = _val.formatMoney(2) + " M";
				}
				else			
				{
					_val /= 1000;
					_element.innerHTML = _val.formatMoney(2) + " B";
				}
				// _element.className = "black_color";
			}
			catch (err) {}
		}
	}
};
var orderBookHandler = 
{
	_orderBookReceived: function(message)
	{
		if (message != null)
		{
			try
			{
				var _stock = message.getAttribute ( '_s' ).toUpperCase();
				var _prev = parseInt ( message.getAttribute ( '_p' ) )
				if (_stock != currentStockCode)
				{
					// removeStockFromListener ( _stock );
					return false;
				}

				// order book
				var i = 0
				var _eStyle = "col_yellow";
				for (i = 0; i < 5; ++i)
				{			
					var _bidValMsg = parseInt ( message.getAttribute ( '_ba' + i ) )
					var _bidVolMsg = parseInt ( message.getAttribute ( '_bo' + i ) )

					var _bidVol = document.getElementById ("_bidVol" + i);
					var _bidVal = document.getElementById ("_bidVal" + i);

					if ( _bidVolMsg != null && _bidValMsg != null )
					{
						if ( _bidValMsg < _prev ) _eStyle = "col_red";
						else if ( _bidValMsg > _prev ) _eStyle = "col_green";
						else if ( _bidValMsg == _prev ) _eStyle = "col_yellow";

						_bidVol.className = _eStyle;
						_bidVal.className = _eStyle;
						_bidVol.innerHTML = _bidVolMsg.formatMoney (0);
						_bidVal.innerHTML = _bidValMsg.formatMoney (0);
					}

					var _offerValMsg = parseInt ( message.getAttribute ( '_oa' + i ) )
					var _offerVolMsg = parseInt ( message.getAttribute ( '_oo' + i ) )

					var _offerVol = document.getElementById ("_offerVol" + i);
					var _offerVal = document.getElementById ("_offerVal" + i);

					if ( _offerVolMsg != null && _offerValMsg != null )
					{
						if ( _offerValMsg < _prev ) _eStyle = "col_red";
						else if ( _offerValMsg > _prev ) _eStyle = "col_green";
						else if ( _offerValMsg == _prev ) _eStyle = "col_yellow";

						_offerVol.className = _eStyle;
						_offerVal.className = _eStyle;
						_offerVol.innerHTML = _offerVolMsg.formatMoney (0);
						_offerVal.innerHTML = _offerValMsg.formatMoney (0);
					}
				}
			}
			catch (err) {}
		}
	}
};

function startScreen()
{
			amq.addListener('ihsg','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
			amq.addListener('rt','topic://RUNNING.TRADE',runningTradeHandler._runningTrade);
			amq.sendMessage('topic://REQUEST.FIRST', '<c s=COMPOSITE>');
}
function stopScreen()
{
	try
	{
		amq.removeAllListener();
		// ihsgHandler.stopIhsg();
	}
	catch (err) {}
}
