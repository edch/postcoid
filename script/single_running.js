var currentStockCode = ''
var _prev = 0
var priceHandler = 
{
	_price: function(message) 
	{
		if (message != null)
		{
			// quotes
			var _stock = message.getAttribute ( '_s' ).toUpperCase();			
			if ( _stock == currentStockCode )
			{
				var _last = parseInt ( message.getAttribute ( '_l' ) )
				_prev = parseInt ( message.getAttribute ( '_p' ) )
				var _open = parseInt ( message.getAttribute ( '_o' ) )
				var _high = parseInt ( message.getAttribute ( '_hi' ) )
				var _low = parseInt ( message.getAttribute ( '_lo' ) )
				var _freq = parseInt ( message.getAttribute ( '_f' ) )
				var _vol = parseInt ( message.getAttribute ( '_vo' ) )
				var _val = parseInt ( message.getAttribute ( '_va' ) )
				var _chg = Math.abs ( _last - _prev )
				var _persenchg = (_chg / _prev) * 100;
				var _stockName = message.getAttribute ( '_n' )
				_val /= 1000000;
				_vol /= _LOT_SIZE;

				var _stockCode = document.getElementById ("_stockCode").value.toUpperCase();
				if ( _stockName != null )
				{
					document.getElementById ("_stockName").innerHTML = _stockName;
				}

				var _eStyle = "col_yellow";
				if ( _last < _prev )
				{
					_eStyle = "col_red";
				}
				else if ( _last > _prev )
				{
					_eStyle = "col_green";
				}
				else if ( _last == _prev )
				{
					_eStyle = "col_yellow";
				}

				var _element = document.getElementById ("_last");
				_element.innerHTML = _last.formatMoney (0);
				_element.className = _eStyle;

				_element = document.getElementById ("_chg");
				_element.innerHTML = _chg.formatMoney (0);
				_element.className = _eStyle;

				_element = document.getElementById ("_prev");
				_element.innerHTML = _prev.formatMoney (0);
				_element.className = "col_yellow";

				_element = document.getElementById ("_persenchg");
				_element.innerHTML = _persenchg.formatMoney (2);
				_element.className = _eStyle;

				_element = document.getElementById ("_open");
				_element.innerHTML = _open.formatMoney (0);			
				if ( _open < _prev ) _element.className = "col_red";
				else if ( _open > _prev ) _element.className = "col_green";
				else if ( _open == _prev ) _element.className = "col_yellow";

				_element = document.getElementById ("_high");
				_element.innerHTML = _high.formatMoney (0);			
				if ( _high < _prev ) _element.className = "col_red";
				else if ( _high > _prev ) _element.className = "col_green";
				else if ( _high == _prev ) _element.className = "col_yellow";

				_element = document.getElementById ("_low");
				_element.innerHTML = _low.formatMoney (0);			
				if ( _low < _prev ) _element.className = "col_red";
				else if ( _low > _prev ) _element.className = "col_green";
				else if ( _low == _prev ) _element.className = "col_yellow";

				_element = document.getElementById ("_freq");
				_element.innerHTML = _freq.formatMoney (0);

				_element = document.getElementById ("_vol");
				_element.innerHTML = _vol.formatMoney (0);

				_element = document.getElementById ("_val");
				if ( _val < 100000 )
				{
					_element.innerHTML = _val.formatMoney (2) + " M";
				}
				else
				{
					_val = _val / 1000;
					_element.innerHTML = _val.formatMoney (2) + " B";
				}
			}
			else
			{
				removeStockFromListener ( _stock );
			}
		}
	}
};

var orderBookHandler = 
{
	_orderBookReceived: function(message)
	{
		if (message != null)
		{
			var _stock = message.getAttribute ( '_s' ).toUpperCase();
			if ( _stock == currentStockCode )
			{
				var _prev = parseInt ( message.getAttribute ( '_p' ) )
				var _stockCode = document.getElementById ("_stockCode").value.toUpperCase();

				// order book
				var i = 0
				var _eStyle = "col_yellow";
				for (i = 0; i < 5; ++i)
				{			
					var _bidValMsg = parseInt ( message.getAttribute ( '_ba' + i ) )
					var _bidVolMsg = parseInt ( message.getAttribute ( '_bo' + i ) )

					var _bidVol = document.getElementById ("_bidVol" + i);
					var _bidVal = document.getElementById ("_bidVal" + i);
					if ( _bidVolMsg != null && _bidValMsg != null )
					{
						if ( _bidValMsg < _prev ) _eStyle = "col_red";
						else if ( _bidValMsg > _prev ) _eStyle = "col_green";
						else if ( _bidValMsg == _prev ) _eStyle = "col_yellow";

						_bidVol.className = _eStyle;
						_bidVal.className = _eStyle;
						_bidVol.innerHTML = _bidVolMsg.formatMoney (0);
						_bidVal.innerHTML = _bidValMsg.formatMoney (0);
					}

					var _offerValMsg = parseInt ( message.getAttribute ( '_oa' + i ) )
					var _offerVolMsg = parseInt ( message.getAttribute ( '_oo' + i ) )

					var _offerVol = document.getElementById ("_offerVol" + i);
					var _offerVal = document.getElementById ("_offerVal" + i);
					if ( _offerVolMsg != null && _offerValMsg != null )
					{
						if ( _offerValMsg < _prev ) _eStyle = "col_red";
						else if ( _offerValMsg > _prev ) _eStyle = "col_green";
						else if ( _offerValMsg == _prev ) _eStyle = "col_yellow";

						_offerVol.className = _eStyle;
						_offerVal.className = _eStyle;
						_offerVol.innerHTML = _offerVolMsg.formatMoney (0);
						_offerVal.innerHTML = _offerValMsg.formatMoney (0);
					}
				}
			}
			else
			{
				removeStockFromListener ( _stock );
			}
		}
	}
};

var tradeBookHandler =
{
	_tradeBookReceived: function(message)
	{
		if (message != null)
		{
			var _stock = message.getAttribute ( '_s' ).toUpperCase();
			if ( _stock != currentStockCode )
			{
				return;
			}

			var _prev = parseInt ( message.getAttribute ( '_p' ) )
			var i = 0;
			var innerCode = '<table class="stock_detail_line2" cellpadding="2" cellspacing="0" width="248" border="0">';
			innerCode += '<tr><td width="55"></td><td width="40"></td><td width="54"></td><td width="68"></td><td width="1"></td></tr>';
			for (i = 0;; ++i)
			{
				try
				{
					var _price = message.getAttribute ( '_p' + i );
					if ( _price == null )
					{
						break;
					}

					_price = parseInt ( _price );
					var eStyle = 'col_yellow';
					if ( _price > _prev )
					{
						eStyle = 'col_green';
					}
					else if ( _price < _prev )
					{
						eStyle = 'col_red';
					}

					var _volume = parseInt ( message.getAttribute ( '_v' + i ) ) / _LOT_SIZE;
					var _freq = parseInt ( message.getAttribute ( '_f' + i ) );
				}
				catch ( err )
				{
					break;
				}

				//if (i%2 == 0) innerCode += '<tr class="stock_running_detail_grey">';
				//else innerCode += '<tr class="stock_running_detail_white">';
				if (i%2 == 0) innerCode += '<tr style="background-color:#e5e5e5;">';
				else innerCode += '<tr>';
				innerCode += '<td class="' + eStyle + '">' + _price.formatMoney(0) + '</td>';
				innerCode += '<td class="col_black">' + _freq + '</td>';
				innerCode += '<td class="col_black">' + _volume.formatMoney(0) + '</td>';
				var _value = _price * _volume;
				if ( _value < 100000 )
				{
					_value /= 1000;
					innerCode += '<td class="col_black">' + _value.formatMoney (2) + ' K' + '</td>';
				}
				else
				{
					_value = _value / 1000000;
					innerCode += '<td class="col_black">' + _value.formatMoney (2) + ' M' + '</td>';
				}

				innerCode += '<td></td></tr>';
			}
			innerCode += '</table>';
			document.getElementById('_tradeBook').innerHTML = innerCode;
		}
	}
};

var ihsgHandler = 
{
	_ihsg: function(message)
	{
		if (message != null) {
			console.log ( message );


			var _composite = parseFloat ( message.getAttribute ( '_c' ) )
			var _prev = parseFloat ( message.getAttribute ( '_p' ) )
			var _freq = parseInt ( message.getAttribute ( '_f' ) )
			var _volume = parseFloat ( message.getAttribute ( '_vo' ) )
			var _value = parseFloat ( message.getAttribute ( '_va' ) ) / 1000000
			var _chg = Math.abs ( _composite - _prev );
			var _persenchg = (_chg / _prev) * 100;

			var _ihsg_title = document.getElementById('header_IHSG_Title');
			var _data = document.getElementById('_ihsg_ihsg')
			_data.innerHTML = _composite.formatMoney (2);

			var _data_chg = document.getElementById('_chg_ihsg')
			var _data_persen_chg = document.getElementById('_persen_ihsg')
			_data_chg.innerHTML = _chg.toFixed(2);
			_data_persen_chg.innerHTML = _persenchg.toFixed(2) + " %";
			if ( _composite < _prev )
			{
				_data_chg.className = "col_red";
				_data_persen_chg.className = "col_red";
				_ihsg_title.style.backgroundColor = '#e54044';
			}
			else if (_composite > _prev  )
			{
				_data_chg.className = "col_green";
				_data_persen_chg.className = "col_green";
				_ihsg_title.style.backgroundColor = '#01ba3a';
			}
			else if ( _composite == _prev  )
			{
				_data_chg.className = "col_yellow";
				_data_persen_chg.className = "col_yellow";
				_ihsg_title.style.backgroundColor = '#01ba3a';
			}

			_data = document.getElementById('_freq_ihsg')
			_data.innerHTML = '<font color="#1055cf">Freq</font> ' + _freq.formatMoney (0);
			_data = document.getElementById('_volume_ihsg')
			_data.innerHTML = '<font color="#1055cf">Vol</font> ' + _volume.formatMoney (0);
			_data = document.getElementById('_value_ihsg')
			
			if ( _value < 100000 )
			{
				_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " M";
			}
			else
			{
				_value = _value / 1000;
				_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " B";
			}
		}
	}
};


var _row = 0;
var _maxRow = 19;
var runningTradeHandler = 
{
	_runningTrade: function(message) 
	{

		console.log ( message );

		if (message != null)
		{
			var _stock = message.getAttribute('_s')
			if ( _stock != currentStockCode ) return;

			var _time = message.getAttribute('_t')
			var _price = parseInt ( message.getAttribute('_p') )
			var _qty = parseInt ( message.getAttribute('_q') )
			var _chg = parseInt ( message.getAttribute('_c') )
			var _persenchg = _chg * 100 / ( _price - _chg );
			var _buyerCode = message.getAttribute('_bc')
			var _buyerType = message.getAttribute('_bt')
			var _sellerCode = message.getAttribute('_sc')
			var _sellerType = message.getAttribute('_st')

			previousRow = (_row - 1 + _maxRow) % _maxRow
			var prow = document.getElementById('row' + previousRow)
			if ( previousRow % 2 == 0 )
			{
				//prow.className = 'trading_dark';
			}
			else
			{
				//prow.className = 'trading_normal';
			}

			var row = document.getElementById ('row' + _row);
			//row.className = 'trading_active';
			
			row = document.getElementById('_time_row' + _row)
			row.innerHTML = _time;

			var priceRow = document.getElementById('_price_row' + _row)
			var chgRow = document.getElementById('_chg_row' + _row)
			var persenChgRow = document.getElementById('_persenchg_row' + _row)
			if ( _chg < 0 )
			{
				priceRow.innerHTML = _price.formatMoney(0);
				priceRow.className = 'col_red';
				chgRow.innerHTML = Math.abs(_chg).formatMoney(0);
				chgRow.className = 'col_red';
				persenChgRow.innerHTML = Math.abs(_persenchg).formatMoney(2);
				persenChgRow.className = 'col_red';
			}
			if ( _chg > 0 )
			{
				priceRow.innerHTML = _price.formatMoney(0);
				priceRow.className = 'col_green';
				chgRow.innerHTML = _chg.formatMoney(0);
				chgRow.className = 'col_green';
				persenChgRow.innerHTML = _persenchg.formatMoney(2);
				persenChgRow.className = 'col_green';
			}
			if ( _chg == 0 )
			{
				priceRow.innerHTML = _price.formatMoney(0);
				priceRow.className = 'col_yellow';
				chgRow.innerHTML = _chg.formatMoney(0);
				chgRow.className = 'col_yellow';
				persenChgRow.innerHTML = _persenchg.formatMoney(2);
				persenChgRow.className = 'col_yellow';
			}


			row = document.getElementById('_qty_row' + _row)
			row.innerHTML = _qty.formatMoney(0);

			row = document.getElementById('_buyerType_row' + _row)
			if ( _buyerType == 'D' )
			{
				row.innerHTML = _buyerType;
				row.className = 'col_green';
				row = document.getElementById('_buyerCode_row' + _row)
				row.innerHTML = _buyerCode;
				row.className = 'col_green';
			}
			else
			{
				row.innerHTML = _buyerType;
				row.className = 'col_red';
				row = document.getElementById('_buyerCode_row' + _row)
				row.innerHTML = _buyerCode;
				row.className = 'col_red';
			}

			row = document.getElementById('_sellerType_row' + _row)
			if ( _sellerType == 'D' )
			{
				row.innerHTML = _sellerType;
				row.className = 'col_green';
				row = document.getElementById('_sellerCode_row' + _row)
				row.innerHTML = _sellerCode;
				row.className = 'col_green';
			}
			else
			{
				row.innerHTML = _sellerType;
				row.className = 'col_red';
				row = document.getElementById('_sellerCode_row' + _row)
				row.innerHTML = _sellerCode;
				row.className = 'col_red';
			}

			++_row;
			if ( _row >= _maxRow )
			{
				_row = 0;
			}
		}
	}
};

function portfolioPoll(first)
{
	if (first)
	{
		amq.addListener('index','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
		amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=COMPOSITE>' );
	}
}

// amq.uri='../amq';
// amq.addPollHandler(portfolioPoll);
function changeStock (obj)
{
	if ( currentStockCode.indexOf('.') == -1 )
	{
		amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode + '.RG');
		amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode + '.RG');
		amq.removeListener ( 'rt', 'topic://STOCKRUN.' + currentStockCode + '.RG');
		amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode + '.RG');
	}
	else
	{
		amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode);
		amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode);
		amq.removeListener ( 'rt', 'topic://STOCKRUN.' + currentStockCode);
		amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode);
	}

	currentStockCode = obj.value.toUpperCase();
	if ( currentStockCode.indexOf('.') == -1 )
	{
		amq.addListener('ob','topic://ORDERBOOK.' + currentStockCode + '.RG', orderBookHandler._orderBookReceived);
		amq.addListener('qt' ,'topic://QUOTES.' + currentStockCode + '.RG', priceHandler._price);
		amq.addListener('rt','topic://STOCKRUN.' + currentStockCode + '.RG', runningTradeHandler._runningTrade);
		amq.addListener('tb', 'topic://TRADEBOOK.' + currentStockCode + '.RG', tradeBookHandler._tradeBookReceived);
	}
	else
	{
		amq.addListener('ob','topic://ORDERBOOK.' + currentStockCode, orderBookHandler._orderBookReceived);
		amq.addListener('qt','topic://QUOTES.' + currentStockCode, priceHandler._price);
		amq.addListener('rt','topic://STOCKRUN.' + currentStockCode, runningTradeHandler._runningTrade);
		amq.addListener('tb', 'topic://TRADEBOOK.' + currentStockCode, tradeBookHandler._tradeBookReceived);
	}

	_row = 0;
	amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + currentStockCode + '>' );
	loadData();
}
function removeStockFromListener ( stockCode )
{
	if ( stockCode.indexOf('.') == -1 )
	{
		amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + stockCode + '.RG' );
		amq.removeListener ( 'qt', 'topic://QUOTES.' + stockCode + '.RG' );
		amq.removeListener ( 'rt', 'topic://STOCKRUN.' + stockCode + '.RG' );
		amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + stockCode + '.RG');
	}
	else
	{
		amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + stockCode);
		amq.removeListener ( 'qt', 'topic://QUOTES.' + stockCode);
		amq.removeListener ( 'rt', 'topic://STOCKRUN.' + stockCode);
		amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + stockCode);
	}
}
function unloadPage()
{
	amq.removeListener ( 'ihsg', 'topic://QUOTES.COMPOSITE.INDICES' );
	amq.removeAllListener();

	if ( currentStockCode.indexOf('.') == -1 )
	{
		amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode + '.RG' );
		amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode + '.RG' );
		amq.removeListener ( 'rt', 'topic://STOCKRUN.' + currentStockCode + '.RG' );
		amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode + '.RG');
	}
	else
	{
		amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode);
		amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode);
		amq.removeListener ( 'rt', 'topic://STOCKRUN.' + currentStockCode);
		amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode);
	}

}
function handleKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		obj.value = obj.value.toUpperCase();
		if ( obj.value.indexOf('.') == -1 )
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value + ':' ) >= 0 )
			{
				clearRunning();
				changeStock ( obj );
			}
			else
			{
				alertInvalid(obj);
			}
		}
		else
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value.substring ( 0, obj.value.indexOf('.') )  + ':' ) >= 0 )
			{
				var _board = obj.value.substring (obj.value.indexOf('.'));
				if ( _board == '.TN' || _board == '.NG' )
				{
					changeStock ( obj );
				}
				else
				{
					alertInvalid(obj);
				}
			}
			else
			{
				alertInvalid(obj);
			}
		}

		obj.select();
		obj.focus();
		return false;
	}
}
function alertInvalid(obj)
{
	document.getElementById('_stockName').innerHTML = 'Invalid Stock Code';
}
function clearRunning()
{
	var i = 0;
	for (i = 0; i < _maxRow; ++i)
	{
		document.getElementById('_time_row'+i).innerHTML = '&nbsp;'
		document.getElementById('_price_row'+i).innerHTML = '&nbsp;'
		document.getElementById('_chg_row'+i).innerHTML = '&nbsp;'
		document.getElementById('_persenchg_row'+i).innerHTML = '&nbsp;'
		document.getElementById('_qty_row'+i).innerHTML = '&nbsp;'
		document.getElementById('_buyerType_row'+i).innerHTML = '&nbsp;'
		document.getElementById('_buyerCode_row'+i).innerHTML = '&nbsp;'
		document.getElementById('_sellerCode_row'+i).innerHTML = '&nbsp;'
		document.getElementById('_sellerType_row'+i).innerHTML = '&nbsp;'
	}
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};


function loadData()
{
	// var _stock = document.getElementById('_stockCode').value;
	var _stock = currentStockCode;
	if (_stock.length > 1)
	{
		var _viewButton = document.getElementById('_refresh');
		_viewButton.value = 'Loading..';
		_viewButton.disabled = true;
		makeRequest('../servlets/SummaryServlet?_a=sbs&_st=' + _stock );
	}
}
function receivedData(result)
{
	var _data = result.split('|');
	// var _brokerSummary = '';
	clearTable();
	var _brokerSummary = document.getElementById("_brokerSummary");
	for(var i=0;i<_data.length;++i)
	{
		try
		{
			if (_data[i].length <= 3) continue;
			_subdata = _data[i].split('#');
			//if (i%2==0) _brokerSummary += '<tr class="stock_running_detail_white">';
			//else _brokerSummary += '<tr class="stock_running_detail_grey">';

			_tr = _brokerSummary.insertRow(_brokerSummary.rows.length);
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			bVol = parseInt(_subdata[1]);
			sVol = parseInt(_subdata[3]);

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("class", 'col_grey');
			_td.setAttribute("align", 'right');
			_td.innerHTML = bVol.formatMoney(0);
			// _brokerSummary += '<td class="col_grey">'+bVol.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("class", 'col_grey');
			_td.setAttribute("align", 'center');
			_td.innerHTML = _subdata[0];
			// _brokerSummary += '<td align="center" class="col_grey">'+_subdata[0]+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("class", 'col_grey');
			_td.setAttribute("align", 'center');
			_td.innerHTML = _subdata[2];
			// _brokerSummary += '<td align="center" class="col_grey">'+_subdata[2]+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("class", 'col_grey');
			_td.setAttribute("align", 'right');
			_td.innerHTML = sVol.formatMoney(0);
			// _brokerSummary += '<td class="col_grey">'+sVol.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = '&nbsp;';
			// _brokerSummary += '<td></td>';
			//_brokerSummary += '</tr>';
		}
		catch(e){}
	}
	//_brokerSummary += '<tr class="stock_running_detail_white">';
	//_brokerSummary += '<td height="10px" colspan="5"></td>';
	//_brokerSummary += '</tr>';
	// document.getElementById('_brokerSummary').innerHTML = _brokerSummary;

	var _viewButton = document.getElementById('_refresh');
	_viewButton.value = 'Refresh';
	_viewButton.disabled = false;
}
function clearTable()
{
	var tbody = document.getElementById("_brokerSummary");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
function makeRequest(url)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			// set type accordingly to anticipated content type
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	http_request.onreadystatechange = alertContents;
	http_request.open('GET', url, true);
	http_request.send(null);
}
function alertContents()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			receivedData(http_request.responseText);
		}
	}
}


function startScreen()
{
	amq.addListener('ihsg','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
	// amq.addListener('rt','topic://RUNNING.TRADE',runningTradeHandler._runningTrade);
	amq.sendMessage('topic://REQUEST.FIRST', '<c s=COMPOSITE>');
}
function stopScreen()
{
	try
	{
		amq.removeAllListener();
		// ihsgHandler.stopIhsg();
	}
	catch (err) {}
}
