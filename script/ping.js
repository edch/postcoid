var pingScript = 
{
	_savedTitle : '',
	http_request : false,
	timeoutVar : false,

	doPing : function()
	{
		pingScript.makeRequest('../servlets/PingServlet');
		// timeoutVar = setTimeout ( 'pingScript.doPing()', 59000 );
		timeoutVar = setTimeout ( 'pingScript.doPing()', 45000 );
	},

	makeRequest : function (url)
	{
		pingScript.http_request = false;
		if (window.XMLHttpRequest)
		{ // Mozilla, Safari,...
			pingScript.http_request = new XMLHttpRequest();
			if (pingScript.http_request.overrideMimeType)
			{
				// set type accordingly to anticipated content type
				pingScript.http_request.overrideMimeType('text/html');
			}
		} else if (window.ActiveXObject)
		{ // IE
			try {
				pingScript.http_request = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					pingScript.http_request = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {}
			}
		}
		if (!pingScript.http_request)
		{
			return false;
		}

		pingScript.http_request.onreadystatechange = pingScript.receivedPing;
		pingScript.http_request.open('GET', url, true);
		pingScript.http_request.send(null);
	},

	receivedPing : function ()
	{
		if (pingScript.http_request.readyState == 4)
		{
			if (pingScript.http_request.status == 200)
			{
				document.title = pingScript.http_request.responseText + pingScript._savedTitle;
			}
			else if (pingScript.http_request.status == 403)
			{
				window.location = '..//timeout.jsp';
			}
			else
			{
				document.title = '[OFF] ' + pingScript._savedTitle;
			}
		}
	},

	doStartPing: function()
	{
		pingScript._savedTitle = document.title;
		pingScript.doPing();
	}
}


pingScript.doStartPing();

