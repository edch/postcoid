var priceHandler = 
{
	_price: function(message) 
	{
		if (message != null)
		{
			processQuotes ( message );
		}
	}
};
var ihsgHandler = 
{
	_ihsg: function(message)
	{
		if (message != null) {
			var _composite = parseFloat ( message.getAttribute ( '_c' ) )
			var _prev = parseFloat ( message.getAttribute ( '_p' ) )
			var _freq = parseInt ( message.getAttribute ( '_f' ) )
			var _volume = parseFloat ( message.getAttribute ( '_vo' ) )
			var _value = parseFloat ( message.getAttribute ( '_va' ) ) / 1000000
			var _chg = Math.abs ( _composite - _prev );
			var _persenchg = (_chg / _prev) * 100;
			var _data = document.getElementById('_ihsg_ihsg')
			_data.innerHTML = _composite.formatMoney (2);

			var _ihsg_title = document.getElementById('header_IHSG_Title');
			var _data_chg = document.getElementById('_chg_ihsg')
			var _data_persen_chg = document.getElementById('_persen_ihsg')
			_data_chg.innerHTML = _chg.toFixed(2);
			_data_persen_chg.innerHTML = _persenchg.toFixed(2) + " %";
			if ( _composite < _prev )
			{
				_data_chg.className = "col_red";
				_data_persen_chg.className = "col_red";
				_ihsg_title.style.backgroundColor = '#e54044';
			}
			else if (_composite > _prev  )
			{
				_data_chg.className = "col_green";
				_data_persen_chg.className = "col_green";
				_ihsg_title.style.backgroundColor = '#01ba3a';
			}
			else if ( _composite == _prev  )
			{
				_data_chg.className = "col_yellow";
				_data_persen_chg.className = "col_yellow";
				_ihsg_title.style.backgroundColor = '#01ba3a';
			}

			_data = document.getElementById('_freq_ihsg')
			_data.innerHTML = '<font color="#1055cf">Freq</font> ' + _freq.formatMoney (0);
			_data = document.getElementById('_volume_ihsg')
			_data.innerHTML = '<font color="#1055cf">Vol</font> ' + _volume.formatMoney (0);
			_data = document.getElementById('_value_ihsg')
			
			if ( _value < 100000 )
			{
				_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " M";
			}
			else
			{
				_value = _value / 1000;
				_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " B";
			}
		}
	}
};
var _maxRow = 19;
function processQuotes ( message )
{
	if ( message != null )
	{
		var _row = 0;
		var _stock = message.getAttribute ( '_s' ).toUpperCase();
		var _time = message.getAttribute ( '_t' );
		var _last = parseFloat ( message.getAttribute ( '_l' ) )
		var _prev = parseFloat ( message.getAttribute ( '_p' ) )
		var _open = parseFloat ( message.getAttribute ( '_o' ) )
		var _high = parseFloat ( message.getAttribute ( '_hi' ) )
		var _low = parseFloat ( message.getAttribute ( '_lo' ) )
		var _freq = parseFloat ( message.getAttribute ( '_f' ) )
		var _vol = parseFloat ( message.getAttribute ( '_vo' ) ) / _LOT_SIZE;
		var _val = parseFloat ( message.getAttribute ( '_va' ) ) / 1000000

		var _chg = Math.abs ( _last - _prev )
		var _persenchg = (_chg / _prev) * 100;

		var _eStyle = "col_green";
		if ( _last < _prev )
		{
			_eStyle = "col_red";
		}
		else if ( _last > _prev )
		{
			_eStyle = "col_green";
		}
		else if ( _last == _prev )
		{
			_eStyle = "col_yellow";
		}

		for ( _row = 0; _row < _maxRow; ++_row )
		{
			var _codeRow = document.getElementById ("_code_row" + _row ).innerHTML;
			if ( _codeRow == _stock )
			{
				document.getElementById ("_last_row" + _row).innerHTML = _last.formatMoney(3);
				document.getElementById ("_last_row" + _row).className = _eStyle;

				document.getElementById ("_chg_row" + _row).innerHTML = _chg.formatMoney(3);
				document.getElementById ("_chg_row" + _row).className = _eStyle;

				document.getElementById ("_persenchg_row" + _row).innerHTML = _persenchg.formatMoney(3);
				document.getElementById ("_persenchg_row" + _row).className = _eStyle;

				document.getElementById ("_time_row" + _row).innerHTML = _time;

				if ( _prev < _open )
				{
					document.getElementById ("_open_row" + _row).className = "col_green";
				}
				else if ( _prev > _open )
				{
					document.getElementById ("_open_row" + _row).className = "col_red";
				}
				else if ( _prev == _open )
				{
					document.getElementById ("_open_row" + _row).className = "col_yellow";
				}

				document.getElementById ("_open_row" + _row).innerHTML = _open.formatMoney(3);

				if ( _prev < _high )
				{
					document.getElementById ("_high_row" + _row).className = "col_green";
				}
				else if ( _prev > _high )
				{
					document.getElementById ("_high_row" + _row).className = "col_red";
				}
				else if ( _prev == _high )
				{
					document.getElementById ("_high_row" + _row).className = "col_yellow";
				}

				document.getElementById ("_high_row" + _row).innerHTML = _high.formatMoney(3);

				if ( _prev < _low )
				{
					document.getElementById ("_low_row" + _row).className = "col_green";
				}
				else if ( _prev > _low )
				{
					document.getElementById ("_low_row" + _row).className = "col_red";
				}
				else if ( _prev == _low )
				{
					document.getElementById ("_low_row" + _row).className = "col_yellow";
				}

				document.getElementById ("_low_row" + _row).innerHTML = _low.formatMoney(3);

				document.getElementById ("_prev_row" + _row).className = "col_yellow";
				document.getElementById ("_prev_row" + _row).innerHTML = _prev.formatMoney(3);
				break;
			}
		}
	}
}

function portfolioPoll(first)
{
	if (first)
	{
		// amq.addListener('index','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
		// amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=COMPOSITE>' );
		for (i = 0; i < _maxRow; ++i)
		{
			var c = document.getElementById('_code_row'+i).innerHTML;
			// amq.addListener('quotes' + c + '.INDICES','topic://QUOTES.' + c + '.INDICES', priceHandler._price);
			amq.addListener('ihsg','topic://QUOTES.' + c + '.INDICES', priceHandler._price);
			amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + c + '>' );
		}
	}
}

//amq.uri='../amq';
//amq.addPollHandler(portfolioPoll);
function unloadPage (unloadIndex)
{
	if (unloadIndex == 'yes')
	{
		// amq.removeListener ( 'index', 'topic://QUOTES.COMPOSITE.INDICES');
		amq.removeListener ( 'ihsg', 'topic://QUOTES.COMPOSITE.INDICES');
		amq.removeAllListener();
	}
	
	var i = 0;
	for (i = 0; i < _maxRow; ++i)
	{
		var c = document.getElementById('_code_row'+i).innerHTML;
		// amq.removeListener('quotes' + c + '.INDICES', 'topic://QUOTES.' + c + '.INDICES');
		amq.removeListener('ihsg', 'topic://QUOTES.' + c + '.INDICES');
	}
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};

function startScreen()
{
	portfolioPoll ( true );
}
function stopScreen()
{
	try
	{
		amq.removeAllListener();
		// ihsgHandler.stopIhsg();
	}
	catch (err) {}
}
