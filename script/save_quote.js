// var _maxRow = 20;
function handleSaveQuotes ()
{
	var group = prompt ('Please Enter Group Name', 'Group1');
	var i = 0;
	var quotesString = '';
	var needSaving = false;
	for (i = 0; i < _maxRow; ++i)
	{
		try
		{
			if ( document.getElementById("_code_row" + i).innerHTML != '' )
			{
				quotesString += document.getElementById("_code_row" + i).innerHTML;
				quotesString += ':';
				needSaving = true;
			}
		}
		catch ( err ) {}
	}

	if ( needSaving )
	{
		var poststr = "action=savegroup&group=" + group + "&quotes=" + quotesString;
		document.getElementById('_send').value = 'Saving ...';
		document.getElementById('_send').disabled = true
		makePOSTRequest('../servlets/SaveQuotesServlet', poststr);
	}
}
function handleDeleteQuotes ()
{
	var group = document.getElementById("_group").options[ document.getElementById("_group").selectedIndex ].value;
	var poststr = "action=deletegroup&group=" + group;

	// alert ( poststr );

	document.getElementById('_remove').value = 'Deleting ...';
	document.getElementById('_remove').disabled = true;
	makePOSTRequest('../servlets/SaveQuotesServlet', poststr);
}
function loadGroupList()
{
	var poststr = "action=loadgroup";
	makePOSTRequest('../servlets/SaveQuotesServlet', poststr);
}
function loadQuoteList(obj)
{
	if ( obj.selectedIndex != 0 )
	{
		var poststr = "action=loadquote&group=" + obj.options[obj.selectedIndex].value;
		makePOSTRequest('../servlets/SaveQuotesServlet', poststr);
	}
}

var http_request = false;
function makePOSTRequest(url, parameters)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			// set type accordingly to anticipated content type
			//http_request.overrideMimeType('text/xml');
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_request)
	{
		alert('Cannot create XMLHTTP instance');
		return false;
	}

	http_request.onreadystatechange = alertContents;
	http_request.open('POST', url, true);
	http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_request.setRequestHeader("Content-length", parameters.length);
	http_request.setRequestHeader("Connection", "close");
	http_request.send(parameters);
}

function alertContents()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			result = http_request.responseText;
			// alert ( result );

			var results = result.split ('|');
			if ( results[0] == "savegroup" )
			{
				alert ( results[1] );
				loadGroupList();
			}
			else if ( results[0] == "loadgroup" )
			{
				var groupName = results[1].split(':');
				var groupSelect = document.getElementById('_group');
				groupSelect.length = groupName.length;
				groupSelect.options[0] = new Option ( '', '' );
				groupSelect.selectedIndex = 0;
				for (i = 0; i < groupName.length-1; ++i)
				{
					groupSelect.options[i+1] = new Option ( groupName[i], groupName[i] );
				}
			}
			else if ( results[0] == "loadquote" )
			{
				processStockList(results[1]);
			}
			else if ( results[0] == "deletegroup" )
			{
				document.getElementById('_remove').value = 'Delete Group';
				document.getElementById('_remove').disabled = false;

				alert ( results[1] );
				loadGroupList();
			}

			document.getElementById('_send').value = 'Save Group';
			document.getElementById('_send').disabled = false
		}
		else if ( http_request.status == 302 )
		{
			alert ('New Location [' + http_request.getResponseHeader('Location') + ']');
		}
		else
		{
			alert ('There was a problem with the request. [' + http_request.status + ']');
		}
	}
}
