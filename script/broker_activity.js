function loadData()
{
	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'Loading..';
	_viewButton.disabled = true;

	var _dateFrom = document.getElementById('popupDatepicker').value;
	var _dateTo = document.getElementById('popupDatepicker2').value;
	var _broker = document.getElementById('_brokerSelect').value;
	makeRequest('../servlets/SummaryServlet?_a=ba&_df=' + _dateFrom + '&_dt=' + _dateTo + '&_b=' + _broker, false);
}
function receivedData(result)
{
	var _data = result.split('|');
	var i;
	var bFreq=0,bVol=0,bVal=0,sFreq=0,sVol=0,sVal=0;
	clearTable();
	var _tsorter = document.getElementById("tsorter");
	for(i=1;i<_data.length;++i)
	{
		try
		{
			if (_data[i].length <= 3) continue;
			_subdata = _data[i].split('#');
			_tr = _tsorter.insertRow(_tsorter.rows.length);
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;vertical-align:top;');
			_td.innerHTML = "<div align='left'>"+_subdata[0]+"</div>";
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;"><div align="center">'+_subdata[0]+'</div></td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;padding-left:2px;');
			_td.innerHTML = "<div align='left'>"+_subdata[1]+"</div>";
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;padding-left:2px;"><div align="left">'+_subdata[1]+'</div></td>';

			bFreq = parseInt(_subdata[2]);
			bVol = parseInt(_subdata[3]);
			bVal = parseInt(_subdata[4]);
			sFreq = parseInt(_subdata[5]);
			sVol = parseInt(_subdata[6]);
			sVal = parseInt(_subdata[7]);
			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = bFreq.formatMoney(0);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+bFreq.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = bVol.formatMoney(0);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+bVol.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = (bVal/1000000).formatMoney(2);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+(bVal/1000000).formatMoney(2)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = (bVal/(bVol*_LOT_SIZE)).formatMoney(2);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+(bVal/(bVol*_LOT_SIZE)).formatMoney(2)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = sFreq.formatMoney(0);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+sFreq.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = sVol.formatMoney(0);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+sVol.formatMoney(0)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = (sVal/1000000).formatMoney(2);
			// _tsorter += '<td style="border-right:1px solid #c1c1c1;padding-right:2px;">'+(sVal/1000000).formatMoney(2)+'</td>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('style','border-right:1px solid #c1c1c1;padding-right:2px;');
			_td.innerHTML = (sVal/(sVol*_LOT_SIZE)).formatMoney(2);
			// _tsorter += '<td>'+(sVal/(sVol*_LOT_SIZE)).formatMoney(2)+'</td>';
			// _tsorter += '</tr>';
		}
		catch(e){}
	}
	// document.getElementById('tsorter').innerHTML = _tsorter;
	$("table").trigger("update");

	var _viewButton = document.getElementById('sum_view');
	_viewButton.value = 'View';
	_viewButton.disabled = false;
}
function clearTable()
{
	var tbody = document.getElementById("tsorter");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
function makeRequest(url, isBroker)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			// set type accordingly to anticipated content type
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}
	if (!http_request)
	{
		return false;
	}
	if ( !isBroker )
	{
		http_request.onreadystatechange = alertContents;
	}
	else
	{
		http_request.onreadystatechange = parseBroker;
	}
	http_request.open('GET', url, true);
	http_request.send(null);
}
function alertContents()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			receivedData(http_request.responseText);
		}
		else if ( http_request.status == 302 )
		{
			//alert ('New Location [' + http_request.getResponseHeader('Location') + ']');
		}
		else
		{
			//alert('There was a problem with the request. [' + http_request.status + ']');
		}
	}
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};
function loadBroker()
{
	makeRequest('../servlets/SummaryServlet?_a=lb', true);
}
function parseBroker()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			receivedBroker(http_request.responseText);
		}
		else if ( http_request.status == 302 )
		{
		}
		else
		{
		}
	}
}
function receivedBroker(result)
{
	var _data = result.split('|');
	var i,j;
	var _brokerSelect = document.getElementById('_brokerSelect');
	_brokerSelect.options.length = 0;//clear
	for (i=0;i<_data.length;++i)
	{
		if (_data[i].length <= 3) continue;
		var _subdata = _data[i].split('#');
		// _brokerSelect.add ( new Option(_subdata[0]+'-'+_subdata[1],_subdata[0]), null );
		_brokerSelect.options[i] = new Option(_subdata[0]+'-'+_subdata[1], _subdata[0], false, false);
	}
}
function loadTodayString()
{
	var d = new Date();
	var _todayString = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
	document.getElementById("popupDatepicker").value = _todayString;
	document.getElementById("popupDatepicker2").value = _todayString;
}
