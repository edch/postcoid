function setCookie ( name, value )
{
	//var today = new Date();
	//today.setTime ( today.getTime() );
	//var expires_date = new Date ( today.getTime() + 2000 );
	document.cookie = name + "=" + escape ( value );
}
function setAccountCookies ( value )
{
	// alert ('Save cookie [' + value + ']');
	document.cookie = 'acc=' + escape(value) + ';path=/';
}
function getAccountCookies ()
{
	return getCookie('acc');
}
function setAccountSelectedByCookies ( comboId )
{
	try
	{
		var acc = getCookie('acc');
		// alert ('Load cookie [' + acc + ']');
		if (acc.length > 0)
		{
			var combo = document.getElementById(comboId);
			for ( var i = 0; i < combo.options.length; ++i )
			{
				if ( combo.options[i].value == acc )
				{
					combo.selectedIndex = i;
					break;
				}
			}
		}
	}
	catch (err) {}
}

function getCookie ( check_name )
{
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f

	for ( i = 0; i < a_all_cookies.length; i++ )
	{
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );

		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		// if the extracted name matches passed check_name
		if ( cookie_name == check_name )
		{
			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( a_temp_cookie.length > 1 )
			{
				cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// note that in cases where cookie is initialized but no value, null is returned
			return cookie_value;
			break;
		}

		a_temp_cookie = null;
		cookie_name = '';
	}

	if ( !b_cookie_found )
	{
		return null;
	}
}


String.prototype.getAttribute = function (_tag)
{
	var tag = ' ' + _tag + '=\'';
	// console.log ( 'getattribute ' + tag + ' ' + this);

	// var startIndex = this.indexOf ( tag ) + tag.length + 1;
	// var startIndex = this.indexOf ( tag ) + tag.length;
	var startIndex = this.indexOf ( tag );
	if ( startIndex < 0 ) return null;

	startIndex += tag.length;
	var endIndex = this.indexOf ( '\'', startIndex );
	var result = this.substring ( startIndex, endIndex );

	// console.log ( 'getattribute result ' + tag + ' ' + startIndex + ' ' + endIndex + ' ' +  result );

    return result;
};

