var _sessionTimer;
function sessionStart()
{
	var _timeout = 60 * 60 * 1000; // 5 menit
	setTimeout("sessionTimeout()", _timeout);
}
function sessionTimeout()
{
	var di = document.getElementById('timeout_expired');
	try
	{
		var x = (window.innerWidth / 2) - (di.width / 2);
		var y = (window.offsetHeight / 2);
		di.style.top = y + 'px';
		di.style.left = x + 'px';
	}
	catch (err) {}
	di.style.display = 'block';
	var _timeout = 2 * 60 * 1000; // 2 menit
	_sessionTimer = setTimeout("sessionLogout()", _timeout);
}
function sessionExtends()
{
	var di = document.getElementById('timeout_expired');
	di.style.display = 'none';
	try
	{
		clearTimeout ( _sessionTimer );
	}
	catch (err) {}
	sessionStart();
}
function sessionLogout()
{
	window.location = '../timeout.jsp';
}
sessionStart();
