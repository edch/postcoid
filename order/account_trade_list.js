var http_request = false;
var _savedPin = '';
function makeRequest(url,callerId)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	if (callerId == 'loadAccount')
	{
		http_request.onreadystatechange = loadAccountResult;
	}
	else if(callerId == 'loadTrade')
	{
		http_request.onreadystatechange = loadTradeResult;
	}
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadAccountResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadAccountResult(http_request.responseText);
		}
		else if ( http_request.status == 302 )
		{
		}
		else
		{
		}
	}
}
function parseLoadAccountResult(result)
{
	var _account = document.getElementById('_account');
	var acc = result.split('#');
	_account.options.length = 0;
	for (i=0; i<acc.length; ++i)
	{
		if ( acc[i].length <= 0 )
		{
			continue;
		}
		try
		{
			_account.options[i] = new Option(acc[i], acc[i], false, false);
		}
		catch (err){}
	}
}
function loadAccount()
{
	// makeRequest('../servlets/AccountServlet?_a=acclist','loadAccount');
	parseLoadAccountResult ( _accountList );
}

function loadTradeCommand()
{
	document.getElementById('_enter').value = 'Load';
	document.getElementById('_enter').disabled = true;
	document.getElementById('_refresh').value = 'Loading..';
	document.getElementById('_refresh').disabled = true;
	// var pin =  document.getElementById("_pin").value;
	loadPin();
	var account =  document.getElementById("_account").value;
	var stock =  document.getElementById("_tradeStockCode").value;
	var dateFrom = document.getElementById('popupDatepicker').value;
	var dateTo = document.getElementById('popupDatepicker2').value;
	makeRequest('../servlets/AccountServlet?_a=tradelist&_account=' + account + '&_pin=' + _savedPin + '&_st=' + stock + '&_df=' + dateFrom + '&_dt=' + dateTo ,'loadTrade');
}
function loadTradeResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadTradeResult(http_request.responseText);
		}
		else if ( http_request.status == 403 )
		{
			invalidPin();
		}
		else if ( http_request.status == 404 )
		{
			invalidPin();
		}
		else
		{
		}
	}
}
function invalidPin()
{
	document.getElementById('sp_post').innerHTML = 'Wrong PIN';
	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
	document.getElementById('_refresh').value = 'Refresh';
	document.getElementById('_refresh').disabled = false;
	// document.getElementById("_tradeListTable").innerHTML = '';
	document.getElementById("_totalBuy").innerHTML = "0";
	document.getElementById("_totalSell").innerHTML = "0";
	document.getElementById("_totalFee").innerHTML = "0"
	document.getElementById("_nett").innerHTML = "0";
	clearTable();
}
function clearTable()
{
	var tbody = document.getElementById("_tradeListTable");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
var _currentCash=0;
var _tradingLimit=0;
var _buyFee=0;
var _sellFee=0;
function parseLoadTradeResult(result)
{
	var _data = result.split('|');
	var _subdata = _data[0].split('#');
	var _tBuy=0,_tSell=0,_tFee=0;
	_currentCash = parseInt(_subdata[0]);
	_tradingLimit = parseInt(_subdata[1]);
	_buyFee = parseFloat(_subdata[2]);
	_sellFee = parseFloat(_subdata[3]);
	document.getElementById('sp_post').innerHTML = _subdata[4];

	// var _tradeList = '';
	var _tr,_td;
	clearTable();
	var _tradeList = document.getElementById("_tradeListTable");
	for(var i=1;i<_data.length;++i)
	{
		if (_data[i].length<=3) continue;
		_subdata = _data[i].split('#');
		_tr = _tradeList.insertRow(_tradeList.rows.length);
		if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
		else _tr.setAttribute("class", "stock_running_detail_white");

		//_tradeList += '<td align="left">'+_subdata[0]+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = "<div align='left'>"+_subdata[0]+"</div>";

		//_tradeList += '<td align="left">'+_subdata[1]+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = "<div align='left'>"+_subdata[1]+"</div>";

		//_tradeList += '<td>'+_subdata[2]+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = _subdata[2];

		// _tradeList += '<td>'+_subdata[3]+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = _subdata[3];

		var p=parseInt(_subdata[6]);
		var v=parseInt(_subdata[7]);
		var _fee=0;
		// if (_subdata[4]=='0' || _subdata[4]=='M')
		if (_subdata[4]=='0' || _subdata[4]=='2')
		{
			// _tradeList += '<td class="col_red">Buy</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = 'Buy';
			_td.setAttribute('class','col_red');

			_tBuy+=(p*v*_LOT_SIZE);
			_fee=(p*v*_LOT_SIZE)*_buyFee;
		}
		else if(_subdata[4]=='1')
		{
			// _tradeList += '<td class="col_green">Sell</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = 'Sell';
			_td.setAttribute('class','col_green');

			_tSell+=(p*v*_LOT_SIZE);
			_fee=(p*v*_LOT_SIZE)*_sellFee;
		}

		_tFee+=_fee;

		// _tradeList += '<td>'+_subdata[5]+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		// _td.innerHTML = _subdata[5];
		_td.innerHTML = '<div align="left">' + _subdata[5] + '</div>';

		// _tradeList += '<td>'+p.formatMoney(0)+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = p.formatMoney(0);

		// _tradeList += '<td>'+v.formatMoney(0)+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = v.formatMoney(0);

		// _tradeList += '<td>'+(p*v*_LOT_SIZE).formatMoney(0)+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = (p*v*_LOT_SIZE).formatMoney(0);

		// _tradeList += '<td>'+_fee.formatMoney(0)+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = _fee.formatMoney(0);
	}

	// document.getElementById("_tradeListTable").innerHTML = _tradeList;
	document.getElementById("_totalBuy").innerHTML = _tBuy.formatMoney(0);
	document.getElementById("_totalSell").innerHTML = _tSell.formatMoney(0);
	document.getElementById("_totalFee").innerHTML = _tFee.formatMoney(0);

	var _nett = _tSell - _tBuy - _tFee;
	// if ((_tBuy-_tFee)>_tSell)
	if ( _nett < 0 )
	{
		document.getElementById("_nett").innerHTML = '-' + (- _nett).formatMoney(0);
		document.getElementById("_nett").className = "col_red";
	}
	else
	{
		document.getElementById("_nett").innerHTML = _nett.formatMoney(0);
		document.getElementById("_nett").className = "col_green";
	}

	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
	document.getElementById('_refresh').value = 'Refresh';
	document.getElementById('_refresh').disabled = false;
}
function loadPin()
{
	var _t = document.getElementById('_pin').value;
	if (_t&&_t.length>1) _savedPin = _t;
	document.getElementById('_pin').value = '';
}
function handlePinKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		loadTradeCommand();
		return false;
	}
}
function loadTodayString()
{
	var d = new Date();
	var _todayString = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
	document.getElementById("popupDatepicker").value = _todayString;
	document.getElementById("popupDatepicker2").value = _todayString;
}
