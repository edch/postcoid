var currentStockCode = 'XXX'
var _prev = 0
var priceHandler = 
{
	_price: function(message) 
	{
		if (message != null)
		{
			// quotes
			try
			{
			var _stock = message.getAttribute ( '_s' ).toUpperCase();
			var _last = parseInt ( message.getAttribute ( '_l' ) )
			_prev = parseInt ( message.getAttribute ( '_p' ) )
			var _open = parseInt ( message.getAttribute ( '_o' ) )
			var _high = parseInt ( message.getAttribute ( '_hi' ) )
			var _low = parseInt ( message.getAttribute ( '_lo' ) )
			var _freq = parseInt ( message.getAttribute ( '_f' ) )
			var _vol = parseInt ( message.getAttribute ( '_vo' ) ) / _LOT_SIZE;
			var _val = parseInt ( message.getAttribute ( '_va' ) ) / 1000000
			var _chg = Math.abs ( _last - _prev )
			var _persenchg = (_chg / _prev) * 100;
			var _stockName = message.getAttribute ( '_n' )

			if ( _stock != currentStockCode )
			{
				removeStockFromListener ( _stock );
				return false;
			}

			if ( _stockName != null )
			{
				document.getElementById ("_stockName").innerHTML = _stockName;
			}

			var _eStyle = "col_yellow";
			var _eChg = "";

			if ( _last < _prev )
			{
				_eStyle = "col_red";
				_eChg = "&nbsp; ";
			}
			else if ( _last > _prev )
			{
				_eStyle = "col_green";
				_eChg = "&nbsp; ";
			}
			else if ( _last == _prev )
			{
				_eStyle = "col_yellow";
				_eChg = "&nbsp;";
			}

			var _element = document.getElementById ("_last");
			_element.innerHTML = _last.formatMoney (0);
			_element.className = _eStyle;
			
			_element = document.getElementById ("_chg");
			_element.innerHTML = _eChg + " " + _chg.formatMoney (0);
			_element.className = _eStyle;
			
			_element = document.getElementById ("_prev");
			_element.innerHTML = _prev.formatMoney (0);
			_element.className = "col_yellow";
			
			_element = document.getElementById ("_persenchg");
			_element.innerHTML = _persenchg.formatMoney (2);
			_element.className = _eStyle;
			
			_element = document.getElementById ("_open");
			_element.innerHTML = _open.formatMoney (0);			
			if ( _open < _prev ) _element.className = "col_red";
			else if ( _open > _prev ) _element.className = "col_green";
			else if ( _open == _prev ) _element.className = "col_yellow";
			
			_element = document.getElementById ("_high");
			_element.innerHTML = _high.formatMoney (0);			
			if ( _high < _prev ) _element.className = "col_red";
			else if ( _high > _prev ) _element.className = "col_green";
			else if ( _high == _prev ) _element.className = "col_yellow";

			_element = document.getElementById ("_low");
			_element.innerHTML = _low.formatMoney (0);			
			if ( _low < _prev ) _element.className = "col_red";
			else if ( _low > _prev ) _element.className = "col_green";
			else if ( _low == _prev ) _element.className = "col_yellow";

			_element = document.getElementById ("_freq");
			_element.innerHTML = _freq.formatMoney (0);
			_element = document.getElementById ("_vol");
			_element.innerHTML = _vol.formatMoney (0);
			_element = document.getElementById ("_val");
			
			if ( _val < 100000 )
			{
				_element.innerHTML = _val.formatMoney(2) + " M";
			}
			else			
			{
				_val /= 1000;
				_element.innerHTML = _val.formatMoney(2) + " B";
			}
			}catch(err){}
		}
	}
};

var orderBookHandler =
{
	_orderBookReceived: function(message)
	{
		if (message != null)
		{
			try
			{
			var _stock = message.getAttribute ( '_s' ).toUpperCase();
			var _prev = parseInt ( message.getAttribute ( '_p' ) )

			if ( _stock != currentStockCode )
			{
				removeStockFromListener ( _stock );
				return false;
			}

			// order book
			var i = 0
			var _eStyle = "col_yellow";
			for (i = 0; i < 5; ++i)
			{			
				var _bidValMsg = parseInt ( message.getAttribute ( '_ba' + i ) )
				var _bidVolMsg = parseInt ( message.getAttribute ( '_bo' + i ) )
				
				var _bidVol = document.getElementById ("_bidVol" + i);
				var _bidVal = document.getElementById ("_bidVal" + i);
				if ( _bidVolMsg != null && _bidValMsg != null )
				{
					if ( _bidValMsg < _prev ) _eStyle = "col_red";
					else if ( _bidValMsg > _prev ) _eStyle = "col_green";
					else if ( _bidValMsg == _prev ) _eStyle = "col_yellow";

					_bidVol.className = _eStyle;
					_bidVal.className = _eStyle;
					_bidVol.innerHTML = _bidVolMsg.formatMoney (0);
					_bidVal.innerHTML = _bidValMsg.formatMoney (0);
				}

				var _offerValMsg = parseInt ( message.getAttribute ( '_oa' + i ) )
				var _offerVolMsg = parseInt ( message.getAttribute ( '_oo' + i ) )

				var _offerVol = document.getElementById ("_offerVol" + i);
				var _offerVal = document.getElementById ("_offerVal" + i);
				if ( _offerVolMsg != null && _offerValMsg != null )
				{
					if ( _offerValMsg < _prev ) _eStyle = "col_red";
					else if ( _offerValMsg > _prev ) _eStyle = "col_green";
					else if ( _offerValMsg == _prev ) _eStyle = "col_yellow";

					_offerVol.className = _eStyle;
					_offerVal.className = _eStyle;
					_offerVol.innerHTML = _offerVolMsg.formatMoney (0);
					_offerVal.innerHTML = _offerValMsg.formatMoney (0);
				}
			}
			}catch(err){}
		}
	}
};
var tradeBookHandler =
{
	_tradeBookReceived: function(message)
	{
		if (message != null)
		{
			// try
			{
				var _stock = message.getAttribute ( '_s' ).toUpperCase();
				if ( _stock != currentStockCode )
				{
					return false;
				}

				var _prev = parseInt ( message.getAttribute ( '_p' ) )
				var i = 0;
				// var innerCode = '';
				var _td, _tr;
				clearTradeBookTable();
				var _tradeBook = document.getElementById("_tradeBook");

				for (i = 0;; ++i)
				{
					try
					{
						var _price = message.getAttribute ( '_p' + i );
						if ( _price == null )
						{
							break;
						}

						_price = parseInt ( _price );
						var eStyle = 'col_yellow';
						if ( _price > _prev )
						{
							eStyle = 'col_green';
						}
						else if ( _price < _prev )
						{
							eStyle = 'col_red';
						}

						var _volume = parseInt ( message.getAttribute ( '_v' + i ) ) / _LOT_SIZE;
						var _freq = parseInt ( message.getAttribute ( '_f' + i ) );
					}
					catch ( err )
					{
						break;
					}


					_tr = _tradeBook.insertRow(_tradeBook.rows.length);
					//if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
					//else _tr.setAttribute("class", "stock_running_detail_white");
					if (i%2 == 0) _tr.setAttribute("style", "background-color:#e5e5e5;");

					_td = _tr.insertCell(_tr.cells.length);
					_td.setAttribute("class", eStyle);
					_td.innerHTML = _price.formatMoney(0);
					// innerCode += '<tr><td class="' + eStyle + '">' + _price.formatMoney(0) + '</td>';

					_td = _tr.insertCell(_tr.cells.length);
					_td.innerHTML = _freq.formatMoney(0);
					// innerCode += '<td>' + _freq.formatMoney(0) + '</td>';

					_td = _tr.insertCell(_tr.cells.length);
					_td.innerHTML = _volume.formatMoney(0);
					// innerCode += '<td>' + _volume.formatMoney(0) + '</td>';

					var _value = _price * _volume;
					if ( _value < 100000 )
					{
						_value /= 1000;
						_td = _tr.insertCell(_tr.cells.length);
						_td.innerHTML = _value.formatMoney (2) + ' K';
						// innerCode += '<td>' + _value.formatMoney (2) + ' K' + '</td>';
					}
					else
					{
						_value = _value / 1000000;
						_td = _tr.insertCell(_tr.cells.length);
						_td.innerHTML = _value.formatMoney (2) + ' K';
						// innerCode += '<td>' + _value.formatMoney (2) + ' M' + '</td>';
					}
					// innerCode += '</tr>';
				}
				// innerCode += '<tr><td colspan="5"></td></tr>';
				// document.getElementById( '_tradeBook' ).innerHTML = innerCode;
			}
			// catch(err){}
		}
	}
};
function clearTradeBookTable()
{
	var tbody = document.getElementById("_tradeBook");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
function clearOrderBookTable()
{
	clearTradeBookTable();
	for (i = 0; i < 5; ++i)
	{
		document.getElementById ("_bidVol" + i).innerHTML = '&nbsp;';
		document.getElementById ("_bidVal" + i).innerHTML = '&nbsp;';
		document.getElementById ("_offerVol" + i).innerHTML = '&nbsp;';
		document.getElementById ("_offerVal" + i).innerHTML = '&nbsp;';
	}

	document.getElementById ('_last').innerHTML = '&nbsp;';
	document.getElementById ('_chg').innerHTML = '&nbsp;';
	document.getElementById ('_prev').innerHTML = '&nbsp;';
	document.getElementById ('_persenchg').innerHTML = '&nbsp;';
	document.getElementById ('_open').innerHTMl = '&nbsp;';
	document.getElementById ('_high').innerHTML = '&nbsp;';
	document.getElementById ('_low').innerHTML = '&nbsp;';
	document.getElementById ('_freq').innerHTML = '&nbsp;';
	document.getElementById ('_vol').innerHTML = '&nbsp;';
	document.getElementById ('_val').innerHTML = '&nbsp;';
}
function portfolioPoll(first)
{
	if (first)
	{
		try
		{
			amq.addListener('index','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
			amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=COMPOSITE>' );
		}
		catch (err){}
	}
}
var ihsgHandler = 
{
	_ihsg: function(message)
	{
		if (message != null) {
			try
			{
				var _composite = parseFloat ( message.getAttribute ( '_c' ) )
				var _prev = parseFloat ( message.getAttribute ( '_p' ) )
				var _freq = parseInt ( message.getAttribute ( '_f' ) )
				var _volume = parseFloat ( message.getAttribute ( '_vo' ) )
				var _value = parseFloat ( message.getAttribute ( '_va' ) ) / 1000000
				var _chg = Math.abs ( _composite - _prev );
				var _persenchg = (_chg / _prev) * 100;
			
				var _data = document.getElementById('_ihsg_ihsg')
				_data.className = "col_black";
				_data.innerHTML = _composite.formatMoney (2);

				var _ihsg_title = document.getElementById('header_IHSG_Title');
				var _data_chg = document.getElementById('_chg_ihsg')
				var _data_persen_chg = document.getElementById('_persen_ihsg')
				_data_chg.innerHTML = _chg.toFixed(2);
				_data_persen_chg.innerHTML = _persenchg.toFixed(2) + " %";
				if ( _composite < _prev )
				{
					_data_chg.className = "col_red";
					_data_persen_chg.className = "col_red";
					_ihsg_title.style.backgroundColor = '#e54044';
				}
				else if (_composite > _prev  )
				{
					_data_chg.className = "col_green";
					_data_persen_chg.className = "col_green";
					_ihsg_title.style.backgroundColor = '#01ba3a';
				}
				else if ( _composite == _prev  )
				{
					_data_chg.className = "col_yellow";
					_data_persen_chg.className = "col_yellow";
					_ihsg_title.style.backgroundColor = '#01ba3a';
				}

				_data = document.getElementById('_freq_ihsg')
				_data.innerHTML = '<font color="#1055cf">Freq</font> ' + _freq.formatMoney (0);
				_data = document.getElementById('_volume_ihsg')
				_data.innerHTML = '<font color="#1055cf">Vol</font> ' + _volume.formatMoney (0);
				_data = document.getElementById('_value_ihsg')
			
				if ( _value < 100000 )
				{
					_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " M";
				}
				else
				{
					_value = _value / 1000;
					_data.innerHTML = '<font color="#1055cf">Val</font> ' + _value.formatMoney (2) + " B";
				}
			}
			catch (err) {}
		}
	}
};
// amq.uri='../amq';
// amq.addPollHandler(portfolioPoll);
function changeStock (obj)
{
	if ( obj.value != '' )
	{
		if ( currentStockCode != obj.value.toUpperCase() )
		{
			if ( currentStockCode.indexOf('.') == -1 )
			{
				amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + currentStockCode + '.RG' );
				amq.removeListener ( 'qt', 'topic://QUOTES.' + currentStockCode + '.RG' );
				amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + currentStockCode + '.RG' );
			}
			else
			{
				amq.removeListener ( 'ob' , 'topic://ORDERBOOK.' + currentStockCode);
				amq.removeListener ( 'qt' , 'topic://QUOTES.' + currentStockCode);
				amq.removeListener ( 'tb' , 'topic://TRADEBOOK.' + currentStockCode);
			}

			currentStockCode = obj.value.toUpperCase();
			if ( currentStockCode.indexOf('.') == -1 )
			{
				amq.addListener('ob' , 'topic://ORDERBOOK.' + currentStockCode + '.RG', orderBookHandler._orderBookReceived);
				amq.addListener('qt' , 'topic://QUOTES.' + currentStockCode + '.RG', priceHandler._price);
				amq.addListener('tb' , 'topic://TRADEBOOK.' + currentStockCode + '.RG', tradeBookHandler._tradeBookReceived);
			}
			else
			{
				amq.addListener('ob' , 'topic://ORDERBOOK.' + currentStockCode, orderBookHandler._orderBookReceived);
				amq.addListener('qt' , 'topic://QUOTES.' + currentStockCode, priceHandler._price);
				amq.addListener('tb' , 'topic://TRADEBOOK.' + currentStockCode, tradeBookHandler._tradeBookReceived);
			}

			amq.sendMessage ( 'topic://REQUEST.FIRST', '<c s=' + currentStockCode + '>' );
		}
	}
}
function removeStockFromListener ( stockCode )
{
	if ( stockCode.indexOf('.') == -1 )
	{
		amq.removeListener ( 'ob', 'topic://ORDERBOOK.' + stockCode + '.RG' );
		amq.removeListener ( 'qt', 'topic://QUOTES.' + stockCode + '.RG' );
		amq.removeListener ( 'tb', 'topic://TRADEBOOK.' + stockCode + '.RG' );
	}
	else
	{
		amq.removeListener ( 'ob' , 'topic://ORDERBOOK.' + stockCode);
		amq.removeListener ( 'qt' , 'topic://QUOTES.' + stockCode);
		amq.removeListener ( 'tb' , 'topic://TRADEBOOK.' + stockCode);
	}
}
function unloadPage ()
{
	try
	{
		amq.removeAllListener();
		if ( currentStockCode.indexOf('.') == -1 )
		{
			amq.removeListener ( 'ob' , 'topic://ORDERBOOK.' + currentStockCode + '.RG' );
			amq.removeListener ( 'qt' , 'topic://QUOTES.' + currentStockCode + '.RG' );
			amq.removeListener ( 'tb' , 'topic://TRADEBOOK.' + currentStockCode + '.RG' );
		}
		else
		{
			amq.removeListener ( 'ob' , 'topic://ORDERBOOK.' + currentStockCode);
			amq.removeListener ( 'qt' , 'topic://QUOTES.' + currentStockCode);
			amq.removeListener ( 'tb' , 'topic://TRADEBOOK.' + currentStockCode);
		}

		amq.removeListener ( 'ihsg', 'topic://QUOTES.COMPOSITE.INDICES');
	}
	catch (err){}
}
function upperKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e)
	{
		obj.value = obj.value.toUpperCase();
		return false;
	}
}
function handleKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		obj.value = obj.value.toUpperCase();
		if ( obj.value.indexOf('.') == -1 )
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value + ':' ) >= 0 )
			{
				changeStock ( obj );
			}
			else
			{
				alertInvalid(obj);
			}
		}
		else
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value.substring ( 0, obj.value.indexOf('.') )  + ':' ) >= 0 )
			{
				var _board = obj.value.substring (obj.value.indexOf('.'));
				if ( _board == '.TN' || _board == '.NG' )
				{
					changeStock ( obj );
				}
				else
				{
					alertInvalid(obj);
				}
			}
			else
			{
				alertInvalid(obj);
			}
		}

		return false;
	}
}
function alertInvalid(obj)
{
	document.getElementById('_stockName').innerHTML = '-';
	clearOrderBookTable();
}
Number.prototype.zzzformatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};

Number.prototype.formatMoney = function(decimals, decimal_sep, thousands_sep)
{ 
   var n = this,
   c = isNaN(decimals) ? 2 : Math.abs(decimals), //if decimal is zero we must take it, it means user does not want to show any decimal
   d = decimal_sep || '.', //if no decimal separator is passed we use the dot as default decimal separator (we MUST use a decimal separator)

   /*
   according to [http://stackoverflow.com/questions/411352/how-best-to-determine-if-an-argument-is-not-sent-to-the-javascript-function]
   the fastest way to check for not defined parameter is to use typeof value === 'undefined' 
   rather than doing value === undefined.
   */   
   t = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep, //if you don't want to use a thousands separator you can pass empty string as thousands_sep value

   sign = (n < 0) ? '-' : '',

   //extracting the absolute value of the integer part of the number and converting to string
   i = parseInt(n = Math.abs(n).toFixed(c)) + '', 

   j = ((j = i.length) > 3) ? j % 3 : 0; 
   return sign + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : ''); 
}

function stockLostFocus(obj)
{
	//if (!e) e = window.event;
	// if (e && e.keyCode == 13)
	{
		obj.value = obj.value.toUpperCase();
		if ( obj.value.indexOf('.') == -1 )
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value + ':' ) >= 0 )
			{
				changeStock ( obj );
			}
			else
			{
				alertInvalid(obj);
			}
		}
		else
		{
			if ( _allStocksCode.indexOf ( ':' + obj.value.substring ( 0, obj.value.indexOf('.') )  + ':' ) >= 0 )
			{
				var _board = obj.value.substring (obj.value.indexOf('.'));
				if ( _board == '.TN' || _board == '.NG' )
				{
					changeStock ( obj );
				}
				else
				{
					alertInvalid(obj);
				}
			}
			else
			{
				alertInvalid(obj);
			}
		}

		return false;
	}
}

function startScreen()
{
	amq.addListener('ihsg','topic://QUOTES.COMPOSITE.INDICES',ihsgHandler._ihsg);
	amq.sendMessage('topic://REQUEST.FIRST', '<c s=COMPOSITE>');
}
function stopScreen()
{
	try
	{
		amq.removeAllListener();
		// ihsgHandler.stopIhsg();
	}
	catch (err) {}
}
