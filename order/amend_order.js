var http_request = false;
var _savedPin = '';
function makeRequest(url,callerId)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}
	if (!http_request)
	{
		return false;
	}
	if (callerId == 'loadAccount')
	{
		http_request.onreadystatechange = loadAccountResult;
	}
	else if(callerId == 'loadOrder')
	{
		http_request.onreadystatechange = loadOrderResult;
	}
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadAccountResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadAccountResult(http_request.responseText);
		}
	}
}
function parseLoadAccountResult(result)
{
	var _account = document.getElementById('_account');
	var acc = result.split('#');
	_account.options.length = 0;
	for (i=0; i<acc.length; ++i)
	{
		if ( acc[i].length <= 0 )
		{
			continue;
		}

		try
		{
			// _account.add ( new Option(acc[i],acc[i]), null );
			_account.options[i] = new Option(acc[i], acc[i], false, false);
		}
		catch (err){}
	}
}
function loadAccount()
{
	// makeRequest('../servlets/AccountServlet?_a=acclist','loadAccount');
	parseLoadAccountResult ( _accountList );
}
function doExecuteAmend()
{
	var _stock = document.getElementById('_amendStock').innerHTML;
	var _price = document.getElementById('_amendNewPrice').value;
	var _lot = document.getElementById('_amendNewVolume').value;
	if ( _price <= 0 )
	{
		alert ('Invalid Price.');
		return;
	}
	if ( _lot <= 0 )
	{
		alert ('Invalid Volume.');
		return;
	}

	var _question = "AMEND " + _stock + " " + _lot + " Lot[s] @" + _price + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		document.getElementById('_amendExecute').value = "Please Wait ...";
		document.getElementById('_amendExecute').disabled = true
		loadPin();

		var _url = "pin=" + encodeURI( _savedPin )
		_url += "&stock=" + _stock
		_url += "&price=" + _price
		_url += "&volume=" + _lot
		_url += "&account=" + encodeURI( document.getElementById("_account").value )
		_url += "&board=" + encodeURI( document.getElementById("_amendBoard").innerHTML )
		_url += "&expiry=" + encodeURI( document.getElementById("_amendExpiry").innerHTML )
		_url += "&jsxid=" + encodeURI( document.getElementById("_amendJsxid").innerHTML )
		_url += "&orderid=" + encodeURI( document.getElementById("_amendOrderid").value )
		_url += "&action=A"
		makePOSTRequest('../servlets/OrderServlet', _url);
	}
}
function doAmend(index)
{
	document.getElementById('_amendJsxid').innerHTML = document.getElementById('jsxid'+index).innerHTML;
	document.getElementById('_amendOrderid').value = document.getElementById('orderid'+index).value;
	document.getElementById('_amendCommand').innerHTML = document.getElementById('cmd'+index).innerHTML;
	document.getElementById('_amendStock').innerHTML = document.getElementById('stock'+index).innerHTML;
	document.getElementById('_amendPrice').innerHTML = document.getElementById('price'+index).innerHTML;
	document.getElementById('_amendVolume').innerHTML = document.getElementById('remain'+index).innerHTML;
	document.getElementById("_amendBoard").innerHTML = document.getElementById('board'+index).value;
	document.getElementById("_amendExpiry").innerHTML = document.getElementById('expiry'+index).value;
	document.getElementById('_amendNewPrice').value = document.getElementById('price'+index).innerHTML.replace(/,/g,""); 
	document.getElementById('_amendNewVolume').value = document.getElementById('remain'+index).innerHTML.replace(/,/g,"");
}
var http_post = false;
function makePOSTRequest(url, parameters)
{
	http_post = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_post = new XMLHttpRequest();
		if (http_post.overrideMimeType)
		{
			http_post.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_post = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_post = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_post)
	{
		return false;
	}

	http_post.onreadystatechange = orderResult;
	http_post.open('POST', url, true);
	http_post.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_post.setRequestHeader("Content-length", parameters.length);
	http_post.setRequestHeader("Connection", "close");
	http_post.send(parameters);
}
function orderResult()
{
	if (http_post.readyState == 4)
	{
		if (http_post.status == 200)
		{
			result = http_post.responseText;
			document.getElementById('_amendExecute').value = "Execute Amend";
			document.getElementById('_amendExecute').disabled = false;
			clearOrderField();
		}
	}
}


function loadOrderCommand()
{
	document.getElementById('_enter').value = 'Load';
	document.getElementById('_enter').disabled = true;
	document.getElementById('_refresh').value = 'Loading..';
	document.getElementById('_refresh').disabled = true;
	loadPin();
	var account =  document.getElementById("_account").value;
	makeRequest('../servlets/AccountServlet?_a=orderlistc&_account=' + account + '&_pin=' + _savedPin,'loadOrder');
}
function loadOrderResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadOrderResult(http_request.responseText);
		}
		else if ( http_request.status == 403 )
		{
			invalidPin();
		}
		else if ( http_request.status == 404 )
		{
			invalidPin();
		}
	}
}
function invalidPin()
{
	document.getElementById('sp_post').innerHTML = 'Wrong PIN';
	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
	document.getElementById('_refresh').value = 'Refresh';
	document.getElementById('_refresh').disabled = false;
	// document.getElementById("_orderListTable").innerHTML = '';
	// document.getElementById("_currentStockPosition").innerHTML = '';
	clearTable();
}
function clearTable()
{
	var tbody = document.getElementById("_orderListTable");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}

	var tbody = document.getElementById("_currentStockPosition");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
var _currentCash=0;
var _tradingLimit=0;
var _buyFee=0;
var _sellFee=0;
function parseLoadOrderResult(result)
{
	var _data = result.split('|');
	var _subdata = _data[0].split('#');
	var _tBuy=0,_tSell=0;
	_currentCash = parseInt(_subdata[0]);
	_tradingLimit = parseInt(_subdata[1]);
	_buyFee = parseFloat(_subdata[2]);
	_sellFee = parseFloat(_subdata[3]);
	document.getElementById('sp_post').innerHTML = _subdata[4];

	var _tr,_td;
	clearTable();
	var _orderList = document.getElementById("_orderListTable");
	var _currentStock = document.getElementById("_currentStockPosition");
	for(var i=1;i<_data.length;++i)
	{
		if (_data[i].length<=3) continue;
		_subdata = _data[i].split('#');

		if (_subdata[0]=='S')
		{
			_tr = _currentStock.insertRow(_currentStock.rows.length);
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			// _currentStock += '<td>'+_subdata[1]+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = _subdata[1];

			// _currentStock += '<td>'+parseInt(_subdata[2]).formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = parseInt(_subdata[2]).formatMoney(0);

			// _currentStock += '<td>'+parseInt(_subdata[4]).formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = parseInt(_subdata[4]).formatMoney(0);

			// _currentStock += '<td></td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = '&nbsp;';
		}
		else
		{
			_tr = _orderList.insertRow(_orderList.rows.length);
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			// _orderList += '<td align="center">';
			_td = _tr.insertCell(_tr.cells.length);
			// _td.innerHTML = '&nbsp;';
			if (_subdata[8]=='1')
			{
				_td.innerHTML = '<input class="btn" type="button" value="Amend" onClick="doAmend('+i+');">';
			}
			else
			{
				_td.innerHTML = '<input class="btn" type="button" value="Amend" disabled>';
			}
			// _orderList += '</td>';
			// _orderList += '<td>'+_subdata[0]+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = _subdata[0];

			// _orderList += '<td id="jsxid'+i+'">'+_subdata[1]+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('id','jsxid'+i);
			_td.innerHTML = _subdata[1];

			// _orderList += '<td>'+_subdata[10]+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = _subdata[10];

			var p=parseInt(_subdata[5]);
			var r=parseInt(_subdata[6]);
			var t=parseInt(_subdata[7]);
			if (_subdata[3]=='0')
			{
				// _orderList += '<td class="col_red" id="cmd'+i+'">Buy</td>';
				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('id','cmd'+i);
				_td.setAttribute('class','col_red');
				_td.innerHTML = 'Buy';

				_tBuy+=(p*(t+r)*_LOT_SIZE);
			}
			else if(_subdata[3]=='1')
			{
				// _orderList += '<td class="col_green" id="cmd'+i+'">Sell</td>';
				_td = _tr.insertCell(_tr.cells.length);
				_td.setAttribute('id','cmd'+i);
				_td.setAttribute('class','col_green');
				_td.innerHTML = 'Sell';

				_tSell+=(p*(t+r)*_LOT_SIZE);
			}

			// _orderList += '<td id="stock'+i+'">'+_subdata[4]+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('id','stock'+i);
			// _td.innerHTML = '<div align="left">' + _subdata[4] + '</div>';
			_td.setAttribute('align','left');
			_td.innerHTML = _subdata[4];

			// _orderList += '<td id="price'+i+'">'+p.formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('id','price'+i);
			_td.innerHTML = p.formatMoney(0);

			// _orderList += '<td id="remain'+i+'">'+r.formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute('id','remain'+i);
			_td.innerHTML = r.formatMoney(0);

			// _orderList += '<td>'+t.formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = t.formatMoney(0);

			_td.innerHTML += '<input type="hidden" id="board'+i+'" value="'+_subdata[2]+'">';
			_td.innerHTML += '<input type="hidden" id="expiry'+i+'" value="'+_subdata[11]+'">';
			_td.innerHTML += '<input type="hidden" id="orderid'+i+'" value="'+_subdata[9]+'">';
			_td.innerHTML += '<input type="hidden" id="ordervolume'+i+'" value="'+_subdata[12]+'">';
			// _orderList += '</tr>';
		}
	}

	// document.getElementById("_currentStockPosition").innerHTML = _currentStock;
	// document.getElementById("_orderListTable").innerHTML = _orderList;

	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
	document.getElementById('_refresh').value = 'Refresh';
	document.getElementById('_refresh').disabled = false;
}
function clearOrderField()
{
	try
	{
		document.getElementById('_amendJsxid').innerHTML = '&nbsp;';
		document.getElementById('_amendCommand').innerHTML = '&nbsp;';
		document.getElementById('_amendStock').innerHTML = '&nbsp;';
		document.getElementById('_amendPrice').innerHTML = '&nbsp;';
		document.getElementById('_amendVolume').innerHTML = '&nbsp;';
		document.getElementById('_amendBoard').innerHTML = '&nbsp;';
		document.getElementById('_amendExpiry').innerHTML = '&nbsp;';
		document.getElementById('_amendNewPrice').value = '';
		document.getElementById('_amendNewVolume').value = '';
	}
	catch (err){};
}
function loadPin()
{
	var _t = document.getElementById('_pin').value;
	if (_t&&_t.length>1) _savedPin = _t;
	document.getElementById('_pin').value = '';
}
function handlePinKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		loadOrderCommand();
		return false;
	}
}
