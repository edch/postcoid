var http_request = false;
var _savedPin = '';
function makeRequest(url,callerId)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}
	if (callerId == 'loadAccount')
	{
		http_request.onreadystatechange = loadAccountResult;
	}
	else if(callerId == 'loadOrder')
	{
		http_request.onreadystatechange = loadOrderResult;
	}
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadAccountResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadAccountResult(http_request.responseText);
		}
	}
}
function parseLoadAccountResult(result)
{
	var _account = document.getElementById('_account');
	var acc = result.split('#');
	_account.options.length = 0;
	for (i=0; i<acc.length; ++i)
	{
		if ( acc[i].length <= 0 )
		{
			continue;
		}
		try
		{
			// _account.add ( new Option(acc[i],acc[i]), null );
			_account.options[i] = new Option(acc[i], acc[i], false, false);
		}
		catch (err){}
	}
}
function loadAccount()
{
	// makeRequest('../servlets/AccountServlet?_a=acclist','loadAccount');
	parseLoadAccountResult ( _accountList );
}

function loadOrderCommand()
{
	document.getElementById('_enter').value = 'Load';
	document.getElementById('_enter').disabled = true;
	document.getElementById('_refresh').value = 'Loading..';
	document.getElementById('_refresh').disabled = true;
	loadPin();
	var account =  document.getElementById("_account").value;
	makeRequest('../servlets/AccountServlet?_a=orderlist&_account=' + account + '&_pin=' + _savedPin,'loadOrder');
}
function loadOrderResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			// if ( http_request.responseText.length > 0 )
			{
				parseLoadOrderResult(http_request.responseText);
			}
		}
		else if ( http_request.status == 403 ) //forbidden
		{
			invalidPin();
		}
		else if ( http_request.status == 404 ) //forbidden
		{
			invalidPin();
		}
	}
}
function invalidPin()
{
	document.getElementById('sp_post').innerHTML = 'Wrong PIN';
	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
	document.getElementById('_refresh').value = 'Refresh';
	document.getElementById('_refresh').disabled = false;
	// document.getElementById("_orderListTable").innerHTML = '';
	document.getElementById("_totalBuy").innerHTML = "0";
	document.getElementById("_totalSell").innerHTML = "0";
	document.getElementById("_nett").innerHTML = "0";
	clearTable();
}
function clearTable()
{
	var tbody = document.getElementById("_orderListTable");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
var _currentCash=0;
var _tradingLimit=0;
var _buyFee=0;
var _sellFee=0;
function parseLoadOrderResult(result)
{
	var _data = result.split('|');
	var _subdata = _data[0].split('#');
	var _tBuy=0,_tSell=0;
	_currentCash = parseInt(_subdata[0]);
	_tradingLimit = parseInt(_subdata[1]);
	_buyFee = parseFloat(_subdata[2]);
	_sellFee = parseFloat(_subdata[3]);
	document.getElementById('sp_post').innerHTML = _subdata[4];

	// var _orderList = '';
	var _tr,_td;
	clearTable();
	var _orderList = document.getElementById("_orderListTable");
	for(var i=1;i<_data.length;++i)
	{
		if (_data[i].length<=3) continue;
		_subdata = _data[i].split('#');
		_tr = _orderList.insertRow(_orderList.rows.length);
		if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
		else _tr.setAttribute("class", "stock_running_detail_white");

		// _orderList += '<td><div align="left">'+_subdata[0]+'</div></td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = "<div align='left'>"+_subdata[0]+"</div>";

		// _orderList += '<td>'+_subdata[1]+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = _subdata[1];

		// _orderList += '<td>'+_subdata[2]+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = _subdata[2];

		var p=parseInt(_subdata[5]);
		var r=parseInt(_subdata[6]);
		var t=parseInt(_subdata[7]);
		if (_subdata[3]=='0')
		{
			// _orderList += '<td class="col_red">Buy</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = 'Buy';
			_td.setAttribute('class','col_red');
			// _tBuy+=(p*(t+r)*_LOT_SIZE);
			if (_subdata[8]!='0') _tBuy+=(p*(t+r)*_LOT_SIZE); // not rejected
		}
		else if(_subdata[3]=='1')
		{
			// _orderList += '<td class="col_green">Sell</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = 'Sell';
			_td.setAttribute('class','col_green');
			// _tSell+=(p*(t+r)*_LOT_SIZE);
			if (_subdata[8]!='0') _tSell+=(p*(t+r)*_LOT_SIZE); // not rejected
		}
		else if (_subdata[3]=='2')
		{
			// _orderList += '<td class="col_red">Buy</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = 'Buy';
			_td.setAttribute('class','col_red');
			// _tBuy+=(p*(t+r)*_LOT_SIZE);
			if (_subdata[8]!='0') _tBuy+=(p*(t+r)*_LOT_SIZE); // not rejected
		}
		else if(_subdata[3]=='3')
		{
			// _orderList += '<td class="col_green">Sell</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = 'Sell';
			_td.setAttribute('class','col_green');
			// _tSell+=(p*(t+r)*_LOT_SIZE);
			if (_subdata[8]!='0') _tSell+=(p*(t+r)*_LOT_SIZE); // not rejected
		}

		// _orderList += '<td>'+_subdata[4]+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = '<div align="left">' + _subdata[4] + '</div>';

		// _orderList += '<td>'+p.formatMoney(0)+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = p.formatMoney(0);

		// _orderList += '<td>'+r.formatMoney(0)+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = r.formatMoney(0);

		// _orderList += '<td>'+t.formatMoney(0)+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = t.formatMoney(0);

		// _orderList += '<td class="'+orderStatusColor(_subdata[8])+'">'+orderStatusString(_subdata[8])+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = orderStatusString(_subdata[8]);
		_td.setAttribute('class', orderStatusColor(_subdata[8]));

		// _orderList += '<td>'+(p*(t+r)*_LOT_SIZE).formatMoney(0)+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = (p*(t+r)*_LOT_SIZE).formatMoney(0);

		// _orderList += '<td>'+(p*t*_LOT_SIZE).formatMoney(0)+'</td>';
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = (p*t*_LOT_SIZE).formatMoney(0);

		// _orderList += '</tr>';
	}

	// document.getElementById("_orderListTable").innerHTML = _orderList;
	document.getElementById("_totalBuy").innerHTML = _tBuy.formatMoney(0);
	document.getElementById("_totalSell").innerHTML = _tSell.formatMoney(0);
	if (_tBuy>_tSell)
	{
		document.getElementById("_nett").innerHTML = '-' + (_tBuy-_tSell).formatMoney(0);
		document.getElementById("_nett").className = "col_red";
	}
	else
	{
		document.getElementById("_nett").innerHTML = (_tSell-_tBuy).formatMoney(0);
		document.getElementById("_nett").className = "col_green";
	}

	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
	document.getElementById('_refresh').value = 'Refresh';
	document.getElementById('_refresh').disabled = false;
}
function orderStatusString(orderStatus)
{
	if(orderStatus=='0') return 'Rejected';
	else if(orderStatus=='1') return 'Open';
	else if(orderStatus=='2') return 'Matched';
	else if(orderStatus=='3') return 'Amending';
	else if(orderStatus=='4') return 'Withdrawing';
	else if(orderStatus=='5') return 'Canceled';
}
function orderStatusColor(orderStatus)
{
	if(orderStatus=='0') return 'col_grey';
	else if(orderStatus=='1') return 'col_black';
	else if(orderStatus=='2') return 'col_green';
	else if(orderStatus=='3') return 'col_purple';
	else if(orderStatus=='4') return 'col_purple';
	else if(orderStatus=='5') return 'col_red';
}
function loadPin()
{
	var _t = document.getElementById('_pin').value;
	if (_t&&_t.length>1) _savedPin = _t;
	document.getElementById('_pin').value = '';
}
function handlePinKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		loadOrderCommand();
		return false;
	}
}
