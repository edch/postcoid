var http_request = false;
var _savedPin = '';
function makeRequest(url,callerId)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	if (callerId == 'loadAccount')
	{
		http_request.onreadystatechange = loadAccountResult;
	}
	else if(callerId == 'loadOrder')
	{
		http_request.onreadystatechange = loadOrderResult;
	}
	else if(callerId == 'loadLimit')
	{
		http_request.onreadystatechange = loadLimitResult;
	}
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadAccountResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadAccountResult(http_request.responseText);
		}
	}
}
function parseLoadAccountResult(result)
{
	var _account = document.getElementById('_account');
	var acc = result.split('#');
	_account.options.length = 0;
	for (i=0; i<acc.length; ++i)
	{
		if ( acc[i].length <= 0 )
		{
			continue;
		}
		try
		{
			// _account.add ( new Option(acc[i],acc[i]), null );
			_account.options[i] = new Option(acc[i], acc[i], false, false);
		}
		catch (err){}
	}
}
function loadAccount()
{
	// makeRequest('../servlets/AccountServlet?_a=acclist','loadAccount');
	parseLoadAccountResult ( _accountList );
}
function doExecuteBuy()
{
	var _stock = document.getElementById('_buyStockCode').value.toUpperCase();
	var _price = parseInt ( document.getElementById('_buyPrice').value );
	var _lot = parseInt ( document.getElementById('_buyLot').value );
	if ( _allStocksCode.indexOf ( ':' + _stock + ':' ) < 0 )
	{
		alert ('Invalid Stock Code.');
		return;
	}
	if ( _price <= 0 )
	{
		alert ('Invalid Price.');
		return;
	}
	if ( _lot <= 0 )
	{
		alert ('Invalid Volume.');
		return;
	}

	var _question = "BUY " + _stock + " " + _lot + " Lot[s] @" + _price + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		document.getElementById('_buyExecute').value = "Please Wait ...";
		document.getElementById('_buyExecute').disabled = true
		loadPin();

		var _url = "pin=" + encodeURI( _savedPin )
		_url += "&stock=" + encodeURI( _stock )
		_url += "&price=" + encodeURI( _price )
		_url += "&volume=" + encodeURI( _lot )
		_url += "&account=" + encodeURI( document.getElementById("_account").value )
		_url += "&board=" + encodeURI( document.getElementById("_buyBoard").value )
		_url += "&expiry=" + encodeURI( document.getElementById("_buyExpiry").value )
		_url += "&action=B"
		makePOSTRequest('../servlets/OrderServlet', _url);
	}
}
function doExecuteSell()
{
	var _stock = document.getElementById('_sellStockCode').value.toUpperCase();
	var _price = parseInt ( document.getElementById('_sellPrice').value );
	var _lot = parseInt ( document.getElementById('_sellLot').value );
	if ( _allStocksCode.indexOf ( ':' + _stock + ':' ) < 0 )
	{
		alert ('Invalid Stock Code.');
		return;
	}
	if ( _price <= 0 )
	{
		alert ('Invalid Price.');
		return;
	}
	if ( _lot <= 0 )
	{
		alert ('Invalid Volume.');
		return;
	}

	var _question = "SELL " + _stock.toUpperCase() + " " + _lot + " Lot[s] @" + _price + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		document.getElementById('_sellExecute').value = "Please Wait ...";
		document.getElementById('_sellExecute').disabled = true
		loadPin();

		var _url = "pin=" + encodeURI( _savedPin )
		_url += "&stock=" + encodeURI( _stock )
		_url += "&price=" + encodeURI( _price )
		_url += "&volume=" + encodeURI( _lot )
		_url += "&account=" + encodeURI( document.getElementById("_account").value )
		_url += "&board=" + encodeURI( document.getElementById("_sellBoard").value )
		_url += "&expiry=" + encodeURI( document.getElementById("_sellExpiry").value )
		_url += "&action=S"
		makePOSTRequest('../servlets/OrderServlet', _url);
	}
}
var http_post = false;
function makePOSTRequest(url, parameters)
{
	http_post = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_post = new XMLHttpRequest();
		if (http_post.overrideMimeType)
		{
			http_post.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_post = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_post = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_post)
	{
		return false;
	}

	http_post.onreadystatechange = orderResult;
	http_post.open('POST', url, true);
	http_post.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_post.setRequestHeader("Content-length", parameters.length);
	http_post.setRequestHeader("Connection", "close");
	http_post.send(parameters);
}
function orderResult()
{
	if (http_post.readyState == 4)
	{
		if (http_post.status == 200)
		{
			result = http_post.responseText;
			try
			{
				clearOrderField();
				if(document.getElementById('_buyExecute').disabled==true)
				{
					document.getElementById('_buyStockCode').focus();
				}
				else if(document.getElementById('_sellExecute').disabled==true)
				{
					document.getElementById('_sellStockCode').focus();
				}

				document.getElementById('_buyExecute').value = "Execute Buy";
				document.getElementById('_buyExecute').disabled = false;
				document.getElementById('_sellExecute').value = "Execute Sell";
				document.getElementById('_sellExecute').disabled = false;
			}
			catch (err) {};
			alert ( result );
		}
	}
}
function clearOrderField()
{
	document.getElementById('_buyStockCode').value='';
	document.getElementById('_buyPrice').value='';
	document.getElementById('_buyLot').value='';
	document.getElementById('_sellStockCode').value='';
	document.getElementById('_sellPrice').value='';
	document.getElementById('_sellLot').value='';
}
function loadOrderCommand()
{
	document.getElementById('_enter').value = 'Load';
	document.getElementById('_enter').disabled = true;
	document.getElementById('_refresh').value = 'Loading..';
	document.getElementById('_refresh').disabled = true;
	loadPin();
	var account =  document.getElementById("_account").value;
	makeRequest('../servlets/AccountServlet?_a=orderlistc&_account=' + account + '&_pin=' + _savedPin, 'loadOrder');
}
function loadOrderResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadOrderResult(http_request.responseText);
		}
		else if ( http_request.status == 403 )
		{
			invalidPin();
		}
		else if ( http_request.status == 404 )
		{
			invalidPin();
		}
	}
}
function invalidPin()
{
	document.getElementById('sp_post').innerHTML = 'Wrong PIN';
	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
	document.getElementById('_refresh').value = 'Refresh';
	document.getElementById('_refresh').disabled = false;
	// document.getElementById("_orderListTable").innerHTML = '';
	// document.getElementById("_stockPosition").innerHTML = '';
	document.getElementById("_totalBuy").innerHTML = "0";
	document.getElementById("_totalSell").innerHTML = "0";
	document.getElementById("_nett").innerHTML = "0";
	clearTable();
}
function clearTable()
{
	var tbody = document.getElementById("_orderListTable");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}

	var tbody = document.getElementById("_stockPosition");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
var _currentCash=0;
var _tradingLimit=0;
var _buyFee=0.0020;
var _sellFee=0.0030;
function parseLoadOrderResult(result)
{
	var _data = result.split('|');
	var _subdata = _data[0].split('#');
	var _tBuy=0,_tSell=0;
	_currentCash = parseInt(_subdata[0]);
	_tradingLimit = parseInt(_subdata[1]);
	_buyFee = parseFloat(_subdata[2]);
	_sellFee = parseFloat(_subdata[3]);
	document.getElementById('sp_post').innerHTML = _subdata[4];
	document.getElementById('_rtl').innerHTML = _tradingLimit.formatMoney(0) + '&nbsp;&nbsp;';
	if ( _currentCash >= 0)
	{
		document.getElementById('_dueBalance').innerHTML = _currentCash.formatMoney(0) + '&nbsp;&nbsp;';
		document.getElementById('_dueBalance').className = "col_green";
	}
	else
	{
		// document.getElementById('_dueBalance').innerHTML = '<span class="col_red">-' + Math.abs(_currentCash).formatMoney(0) + '</span>&nbsp;&nbsp;';
		document.getElementById('_dueBalance').innerHTML = '-' + Math.abs(_currentCash).formatMoney(0) + '&nbsp;&nbsp;';
		document.getElementById('_dueBalance').className = "col_red";
	}

	clearTable();
	var _orderList = document.getElementById("_orderListTable");
	var _stockPosition = document.getElementById("_stockPosition");
	var _tr,_td;
	for(var i=1;i<_data.length;++i)
	{
		if (_data[i].length<=3) continue;
		_subdata = _data[i].split('#');

		if (_subdata[0] == 'S')
		{
			_tr = _stockPosition.insertRow(_stockPosition.rows.length);
			// _stockPosition += '<tr>';
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			// _stockPosition += '<td align="left">'+_subdata[1]+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = "<div align='left'>"+_subdata[1]+"</div>";
			var _avg=parseInt(_subdata[2]);
			var _vol=parseInt(_subdata[3]);
			var _volRemain=parseInt(_subdata[4]);

			// _stockPosition += '<td>'+_avg.formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = _avg.formatMoney(0);

			// _stockPosition += '<td>'+_vol.formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = _vol.formatMoney(0);

			// _stockPosition += '<td>'+(_avg*_vol*_LOT_SIZE).formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = (_avg*_vol*_LOT_SIZE).formatMoney(0);

			// _stockPosition += '<td>'+_volRemain.formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = _volRemain.formatMoney(0);

			// _stockPosition += '<td></td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = '&nbsp;';
		}
		else
		{
			_tr = _orderList.insertRow(_orderList.rows.length);
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			// _orderList += '<tr>';
			// _orderList += '<td><div align="left">'+_subdata[0]+'</div></td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = "<div align='left'>"+_subdata[0]+"</div>";

			// _orderList += '<td>'+_subdata[1]+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = "<div align='left'>"+_subdata[1]+"</div>";

			// _orderList += '<td>'+_subdata[2]+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = "<div align='left'>"+_subdata[2]+"</div>";

			var p=parseInt(_subdata[5]);
			var r=parseInt(_subdata[6]);
			var t=parseInt(_subdata[7]);
			if (_subdata[3]=='0')
			{
				// _orderList += '<td class="col_red">Buy</td>';
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = 'Buy';
				_td.setAttribute('class','col_red');
				if (_subdata[8]!='0') _tBuy+=(p*(t+r)*_LOT_SIZE); // not rejected
			}
			else if(_subdata[3]=='1')
			{
				// _orderList += '<td class="col_green">Sell</td>';
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = 'Sell';
				_td.setAttribute('class','col_green');
				if (_subdata[8]!='0') _tSell+=(p*(t+r)*_LOT_SIZE); // not rejected
			}
			else if (_subdata[3]=='2')
			{
				// _orderList += '<td class="col_red">Buy</td>';
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = 'Buy';
				_td.setAttribute('class','col_red');
				if (_subdata[8]!='0') _tBuy+=(p*(t+r)*_LOT_SIZE); // not rejected
			}
			else if(_subdata[3]=='3')
			{
				// _orderList += '<td class="col_green">Sell</td>';
				_td = _tr.insertCell(_tr.cells.length);
				_td.innerHTML = 'Sell';
				_td.setAttribute('class','col_green');
				if (_subdata[8]!='0') _tSell+=(p*(t+r)*_LOT_SIZE); // not rejected
			}

			//_orderList += '<td>'+_subdata[4]+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			// _td.innerHTML = '<div align="left">' + _subdata[4] + '</div>';
			_td.setAttribute('align','left');
			 _td.innerHTML = _subdata[4];

			//_orderList += '<td>'+p.formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = p.formatMoney(0);

			//_orderList += '<td>'+r.formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = r.formatMoney(0);

			//_orderList += '<td>'+t.formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = t.formatMoney(0);

			//_orderList += '<td class="'+orderStatusColor(_subdata[8])+'">'+orderStatusString(_subdata[8])+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = orderStatusString(_subdata[8]);
			_td.setAttribute('class',orderStatusColor(_subdata[8]));

			//_orderList += '<td>'+(p*(t+r)*_LOT_SIZE).formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = (p*(t+r)*_LOT_SIZE).formatMoney(0);

			//_orderList += '<td>'+(p*t*_LOT_SIZE).formatMoney(0)+'</td>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = (p*t*_LOT_SIZE).formatMoney(0);

			// _orderList += '<td>';
			_td = _tr.insertCell(_tr.cells.length);
			if (_subdata[8]=='1')
			{
				var _temp = '';
				_temp += '<a href="#" ';
				_temp += 'onclick="confirmWithdraw(\''+_subdata[4]+'\',\''+_subdata[5]+'\',\''+_subdata[6]+'\',\''+_subdata[2]+'\',\''+_subdata[11]+'\',\''+_subdata[1]+'\',\''+_subdata[9]+'\');" ';
				_temp += 'style="text-decoration: none;"><img src="../images/btn_w.png" width="15" height="12" align="left"></a>';

				_temp += '<a href="#" ';
				// _temp += 'onclick="confirmAmend (\''+_subdata[4]+'\',\''+_subdata[5]+'\',\''+_subdata[6]+'\',\''+_subdata[2]+'\',\''+_subdata[11]+'\',\''+_subdata[1]+'\',\''+_subdata[9]+'\');" ';
				_temp += 'onclick="confirmAmend (\''+_subdata[4]+'\',\''+_subdata[5]+'\',\''+_subdata[12]+'\',\''+_subdata[2]+'\',\''+_subdata[11]+'\',\''+_subdata[1]+'\',\''+_subdata[9]+'\');" ';
				_temp += 'style="text-decoration: none;"><img src="../images/btn_d.png" width="15" height="12" align="left"></a>'
				_td.innerHTML = _temp;
			}

			// _orderList += '</td>'
			// _orderList += '</tr>';
		}
	}

	// document.getElementById("_orderListTable").innerHTML = _orderList;
	// document.getElementById("_stockPosition").innerHTML = _stockPosition;
	document.getElementById("_totalBuy").innerHTML = _tBuy.formatMoney(0);
	document.getElementById("_totalSell").innerHTML = _tSell.formatMoney(0);
	if (_tBuy>_tSell)
	{
		document.getElementById("_nett").innerHTML = '-' + (_tBuy-_tSell).formatMoney(0);
		document.getElementById("_nett").className = "col_red";
	}
	else
	{
		document.getElementById("_nett").innerHTML = (_tSell-_tBuy).formatMoney(0);
		document.getElementById("_nett").className = "col_green";
	}

	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
	document.getElementById('_refresh').value = 'Refresh';
	document.getElementById('_refresh').disabled = false;
}
function orderStatusString(orderStatus)
{
	if(orderStatus=='0') return 'Rejected';
	else if(orderStatus=='1') return 'Open';
	else if(orderStatus=='2') return 'Matched';
	else if(orderStatus=='3') return 'Amending';
	else if(orderStatus=='4') return 'Withdrawing';
	else if(orderStatus=='5') return 'Canceled';
}
function orderStatusColor(orderStatus)
{
	if(orderStatus=='0') return 'col_grey';
	else if(orderStatus=='1') return 'col_black';
	else if(orderStatus=='2') return 'col_green';
	else if(orderStatus=='3') return 'col_purple';
	else if(orderStatus=='4') return 'col_purple';
	else if(orderStatus=='5') return 'col_red';
}
function buyChanged()
{
	var _price = parseFloat(document.getElementById('_buyPrice').value);
	var _lot = parseFloat(document.getElementById('_buyLot').value);
	document.getElementById('_buyStockValue').innerHTML = (_price*_lot*_LOT_SIZE).formatMoney(0);
	document.getElementById('_buyTransFee').innerHTML = (_price*_lot*_LOT_SIZE*_buyFee).formatMoney(0);
	document.getElementById('_buyPayable').innerHTML = ((_price*_lot*_LOT_SIZE)+(_price*_lot*_LOT_SIZE*_buyFee)).formatMoney(0);

	// var tmp = (_currentCash/(_price*_LOT_SIZE));
	if ( _currentCash >= 0 )
	{
		document.getElementById('_cashLots').innerHTML = (_currentCash/(_price*_LOT_SIZE)).formatMoney(0);
		document.getElementById('_cashShares').innerHTML = (_currentCash/_price).formatMoney(0);
	}
	else
	{
		document.getElementById('_cashLots').innerHTML = '0';
		document.getElementById('_cashShares').innerHTML = '0';
	}

	if ( _tradingLimit >= 0 )
	{
		var maxLot = Math.floor ( _tradingLimit / ( ( _price * _LOT_SIZE ) + ( _price * _LOT_SIZE * _buyFee ) ) );
		var maxShares = Math.floor ( _tradingLimit / ( _price  + ( _price * _buyFee ) ) );

		//document.getElementById('_creditLots').innerHTML = Math.floor(_tradingLimit/(_price*_LOT_SIZE)).formatMoney(0);
		//document.getElementById('_creditShares').innerHTML = Math.floor(_tradingLimit/_price).formatMoney(0);
		document.getElementById('_creditLots').innerHTML = Math.floor ( maxLot  ).formatMoney(0);
		document.getElementById('_creditShares').innerHTML = Math.floor ( maxShares ).formatMoney(0);
	}
	else
	{
		document.getElementById('_creditLots').innerHTML = '0';
		document.getElementById('_creditShares').innerHTML = '0';
	}
}
function sellChanged()
{
	var _price = parseFloat(document.getElementById('_sellPrice').value);
	var _lot = parseFloat(document.getElementById('_sellLot').value);
	document.getElementById('_sellStockValue').innerHTML = (_price*_lot*_LOT_SIZE).formatMoney(0);
	document.getElementById('_sellTransFee').innerHTML = (_price*_lot*_LOT_SIZE*_sellFee).formatMoney(0);
	document.getElementById('_sellReceivable').innerHTML = ((_price*_lot*_LOT_SIZE)-(_price*_lot*_LOT_SIZE*_sellFee)).formatMoney(0);
}
function loadPin()
{
	var _t = document.getElementById('_pin').value;
	if (_t&&_t.length>1) _savedPin = _t;
	document.getElementById('_pin').value = '';
}
function confirmWithdraw(_stock, _price, _lot, _board, _expiry, _jsxid, _orderid)
{
	var _question = "WITHDRAW " + _stock + " " + _lot + " Lot[s] @" + _price + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		loadPin();
		var _account = document.getElementById("_account").value;

		var _url = "pin=" + encodeURI( _savedPin )
		_url += "&stock=" + _stock
		_url += "&price=" + _price
		_url += "&volume=" + _lot
		_url += "&account=" + encodeURI( _account )
		_url += "&board=" + encodeURI( _board )
		_url += "&expiry=" + encodeURI( _expiry )
		_url += "&jsxid=" + encodeURI( _jsxid )
		_url += "&orderid=" + encodeURI( _orderid )
		_url += "&action=W"
		makePOSTRequest('../servlets/OrderServlet', _url);
	}
}
var generator = null;
function confirmAmend(_stock, _price, _lot, _board, _expiry, _jsxid, _orderid)
{
	generator=window.open('',null,'height=200,width=250');
	setTimeout ( "writeAmend('" + _stock + "','" + _price + "','" + _lot + "','" + _board + "','" + _expiry + "','" + _jsxid + "','" + _orderid + "')", 1000 );
}
function writeAmend(_stock, _price, _lot, _board, _expiry, _jsxid, _orderid)
{
	generator.document.write('<html><head><title>Amend Order</title>');
	generator.document.write('<' + 'script type="text/javascript" src="./order_command.js"' + '><' + '/' + 'script>');
	generator.document.write('<link rel="stylesheet" href="../style/main.css">');
	generator.document.write('</head><body>');

	var tableData = '<div align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="color:#FFFFFF;font-weight:bold">';
	tableData += '<tr>';
	tableData += '<td class="col_black">Stock</td>';
	tableData += '<td>: <input type="text" name="_stock" id="_stock" maxlength="8" class="inp_code2" value="' + _stock + '" disabled/></td>';
	tableData += '</tr>';
	tableData += '<tr>';
	tableData += '<td class="col_black">Price</td>';
	tableData += '<td>: <input type="text" name="_price" id="_price" maxlength="8" class="inp_code2" value="' + _price + '"/></td>';
	tableData += '</tr>';
	tableData += '<tr>';
	tableData += '<td class="col_black">Vol</td>';
	tableData += '<td>: <input type="text" name="_volume" id="_volume" maxlength="6" class="inp_code2" value="' + _lot + '"/></td>';
	tableData += '</tr>';
	tableData += '<tr>';
	tableData += '<td height="5" colspan="2">&nbsp;</td>';
	tableData += '</tr>';
	tableData += '<tr>';
	tableData += '<td colspan="2"><label>';
	tableData += '<div align="center">';
	tableData += '<input class="btnQuick" id="_executeAmend" type="button" value="Execute Amend" onclick="executeAmend(';
	tableData += 'document.getElementById(\'_stock\').value, document.getElementById(\'_price\').value, ';
	tableData += 'document.getElementById(\'_volume\').value, document.getElementById(\'_account\').value, ';
	tableData += 'document.getElementById(\'_pin\').value, document.getElementById(\'_board\').value, ';
	tableData += 'document.getElementById(\'_expiry\').value, document.getElementById(\'_jsxid\').value, ';
	tableData += 'document.getElementById(\'_orderid\').value ';
	tableData += ');">';
	tableData += '</div>';
	tableData += '</label>';
	tableData += '</td>';
	tableData += '</tr>';
	tableData += '<tr>';
	tableData += '<td height="5" colspan="2" align="center">';
	tableData += '<p><a href="javascript:self.close()">Close</a>.</p>';
	tableData += '</td>';
	tableData += '</tr>';
	tableData += '</table></div>';
	tableData += '<input type="hidden" name="_orderid" id="_orderid" value="' + _orderid + '"/></td>';
	tableData += '<input type="hidden" name="_jsxid" id="_jsxid" value="' + _jsxid + '"/></td>';
	loadPin();
	var _account = document.getElementById("_account").value;
	tableData += '<input type="hidden" name="_account" id="_account" value="' + _account+ '"/></td>';
	tableData += '<input type="hidden" name="_pin" id="_pin" value="' + _savedPin + '"/></td>';
	tableData += '<input type="hidden" name="_board" id="_board" value="' + _board + '"/></td>';
	tableData += '<input type="hidden" name="_expiry" id="_expiry" value="' + _expiry + '"/></td>';

	generator.document.write( tableData );
	generator.document.write('</body></html>');
	generator.document.close();
}
function executeAmend (_stock, _price, _lot, _account, _pin, _board, _expiry, _jsxid, _orderid)
{
	if ( _price <= 0 )
	{
		alert ('Invalid Price.');
		return;
	}
	if ( _lot <= 0 )
	{
		alert ('Invalid Volume.');
		return;
	}

	try
	{
		document.getElementById('_executeAmend').value = 'Sending Order ...';
		document.getElementById('_executeAmend').disabled = true;
		document.getElementById('_price').disabled = true;
		document.getElementById('_volume').disabled = true;
	}
	catch (err) {}

	var _question = "AMEND " + _stock + " " + _lot + " Lot[s] @" + _price + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		loadPin();

		var _url = "pin=" + encodeURI( _pin )
		_url += "&stock=" + _stock
		_url += "&price=" + _price
		_url += "&volume=" + _lot
		_url += "&account=" + encodeURI( _account )
		_url += "&board=" + encodeURI( _board )
		_url += "&expiry=" + encodeURI( _expiry )
		_url += "&jsxid=" + encodeURI( _jsxid )
		_url += "&orderid=" + encodeURI( _orderid )
		_url += "&action=A"
		makePOSTRequest('../servlets/OrderServlet', _url);
	}
}
function handlePinKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		loadOrderCommand();
		return false;
	}
}
function checkStockCodeOnBlur (obj,action)
{
	try
	{
		if (action=='b')
		{
			var _stock = document.getElementById('_buyStockCode').value.toUpperCase();
			if ( _stock.substring ( _stock.length - 2 ) == '-R' )
			{
				document.getElementById('_buyBoard').selectedIndex = 1;
				document.getElementById('_buyExpiry').selectedIndex = 1;
			}

			var _account = document.getElementById('_account').value;
			var _pin = document.getElementById('_pin').value;
			getInformationWithStock ( _account, _pin, _stock );
		}
		else if (action=='s')
		{
			var _stock = document.getElementById('_sellStockCode').value.toUpperCase();
			if ( _stock.substring ( _stock.length - 2 ) == '-R' )
			{
				document.getElementById('_sellBoard').selectedIndex = 1;
				document.getElementById('_sellExpiry').selectedIndex = 1;
			}
		}
	}
	catch (err) {}
}
function loadLimitResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadLimitResult(http_request.responseText);
		}
	}
}
function parseLoadLimitResult(result)
{
	var _stockPosition = result.split('|');
	var _info = _stockPosition[0].split('#');
	_tradingLimit = parseFloat(_info[0]);
	document.getElementById('_rtl').innerHTML = _tradingLimit.formatMoney(0);
}
function getInformationWithStock(_acc,_pin,_stock)
{
	// makeRequest ('../servlets/AccountServlet?_a=quickorder&_pin='+_pin+'&_account='+_acc+'&_stock='+_stock, 'loadLimit');
	makeRequest ('../servlets/AccountServlet?_a=quickorder&_pin='+_savedPin+'&_account='+_acc+'&_stock='+_stock, 'loadLimit');
}
