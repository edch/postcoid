function priceOrVolumeBuyChanged ()
{
	var _price = 0;
	var _volume = 0;

	try
	{
		_price = parseInt ( document.getElementById ("orderForm:inputPrice").value );
		_volume = parseInt ( document.getElementById ("orderForm:inputVolume").value );
	}
	catch (err){}

	document.getElementById("stockValue").innerHTML = (_price * _volume * _LOT_SIZE).formatMoney(0);
	document.getElementById("transactionFee").innerHTML = ((_price * _volume * 5) * 0.20).formatMoney(0);
	document.getElementById("payable").innerHTML = ((_price * _volume * _LOT_SIZE) + (_price * _volume * 5 * 0.20)).formatMoney(0);

	var _remainTradingLimit = 0;
	var _dueDateBalance = 0;
	try
	{
		_remainTradingLimit = parseInt ( document.getElementById ("orderForm:remainTradingLimit").value );
		_dueDateBalance = parseInt ( document.getElementById ("orderForm:dueDateBalance").value );
	}
	catch (err) {}

	if ( _remainTradingLimit >= 0 )
	{
		document.getElementById("creditOrderInLots").innerHTML = (_remainTradingLimit/(_price*_LOT_SIZE)).formatMoney(0);
		document.getElementById("creditOrderInShares").innerHTML = (_remainTradingLimit/(_price)).formatMoney(0);
	}
	else
	{
		document.getElementById("creditOrderInLots").innerHTML = 0;
		document.getElementById("creditOrderInShares").innerHTML = 0;
	}
	
	if ( _dueDateBalance >= 0 )
	{
		document.getElementById("cashOrderInLots").innerHTML = (_dueDateBalance/(_price*_LOT_SIZE)).formatMoney(0);
		document.getElementById("cashOrderInShares").innerHTML = (_dueDateBalance/(_price)).formatMoney(0);
	}
	else
	{
		document.getElementById("cashOrderInLots").innerHTML = 0;
		document.getElementById("cashOrderInShares").innerHTML = 0;
	}
}
function priceOrVolumeSellChanged ()
{
	var _price = 0;
	var _volume = 0;

	try
	{
		_price = parseInt ( document.getElementById ("orderForm:inputPrice").value );
		_volume = parseInt ( document.getElementById ("orderForm:inputVolume").value );
	}
	catch (err){}

	document.getElementById("stockValue").innerHTML = (_price * _volume * _LOT_SIZE).formatMoney(0);
	document.getElementById("transactionFee").innerHTML = ((_price * _volume * 5) * 0.30).formatMoney(0);
	document.getElementById("payable").innerHTML = ((_price * _volume * _LOT_SIZE) - (_price * _volume * 5 * 0.30)).formatMoney(0);

}
function priceOrVolumeAmendChanged ()
{
	var _price = 0;
	var _volume = 0;

	try
	{
		_price = parseInt ( document.getElementById ("orderForm:inputPrice").value );
		_volume = parseInt ( document.getElementById ("orderForm:inputVolume").value );
	}
	catch (err){}

	document.getElementById("stockValue").innerHTML = (_price * _volume * _LOT_SIZE).formatMoney(0);
	document.getElementById("transactionFee").innerHTML = ((_price * _volume * 5) * 0.30).formatMoney(0);
	document.getElementById("payable").innerHTML = ((_price * _volume * _LOT_SIZE) + (_price * _volume * 5 * 0.20)).formatMoney(0);

}
function askBuyConfirmation()
{
	var _price = document.getElementById ("orderForm:inputPrice").value;
	var _volume = document.getElementById ("orderForm:inputVolume").value;
	var _stockCode = document.getElementById ("orderForm:inputStockCode").value;
	var _question = "BUY " + _stockCode.toUpperCase() + " " + _volume + " Lot[s] @" + _price + " ?";
	var _confirm = confirm ( _question );
	// var _confirm = prompt ( _question, "" );
	// if ( _confirm.length > 0 )
	if ( _confirm )
	{
		// document.getElementById ("pinForm:_inputPin").value = _confirm;
		document.getElementById ("orderForm:inputSubmit").style.visibility = 'hidden';
	}

	return _confirm;
	// return true;
}
function askSellConfirmation()
{
	var _price = document.getElementById ("orderForm:inputPrice").value;
	var _volume = document.getElementById ("orderForm:inputVolume").value;
	var _stockCode = document.getElementById ("orderForm:inputStockCode").value;
	var _question = "SELL " + _stockCode.toUpperCase() + " " + _volume + " Lot[s] @" + _price + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		document.getElementById ("orderForm:inputSubmit").style.visibility = 'hidden';
	}
	return _confirm;
}
function askWithdrawConfirmation()
{
	var _price = document.getElementById ("orderForm:inputPrice").value;
	var _volume = document.getElementById ("orderForm:inputVolume").value;
	var _stockCode = document.getElementById ("orderForm:inputStockCode").value;
	var _question = "WITHDRAW " + _stockCode.toUpperCase() + " " + _volume + " Lot[s] @" + _price + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		document.getElementById ("orderForm:inputSubmit").style.visibility = 'hidden';
	}
	return _confirm;
}
function askAmendConfirmation()
{
	var _price = document.getElementById ("orderForm:inputPrice").value;
	var _volume = document.getElementById ("orderForm:inputVolume").value;
	var _newPrice = document.getElementById ("orderForm:inputNewPrice").value;
	var _newVolume = document.getElementById ("orderForm:inputNewVolume").value;
	var _stockCode = document.getElementById ("orderForm:inputStockCode").value;
	var _question = "AMEND " + _stockCode.toUpperCase() + " " + _volume + " Lot[s] @" + _price + " to " + _newVolume + " Lot[s] @" + _newPrice + " ?";
	var _confirm = confirm ( _question );
	if ( _confirm )
	{
		document.getElementById ("orderForm:inputSubmit").style.visibility = 'hidden';
	}
	return _confirm;
}
