var http_request = false;
function makeRequest(url)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	http_request.onreadystatechange = loadNewsResult;
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadNewsResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadNewsResult(http_request.responseText);
		}
	}
}
function parseLoadNewsResult(result)
{
	var _data = result.split('|');
	var _antaraNews = '',_idxNews='';
	var _rightsNews = document.getElementById("_rightsNews");
	var _warrantNews = document.getElementById("_warrantNews");
	var _tr,_td;
	for (i=0; i<_data.length; ++i)
	{
		if ( _data[i].length <= 3 ) continue;
		var _subdata = _data[i].split('#');

		if (_subdata[0]=='A') //antara
		{
			if (i%2 == 0)
			{
				_antaraNews += '<dl class="stock_running_detail_grey">';
			}
			else
			{
				_antaraNews += '<dl class="stock_running_detail_white">';
			}
			// _antaraNews += '<dt><a href="#" onclick="javascript:window.open(\''+_subdata[3]+'\',\'IDX StockList\',\'width=335,height=300,scrollbars=yes\');" style="text-decoration: none;"><strong>'+_subdata[1]+'</strong></a></dt>';
			_antaraNews += '<dt><a href="#" onclick="window.open(\''+_subdata[3]+'\',null,\'width=335,height=300,scrollbars=yes\');" style="text-decoration: none;"><strong>'+_subdata[1]+'</strong></a></dt>';
			_antaraNews += '<dd>'+_subdata[2]+'</dt>';
			_antaraNews += '</dl>';
		}
		else if (_subdata[0]=='I') //idx
		{
			if (i%2 == 0)
			{
				_idxNews += '<dl class="stock_running_detail_grey">';
			}
			else
			{
				_idxNews += '<dl class="stock_running_detail_white">';
			}
			_idxNews += '<dd><a href="#" onclick="window.open(\''+_subdata[3]+'\',null,\'width=335,height=300,scrollbars=yes\');" style="text-decoration: none;"><strong>'+_subdata[2]+'</strong></a></dt>';
			_idxNews += '</dl>';
		}
		else if (_subdata[0]=='R') //rights
		{
			_tr = _rightsNews.insertRow(_rightsNews.rows.length);
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "10");
			_td.innerHTML = "&nbsp;";

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "114");
			_td.innerHTML = _subdata[1];

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "4");
			_td.innerHTML = "&nbsp;";

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "142");
			_td.innerHTML = _subdata[2];

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "81");
			_td.innerHTML = _subdata[3];

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "94");
			_td.innerHTML = _subdata[4];

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "22");
			_td.innerHTML = "&nbsp;";
		}
		else if (_subdata[0]=='W') //warrant
		{
			_tr = _warrantNews.insertRow(_warrantNews.rows.length);
			if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
			else _tr.setAttribute("class", "stock_running_detail_white");

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "10");
			_td.innerHTML = "&nbsp;";

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "114");
			_td.innerHTML = _subdata[1];

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "4");
			_td.innerHTML = "&nbsp;";

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "142");
			_td.innerHTML = _subdata[2];

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "81");
			_td.innerHTML = _subdata[3];

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "94");
			_td.innerHTML = _subdata[4];

			_td = _tr.insertCell(_tr.cells.length);
			_td.setAttribute("width", "22");
			_td.innerHTML = "&nbsp;";
		}
	}

	document.getElementById('_antaraNews').innerHTML = _antaraNews;
	document.getElementById('_idxNews').innerHTML = _idxNews;
}
function loadNews()
{
	makeRequest('../servlets/AccountServlet?_a=newslist');
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};

