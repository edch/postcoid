<%
	String userName = null;
	try
	{
		userName = request.getRemoteUser().toUpperCase();
	}
	catch (Exception e) {}

	if ( userName == null || userName.length() < 1 )
	{
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- <META HTTP-EQUIV="Refresh" CONTENT="0;URL=/onlineTrading/login.jsp"> -->
<meta http-equiv="Refresh" CONTENT="0;URL='http://www.post-pro.co.id'">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<meta name="Description" content="post.co.id merupakan web online trading tercepat dan teraman di indonesia.">
<meta name="Keywords" content="post.co.id, Panin, Panin Sekuritas, Sekuritas, POST, panin sekuritas online trading, online trading, online, trading, transaksi bursa, bursa efek indonesia, sekuritas, securities, anggota bursa, pasar modal, pasar saham, reksadana, secure online trading, peter, peter wijaya, piwi, piwisaint, online trading developer, web trading, web online trading, indonesia, jakarta, bei, idx, indonesia stock exchange, ajax push, ajax">
<title>Welcome to Panin Sekuritas Online Stock Trading (POST)</title>
</head>
<body>
<font style="font-size:24px;">Please Wait ...</font>
</body>
</html>
<%
	}
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>POST - Welcome to POST</title>
<link href="../style/main.css" rel="stylesheet" type="text/css">
<link href="../style/dropdown/dropdown.css" media="screen" rel="stylesheet" type="text/css" />
<link href="../style/dropdown/default.advanced.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../servlets/AccountServlet?_a=jsbreakingnews"></script>
<script type="text/javascript" src="../script/menu.jsp"></script>
<script type="text/javascript" src="./home.js"></script>
<script type="text/javascript" src="../script/ping.js"></script>
<style type="text/css">
body {
	background-color: #ffffff;
	font: 11px/normal Tahoma, Arial, Helvetica, sans-serif;
}
</style>

</head>
<body>
<div id="page-wrap">
	<div id="header">
		<div id="logo-top"><img src="../images/logo.gif"></div>
		<div id="menu-right">
		<ul class="dropdown dropdown-horizontal">
			<li><a href="#" class="dir">HOME</a>
				<ul>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/index/home.html'">Home</a></li>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/index/news.html'">News History</a></li>
					<li><a href="#" onClick="javascript:window.open('/docs/manual_book.pdf');">Manual Book</a></li>
					<li><a href="#" onClick="javascript:window.open('http://'+_uri+'/index/stock_list.html',null,'width=335,height=300,scrollbars=yes');">Stock List</a></li>
					<li><a href="#" onClick="javascript:window.location='../account/feedback.html'">Feedback</a></li>
					<li><a href="#" onClick="javascript:window.open('/docs/manual_book_bb.pdf');">Blackberry Manual Book</a></li>
				</ul>
			</li>
			<li><a href="#" class="dir">ACCOUNT</a>
				<ul>
					<li><a href="../account/portfolio.html">Portfolio</a></li>
					<li><a href="../account/account_setting.html">Account Settings</a></li>
					<li><a href="../account/paymentconfirmation.html">Payment Confirmation</a></li>
				</ul>
			</li>
			<li><a href="#" class="dir">MARKET INFO</a>
				<ul>
					<li><a href="#" onClick="javascript:window.location='../market_info/running_trade.html'">Running Trade</a></li>
					<li><a href="#" onClick="javascript:window.location='../market_info/personal_quotes.html'">Personal Quotes</a></li>
					<li><a href="#" onClick="javascript:window.location='../market_info/multiple_order_book.html'">Multiple Order Books</a></li>
					<li><a href="#" onClick="javascript:window.location='../market_info/complete_quotes.html'">Complete Quotes</a></li>
					<li><a href="#" onClick="javascript:window.location='../market_info/single_stock_running.html'">Single Stock Running</a></li>
					<li><a href="#" onClick="javascript:window.location='../market_info/personal_running.html'">Personal Running</a></li>
					<li><a href="#" onClick="javascript:window.location='../market_info/complete_indices.html'">Complete Indices</a></li>
				</ul>
			</li>
			<li><a href="#" class="dir">ORDER</a>
				<ul>
					<li><a href="../order/order_command.html">Order Command</a></li>
					<li><a href="../order/amend_order.html">Amend Order</a></li>
					<li><a href="../order/withdraw_order.html">Withdraw Order</a></li>
					<li><a href="../order/account_order_list.html">Account Order List</a></li>
					<li><a href="../order/account_trade_list.html">Account Trade List</a></li>
				</ul>
			</li>
			<li><a href="#" class="dir">CHART</a>
				<ul>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/chart/chart.html'" class="dir">CHART</a></li>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/chart/fnchart.html'" class="dir">APPCHART</a></li>
				</ul>
			</li>
			<li><a href="#" class="dir">SUMMARY</a>
				<ul>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/summary/stock_ranking.html'">Stock Ranking</a></li>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/summary/broker_ranking.html'">Broker Ranking</a></li>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/summary/stock_summary.html'">Stock Summary</a></li>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/summary/broker_activity.html'">Broker Activity</a></li>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/summary/foreign_activity.html'">Foreign Activity</a></li>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/summary/detail_transaction.html'">Detail Transaction</a></li>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/summary/depth_transaction.html'">Depth Transaction</a></li>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/summary/historical_price.html'">Historical Price</a></li>
					<li><a href="#" onClick="javascript:window.location='http://'+_uri+'/summary/board_stock_type.html'">Board & Stock Type</a></li>
				</ul>
			</li>
			<li><a href="../logout.jsp" class="logout">LOGOUT</a></li>
		</ul>
		</div>
	</div>
	<div id="header_info">
		<div id="header_no_ihsg">&nbsp;</div>
	</div>
	<div id="content">
		<div id="home_left">
			<div class="home_title">Finance News</div>
			<div id="_antaraNews" class="inside" style="border-left: medium none; border-bottom: medium none; margin: 0px; padding: 0px; overflow: auto; width: 100%; height: 532px;">
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
			</div>
		</div>
		<div id="home_right">
			<div class="home_title">Indonesia Stock Exchange News</div>
			<div id="_idxNews" class="inside" style="border-left: medium none; border-bottom: medium none; margin: 0px; padding: 0px; overflow: auto; width: 489px; height: 220px;">
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
				<dl>
					<dt>&nbsp;</dt>
					<dd>&nbsp;</dd>
				</dl>
			</div>
			<div class="home_title2">Rights And Warrants</div>
			<table class="warrant_title" cellpadding="2" cellspacing="0" width="100%">
				<tr bgcolor="#ff0000">
					<td width="124" height="23px">Warrant</td>
					<td width="146">Exercise Price </td>
					<td width="81">Trade Date</td>
					<td width="104">Maturity Date </td>
					<td width="12"></td>
				</tr>
			</table>
			<div class="inside" style="border-left: medium none; border-bottom: medium none; margin: 0px; padding: 0px; overflow-y:scroll;overflow-x:hidden; width: 489px; height: 150px;">
				<table width="489" border="0" cellpadding="2" cellspacing="0" class="warrant_detail" id="_warrantNews">
					<!-- thead -->
					<!-- tr>
						<td width="10">&nbsp;</td>
						<td width="114">&nbsp;</td>
						<td width="4">&nbsp;</td>
						<td width="142">&nbsp;</td>
						<td width="81">&nbsp;</td>
						<td width="94">&nbsp;</td>
						<td width="22">&nbsp;</td>
					</tr>
					<tr>
						<td width="10">&nbsp;</td>
						<td width="147">&nbsp;</td>
						<td width="4">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="91">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="22">&nbsp;</td>
					</tr>
					<tr>
						<td width="10">&nbsp;</td>
						<td width="147">&nbsp;</td>
						<td width="4">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="91">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="22">&nbsp;</td>
					</tr>
					<tr>
						<td width="10">&nbsp;</td>
						<td width="147">&nbsp;</td>
						<td width="4">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="91">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="22">&nbsp;</td>
					</tr>
					<tr>
						<td width="10">&nbsp;</td>
						<td width="147">&nbsp;</td>
						<td width="4">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="91">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="22">&nbsp;</td>
					</tr>
					<tr>
						<td width="10">&nbsp;</td>
						<td width="147">&nbsp;</td>
						<td width="4">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="91">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="22">&nbsp;</td>
					</tr>
					<tr>
						<td width="10">&nbsp;</td>
						<td width="147">&nbsp;</td>
						<td width="4">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="91">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="22">&nbsp;</td>
					</tr>
					<tr>
						<td width="10">&nbsp;</td>
						<td width="147">&nbsp;</td>
						<td width="4">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="91">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="22">&nbsp;</td>
					</tr>
					<tr>
						<td width="10">&nbsp;</td>
						<td width="147">&nbsp;</td>
						<td width="4">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="91">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="22">&nbsp;</td>
					</tr>
					<tr>
						<td width="10">&nbsp;</td>
						<td width="147">&nbsp;</td>
						<td width="4">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="91">&nbsp;</td>
						<td width="87">&nbsp;</td>
						<td width="22">&nbsp;</td>
					</tr -->
					<!-- /thead>
					<tbody id="_warrant">
					</tbody -->
				</table>
			</div>
			<table class="warrant_title" cellpadding="2" cellspacing="0" width="100%">
				<tr bgcolor="#ff0000">
					<td width="124" height="23px">Rights</td>
					<td width="146">Exercise Price </td>
					<td width="81">Trade Date</td>
					<td width="104">Maturity Date </td>
					<td width="12"></td>
				</tr>
		  	</table>
			<div class="inside" style="border-left: medium none; border-bottom: medium none; margin: 0px; padding: 0px; overflow-y:scroll;overflow-x:hidden; width: 489px; height: 57px;">
				<table width="489" border="0" cellpadding="2" cellspacing="0" class="warrant_detail" id="_rightsNews">
					<!-- thead -->
						<!-- tr>
							<td width="10">&nbsp;</td>
							<td width="147">&nbsp;</td>
							<td width="4">&nbsp;</td>
							<td width="87">&nbsp;</td>
							<td width="91">&nbsp;</td>
							<td width="87">&nbsp;</td>
							<td width="22">&nbsp;</td>
						</tr>
						<tr>
							<td width="10">&nbsp;</td>
							<td width="147">&nbsp;</td>
							<td width="4">&nbsp;</td>
							<td width="87">&nbsp;</td>
							<td width="91">&nbsp;</td>
							<td width="87">&nbsp;</td>
							<td width="22">&nbsp;</td>
						</tr -->
					<!-- /thead>
					<tbody id="_rights">
					</tbody -->
				</table>
			</div>
	</div>
	<div id="footer">
	2010 &copy; PT Panin Sekuritas Tbk. All Rights Reserved. Version 1.2
	</div>
</div>
<script type="text/javascript">doBreakingNews();</script>
<script type="text/javascript">loadNews();</script>
<script type="text/javascript">
	window.open('/docs/popup.html',null,'width=600,height=350,scrollbars=yes');
</script>
</body>
</html>
