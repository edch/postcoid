var http_request = false;
function makeRequest(url, callerId)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	if ( callerId == 'antara' )
	{
		http_request.onreadystatechange = loadAntaraNewsResult;
	}
	else if ( callerId == 'idx' )
	{
		http_request.onreadystatechange = loadIdxNewsResult;
	}

	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadAntaraNewsResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadAntaraNewsResult(http_request.responseText);
		}
	}
}
function loadIdxNewsResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadIdxNewsResult(http_request.responseText);
		}
	}
}
function parseLoadIdxNewsResult(result)
{
	var _data = result.split('|');
	var _idxNews='';
	for (i=0; i<_data.length; ++i)
	{
		if ( _data[i].length <= 3 ) continue;
		var _subdata = _data[i].split('#');

		if (_subdata[0]=='I') //idx
		{
			if (i%2 == 0)
			{
				_idxNews += '<dl class="stock_running_detail_grey">';
			}
			else
			{
				_idxNews += '<dl class="stock_running_detail_white">';
			}
			_idxNews += '<dd><a href="#" onclick="javascript:window.open(\''+_subdata[3]+'\',null,\'width=335,height=300,scrollbars=yes\');" style="text-decoration: none;"><strong>'+_subdata[2]+'</strong></a></dt>';
			_idxNews += '</dl>';
		}
	}

	document.getElementById('_idxNews').innerHTML = _idxNews;
	var _viewButton = document.getElementById('sum_view_idx');
	_viewButton.value = 'View';
	_viewButton.disabled = false;
}
function parseLoadAntaraNewsResult(result)
{
	var _data = result.split('|');
	var _antaraNews='';
	for (i=0; i<_data.length; ++i)
	{
		if ( _data[i].length <= 3 ) continue;
		var _subdata = _data[i].split('#');

		if (_subdata[0]=='A') //antara
		{
			if (i%2 == 0)
			{
				_antaraNews += '<dl class="stock_running_detail_grey">';
			}
			else
			{
				_antaraNews += '<dl class="stock_running_detail_white">';
			}

			_antaraNews += '<dt><a href="#" onclick="javascript:window.open(\''+_subdata[3]+'\',null,\'width=335,height=300,scrollbars=yes\');" style="text-decoration: none;"><strong>'+_subdata[1]+'</strong></a></dt>';
			_antaraNews += '<dd>'+_subdata[2]+'</dt>';
			_antaraNews += '</dl>';
		}
	}

	document.getElementById('_antaraNews').innerHTML = _antaraNews;
	var _viewButton = document.getElementById('sum_view_antara');
	_viewButton.value = 'View';
	_viewButton.disabled = false;
}
function loadAntaraNews()
{
	var _viewButton = document.getElementById('sum_view_antara');
	_viewButton.value = 'Loading..';
	_viewButton.disabled = true;
	var _date = document.getElementById('popupDatepicker').value;
	makeRequest('../servlets/AccountServlet?_a=antaralist&_d=' + _date, 'antara');
}
function loadIdxNews()
{
	var _viewButton = document.getElementById('sum_view_idx');
	_viewButton.value = 'Loading..';
	_viewButton.disabled = true;
	var _date = document.getElementById('popupDatepicker2').value;
	makeRequest('../servlets/AccountServlet?_a=idxlist&_d=' + _date, 'idx');
}
Number.prototype.formatMoney = function (c, d, t)
{
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t,
    i = parseInt(n = (+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + (n - i).toFixed(c).slice(2) : "");
};
