var http_request = false;
function makeRequest(url)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	http_request.onreadystatechange = loadResult;
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadResult(http_request.responseText);
		}
	}
}
function parseLoadResult(result)
{
	var _data = result.split('|');
	clearTable();
	// var _stockList='';
	var _stockList = document.getElementById("_stockList");
	for (i=0; i<_data.length; ++i)
	{
		if ( _data[i].length <= 3 ) continue;
		var _subdata = _data[i].split('#');

		//if (i%2 == 0) _stockList += '<tr class="stock_running_detail_grey">';
		//else _stockList += '<tr class="stock_running_detail_white">';
		_tr = _stockList.insertRow(_stockList.rows.length);
		if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
		else _tr.setAttribute("class", "stock_running_detail_white");

		_td = _tr.insertCell(_tr.cells.length);
		_td.setAttribute('align','left');
		_td.setAttribute('style','border-right:1px #999999 solid;');
		_td.innerHTML = _subdata[0];

		_td = _tr.insertCell(_tr.cells.length);
		_td.setAttribute('align','left');
		_td.innerHTML = _subdata[1];

		//_stockList += '<td align="left">'+_subdata[0]+'</td><td align="left">'+_subdata[1]+'</td>';
		//_stockList += '</tr>';
	}

	// document.getElementById('_stockList').innerHTML = _stockList;
}
function clearTable()
{
	var tbody = document.getElementById("_stockList");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
function loadData()
{
	makeRequest('../servlets/AccountServlet?_a=namestocklist');
}
