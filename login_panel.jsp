<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300|Roboto+Condensed:400,300,700|Lato:400,700" rel="stylesheet" type="text/css">
<script type="text/javascript">
function doSubmit()
{
	var _password = document.getElementById("password").value;
	var _token = document.getElementById("j_token").value;
	document.getElementById("j_password").value = "<%= session.getId() %>#" + _token + "#<%= request.getRemoteAddr() %>#<%= request.getRemotePort() %>#" + _password;
	return true;
}
</script>

<style>
	* {
		margin: 0;
		padding: 0;
	}
	body {
		font-family: "Roboto Condensed","Helvetica Neue",Helvetica,Arial,sans-serif;
		font-size: 14px;
	}
	input, button, select, textarea {
	  font-family: inherit;
	  font-size: inherit;
	  line-height: inherit;
	}
	label {
		display: inline-block;
	  max-width: 100%;
	  margin-bottom: 5px;
	  font-weight: 700;
	}
	a {
		text-decoration: none;
	}
	.white {
		color: #FFF;
	}
	.error {
		font-size: 12px;
		color: #CE1A00;
		margin-bottom: 5px;
	}
	.bm-1 {
		margin-bottom: 5px;
	}
	.bm-3 {
		margin-bottom: 15px;
	}
	.divider {
	  display: block;
	  width: 100%;
	  height: 1px;
	  margin: 9px 0;
	  overflow: hidden;
	  background-color: #777777;
	}
	.fw-light {
	  font-weight: 200!important;
	}
	.fw-md {
	  font-weight: 500!important;
	}
	.fz-xs {
	  font-size: 12px!important;
	}
	.fz-sm {
	  font-size: 14px!important;
	}
	.fz-base {
	  font-size:16px!important;
	}
	.fz-md {
	  font-size:18px!important;
	}
	.fz-lg {
	  font-size:24px!important;
	}
	.fz-xlg {
	  font-size:32px!important;
	}
	.fz-huge {
	  font-size:42px!important;
	}
	.login-panel {
		  position: relative;
		  width: 225px;
	}
	.login-panel .panel {
		margin-bottom: 20px;
	  background-color: #fff;
	  border: 0;
	  border-radius: 4px;
	  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
	  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
	}
	.login-panel .panel-default {
		background-color: rgba(0, 0, 0, 0.6);
 		border-color: transparent;
	}
	.panel-heading {
	  padding: 10px 15px;
	  border-bottom: 1px solid transparent;
	  border-top-right-radius: 3px;
	  border-top-left-radius: 3px;
	}
	.login-panel .panel-default .panel-heading {
		background-color: rgba(33, 131, 200, 0.4);
	  border-color: transparent;
	  color: #FFF;
	  text-transform: uppercase;
	}
	.login-panel .panel-default .panel-body {
		color: #FFF;
	}
	.panel-body {
		padding: 15px;
	}
	.panel-body:before, .panel-body:after {
	  content: " ";
	  display: table;
	}
	.panel-body:after {
	  clear: both;
	}
	.form-group {
	  margin-bottom: 15px;
	}
	.form-control {
		display: block;
	  width: 100%;
	  height: 34px;
	  padding: 6px 12px;
	  font-size: 14px;
	  line-height: 1.42857;
	  color: #FFF;
	  background-color: transparent;
	  background-image: none;
	  border: 1px solid #ccc;
	  border-radius: 4px;
	  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	  -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
	  -o-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
	  transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
	}
	.login-panel .form-control:focus {
	  box-shadow: none;
	  -webkit-box-shadow: none;
	  border-color: #CCC;
	}
	.btn {
	  display: inline-block;
	  margin-bottom: 0;
	  font-weight: normal;
	  text-align: center;
	  vertical-align: middle;
	  touch-action: manipulation;
	  cursor: pointer;
	  background-image: none;
	  border: 1px solid transparent;
	  white-space: nowrap;
	  padding: 6px 12px;
	  font-size: 14px;
	  line-height: 1.42857;
	  border-radius: 4px;
	  -webkit-user-select: none;
	  -moz-user-select: none;
	  -ms-user-select: none;
	  user-select: none;
	}
	.btn-block {
	  display: block;
	  width: 100%;
	}
	.btn-red {
	  color: #FFF;
	  background-color: #CE1A00;
	  border-color: #CE1A00;
	}
	.btn-red:hover {
	  color: #FFF;
	  background-color: #ff2202;
	}
	.btn-login {
		margin-bottom: 15px;
	}
</style>
<div class="login-panel">
	<div class="panel panel-default">
		<div class="panel-heading">Login Nasabah</div>
		<div class="panel-body">
    	<form method="POST" action="j_security_check" onsubmit="return doSubmit();" target="_parent">
  			<p class="error"><%= com.piwi.web.login.OnlineTradingMessage.getMessage ( session.getId() ) %></p>
			  <div class="form-group">
			    <input type="text" class="form-control" name="j_username" id="j_username" placeholder="Username">
			  </div>
			  <div class="form-group">
			    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
			  </div>
			  <div class="form-group">
			    <label class="fw-md">Ketik Angka di Bawah</label>
			    <input type="text" class="form-control" name="j_token" id="j_token">
			  </div>
			  <div class="form-group">
			  	<img src="./publicservlets/AuthToken" width="178" height="50">
					<input type="hidden" name="j_password" id="j_password" value="">
			  </div>
			  <p class="bm-1"><a href="/root/public/forget.faces" class="white fz-xs" target="_blank">Lupa Kata Sandi?</a></p>
			  <!--<p class="bm-1"><a href="/syarat-dan-ketentuan.html" class="white fz-xs" target="_blank">Baca Syarat dan Ketentuan</a></p>-->
			  <span class="divider"></span>
			  <button type="submit" class="btn btn-red btn-block text-uppercase btn-login">Login</button>
<!--				<p class="bm-1 fz-xs">Bukan Nasabah? <a href="/instruksi-pembukaan-rekening.html" class="white" target="_blank">Buat Rekening Baru</a></p>-->
			</form>
		</div>
	</div>
</div>
