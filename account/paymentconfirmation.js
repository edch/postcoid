var http_request = false;
function makeRequest(url)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	http_request.onreadystatechange = loadAccountResult;
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadAccountResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadAccountResult(http_request.responseText);
		}
	}
}
function parseLoadAccountResult(result)
{
	var _account = document.getElementById('_account');
	var acc = result.split('#');
	_account.options.length = 0;
	for (i=0; i<acc.length; ++i)
	{
		if ( acc[i].length <= 0 )
		{
			continue;
		}
		try
		{
			_account.options[i] = new Option(acc[i], acc[i], false, false);
		}
		catch (err){}
	}

	document.getElementById('_userid').innerHTML = _userId;
}
function loadAccount()
{
	// makeRequest('../servlets/AccountServlet?_a=acclist');
	parseLoadAccountResult ( _accountList );
}


var http_post = false;
function makePOSTRequest(url, parameters)
{
	http_post = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_post = new XMLHttpRequest();
		if (http_post.overrideMimeType)
		{
			http_post.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_post = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_post = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_post)
	{
		return false;
	}

	http_post.onreadystatechange = saveConfirmationResult;
	http_post.open('POST', url, true);
	http_post.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_post.setRequestHeader("Content-length", parameters.length);
	http_post.setRequestHeader("Connection", "close");
	http_post.send(parameters);
}
function saveConfirmationCommand()
{
	var account = document.getElementById("_account").value;
	var pin = document.getElementById("_pin").value;
	if ( pin.length < 1 )
	{
		alert ('Please Enter Your PIN.');
		return;
	}

	var dateStr = document.getElementById("popupDatepicker").value;
	var bank = document.getElementById("_bank").value;
	var bankAcc = document.getElementById("_bankAcc").value;
	var validationNo = document.getElementById("_validationNo").value;

	var deposit = 'transfer';
	if ( document.getElementById ('_deposit2').checked )
	{
		deposit = 'cash';
		if ( validationNo.length < 1 )
		{
			alert ('Please Fill Validation No.');
			return;
		}
	}
	else if ( document.getElementById ('_deposit1').checked )
	{
		deposit = 'transfer';
		if ( bank.length < 1 )
		{
			alert ('Please Fill Bank.');
			return;
		}

		if ( bankAcc.length < 1 )
		{
			alert ('Please Fill Bank Account');
			return;
		}
	}
	else
	{
		alert ('Please Select Deposit Type.');
		return;
	}

	var amount = document.getElementById("_amount").value;
	if ( amount < 1 )
	{
		alert ('Please Fill Transfer Amount');
		return;
	}

	var transferTo = 'BCA';
	if ( document.getElementById ('_transferTo1').checked ) transferTo = 'BCA Cabang Bursa Efek Indonesia';
	else if ( document.getElementById ('_transferTo2').checked ) transferTo = 'Bank Mandiri Cabang Bursa Efek Indonesia';
	else if ( document.getElementById ('_transferTo3').checked ) transferTo = 'Bank Panin Cabang Senayan';
	else if ( document.getElementById ('_transferTo4').checked ) transferTo = 'Others';
	else
	{
		alert ('Please Choose Transfer To.');
		return;
	}

	var comment = document.getElementById("_comments").value;
	var _url = "_a=paymentconfirmation" ;
	_url += "&account=" + encodeURI(account);
	_url += "&pin=" + encodeURI(pin);
	_url += "&date=" + encodeURI(dateStr);
	_url += "&deposit=" + encodeURI(deposit);
	_url += "&validationNo=" + encodeURI(validationNo);
	_url += "&bank=" + encodeURI(bank);
	_url += "&bankAcc=" + encodeURI(bankAcc);
	_url += "&amount=" + encodeURI(amount);
	_url += "&transferTo=" + encodeURI(transferTo);
	_url += "&comment=" + encodeURI(comment);

	document.getElementById('_enter').value = 'Sending';
	document.getElementById('_enter').disabled = true;
	makePOSTRequest('../servlets/AccountServlet', _url);
}
function saveConfirmationResult()
{
	if (http_post.readyState == 4)
	{
		if (http_post.status == 200)
		{
			clearEntry();
			alert (http_post.responseText);
		}
		else if ( http_post.status == 404 )
		{
			alert ('Invalid PIN.');
		}
	}
}
function depositTypeChange()
{
	if ( document.getElementById ('_deposit1').checked )
	{
		document.getElementById("_bank").disabled = false;
		document.getElementById("_bankAcc").disabled = false;
		document.getElementById("_validationNo").disabled = true;
	}
	else if ( document.getElementById ('_deposit2').checked )
	{
		document.getElementById("_bank").disabled = true;
		document.getElementById("_bankAcc").disabled = true;
		document.getElementById("_validationNo").disabled = false;
	}
}
function clearEntry()
{
	try
	{
		document.getElementById('_pin').value = '';
		document.getElementById('_validationNo').value = '';
		document.getElementById('_bank').value = '';
		document.getElementById('_bankAcc').value = '';
		document.getElementById('_amount').value = '';
		document.getElementById('_comments').value = '';
		document.getElementById('_enter').value = 'Submit Confirmation';
		document.getElementById('_enter').disabled = false;

		document.getElementById('_deposit1').checked = false;
		document.getElementById('_deposit2').checked = false;
		document.getElementById('_transferTo1').checked = false;
		document.getElementById('_transferTo2').checked = false;
		document.getElementById('_transferTo3').checked = false;

		document.getElementById('popupDatepicker').value = '';
	}
	catch (err) {}
}
