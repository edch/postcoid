var http_request = false;
var _cashWithdrawString = '';
function makeRequest(url)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}

	http_request.onreadystatechange = loadAccountResult;
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadAccountResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadAccountResult(http_request.responseText);
		}
	}
}
function parseLoadAccountResult(result)
{
	var _account = document.getElementById('_account');
	var _bankAccount = document.getElementById('_bankAccount');
	_cashWithdrawString = result;
	var acc = result.split('#');
	_account.options.length = 0;
	_bankAccount.options.length = 0;
	
	for (i=0; i<acc.length; ++i)
	{
		if ( acc[i].length <= 0 )
		{
			continue;
		}
		//try
		{
			var _info = acc[i].split('|');
			_account.options[i] = new Option(_info[0], _info[0], false, false);

			for (var j=2; j<_info.length; ++j)
			{
				var _infos = _info[j].split(';');
				_bankAccount.options[j-2] = new Option(_infos[0], _infos[0], false, false);
			}
		}
		//catch (err){}
	}

	document.getElementById('_userid').innerHTML = _userId;
	updateInformation();
}
function loadAccount()
{
	makeRequest('../servlets/AccountServlet?_a=acclistcw');
}
function updateAccountInformation()
{
	var account = document.getElementById("_account").value;
	var bankAccount = document.getElementById("_bankAccount");
	var acc = _cashWithdrawString.split('#');

	//try
	{
		bankAccount.options.length = 0;
		for (i=0; i<acc.length; ++i)
		{
			if ( acc[i].length <= 0 )
			{
				continue;
			}

			//try
			{
				var _info = acc[i].split('|');
				if ( _info[0] != account ) continue;
	
				for (var j=2; j<_info.length; ++j)
				{
					var _infos = _info[j].split(';');
					bankAccount.options[j-2] = new Option(_infos[0], _infos[0], false, false);
				}
			}
			//catch (err){}
		}
	}
	//catch (err) {}

	updateInformation();
}
function updateInformation()
{
	//try
	{
		var account = document.getElementById("_account").value;
		var bankAccount = document.getElementById("_bankAccount").value;
		var acc = _cashWithdrawString.split('#');

		document.getElementById('_bankName').innerHTML = '';
		document.getElementById('_bank').innerHTML = '';
		document.getElementById('_branch').innerHTML = '';

		for (i=0; i<acc.length; ++i)
		{
			if ( acc[i].length <= 0 )
			{
				continue;
			}

			var _info = acc[i].split('|');
			if ( _info[0] != account ) continue;

			document.getElementById('_accountName').innerHTML = _info[1];
			for (var j=2; j<_info.length; ++j)
			{
				var _infos = _info[j].split(';');
				if ( _infos[0] == bankAccount )
				{
					document.getElementById('_bankName').innerHTML = _infos[1];
					document.getElementById('_bank').innerHTML = _infos[2];
					document.getElementById('_branch').innerHTML = _infos[3];
					break;
				}
			}

			break;
		}
	}
	//catch (err) {}
}


var http_post = false;
function makePOSTRequest(url, parameters)
{
	http_post = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_post = new XMLHttpRequest();
		if (http_post.overrideMimeType)
		{
			http_post.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_post = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_post = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_post)
	{
		return false;
	}

	http_post.onreadystatechange = saveWithdrawResult;
	http_post.open('POST', url, true);
	http_post.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_post.setRequestHeader("Content-length", parameters.length);
	http_post.setRequestHeader("Connection", "close");
	http_post.send(parameters);
}
function saveWithdrawCommand()
{
	var account = document.getElementById("_account").value;
	var pin = document.getElementById("_pin").value;
	if ( pin.length < 1 )
	{
		alert ('Please Enter Your PIN.');
		document.getElementById ('_pin').focus();
		return;
	}

	var amount = document.getElementById ("_amount").value;
	if ( amount.length < 3 )
	{
		alert ('Please Enter a Valid Amount.');
		document.getElementById ('_amount').focus();
		return;
	}

	var terbilang = document.getElementById ("_terbilang").value;
	if ( terbilang.length < 3 )
	{
		alert ('Please Enter a Valid Value.');
		document.getElementById ('_terbilang').focus();
		return;
	}

	var dateStr = document.getElementById("popupDatepicker").value;
	if ( dateStr.length < 3 )
	{
		alert ('Please Enter a Valid Date.');
		return;
	}

	// var comment = document.getElementById("_comments").value;
	var bankAccount = document.getElementById("_bankAccount").value;
	if ( bankAccount.length < 3 )
	{
		alert ('You do not have Bank Account for Fund Withdrawal.');
		return;
	}

	var _url = "_a=cashwithdraw" ;
	_url += "&account=" + encodeURI(account);
	_url += "&pin=" + encodeURI(pin);
	_url += "&date=" + encodeURI(dateStr);
	_url += "&amount=" + encodeURI(amount);
	_url += "&terbilang=" + encodeURI(terbilang);
	_url += "&bank=" + encodeURI(bankAccount);
	// _url += "&comment=" + encodeURI(comment);

	document.getElementById('_enter').value = 'Sending';
	document.getElementById('_enter').disabled = true;
	makePOSTRequest('../servlets/AccountServlet', _url);
}
function saveWithdrawResult()
{
	if (http_post.readyState == 4)
	{
		if (http_post.status == 200)
		{
			clearEntry();
			alert (http_post.responseText);
		}
		else if ( http_post.status == 404 )
		{
			alert ('Invalid PIN.');
		}
	}
}
function clearEntry()
{
	try
	{
		document.getElementById('_pin').value = '';
		document.getElementById('_amount').value = '';
		// document.getElementById('_comments').value = '';
		document.getElementById('_terbilang').value = '';
		document.getElementById('popupDatepicker').value = '';
		document.getElementById('_enter').value = 'Submit Withdraw';
		document.getElementById('_enter').disabled = false;

		document.getElementById('popupDatepicker').value = '';
	}
	catch (err) {}
}
