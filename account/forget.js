var http_request = false;
function makeRequest(url,callerId)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}
	if (callerId == 'loadAccount')
	{
		http_request.onreadystatechange = loadAccountResult;
	}
	if (callerId == 'submitAnswer')
	{
		http_request.onreadystatechange = submitAnswerResult;
	}
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadAccountResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadAccountResult(http_request.responseText);
		}
	}
}
function parseLoadAccountResult(result)
{
	var _account = document.getElementById('_account');
	var acc = result.split('#');

	document.getElementById('_secretQuestion').innerHTML = acc[0];
	_account.options.length = 0;
	for (i=1; i<acc.length; ++i)
	{
		if ( acc[i].length <= 0 )
		{
			continue;
		}
		try
		{
			_account.options[i] = new Option(acc[i], acc[i], false, false);
		}
		catch (err){}
	}
}
function loadAccount()
{
	makeRequest('../servlets/AccountServlet?_a=qacclist','loadAccount');
}

function submitAnswerResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			document.getElementById('_forgetResult').innerHTML = 'Your New PIN has been sent to ' + http_request.responseText;
		}
		else if (http_request.status == 403)
		{
			document.getElementById('_forgetResult').innerHTML = 'Invalid Answer.';
		}
		else if (http_request.status == 404)
		{
			document.getElementById('_forgetResult').innerHTML = 'Invalid Answer.';
		}

		document.getElementById('_submitAnswer').value = 'Request My PIN';
		document.getElementById('_submitAnswer').disabled = false;
	}
}
function submitAnswer()
{
	document.getElementById('_submitAnswer').value = 'Please Wait..';
	document.getElementById('_submitAnswer').disabled = true;
	var answer =  document.getElementById("_answer").value;
	var account =  document.getElementById("_account").value;
	makeRequest('../servlets/AccountServlet?_a=resetpin&account=' + account + '&answer=' + answer,'submitAnswer');
}

