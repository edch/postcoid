var http_post = false;
function makePOSTRequest(url, parameters)
{
	http_post = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_post = new XMLHttpRequest();
		if (http_post.overrideMimeType)
		{
			http_post.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_post = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_post = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_post)
	{
		return false;
	}

	http_post.onreadystatechange = feedbackReply;
	http_post.open('POST', url, true);
	http_post.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_post.setRequestHeader("Content-length", parameters.length);
	http_post.setRequestHeader("Connection", "close");
	http_post.send(parameters);
}
function giveFeedback()
{
	var subject =  document.getElementById("_subject").value;
	var feedback =  document.getElementById("_feedback").value;

	document.getElementById('_confirm').disabled = true;

	var _url = "_a=feedback" ;
	_url += "&subject=" + encodeURI(subject);
	_url += "&feedback=" + encodeURI(feedback);
	makePOSTRequest('../servlets/AccountServlet',_url);
}
function feedbackReply()
{
	if (http_post.readyState == 4)
	{
		if (http_post.status == 200)
		{
			document.getElementById('_confirmError').innerHTML = '<span class="col_red"><strong>'+http_post.responseText+'</strong></span>';
		}
		else if ( http_post.status == 403 )
		{
			document.getElementById('_confirmError').innerHTML = '<span class="col_red"><strong>User Already Confirmed. Confirm Failed.</strong></span>';
			// document.getElementById('_confirm').disabled = false;
		}
		else if ( http_post.status == 404 )
		{
			document.getElementById('_confirmError').innerHTML = '<span class="col_red"><strong>User Already Confirmed. Confirm Failed.</strong></span>';
			// document.getElementById('_confirm').disabled = false;
		}

		document.getElementById('_confirm').disabled = false;
		document.getElementById("_subject").value = '';
		document.getElementById("_feedback").value = '';
	}
}
