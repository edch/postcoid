var http_request = false;
var _savedPin = '';
function makeRequest(url,callerId)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}
	if (callerId == 'loadAccount')
	{
		http_request.onreadystatechange = loadAccountResult;
	}
	else if(callerId == 'loadPortfolio')
	{
		http_request.onreadystatechange = loadPortfolioResult;
	}
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadAccountResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadAccountResult(http_request.responseText);
		}
	}
}
function parseLoadAccountResult(result)
{
	var _account = document.getElementById('_account');
	var acc = result.split('#');
	_account.options.length = 0;
	for (i=0; i<acc.length; ++i)
	{
		if ( acc[i].length <= 0 )
		{
			continue;
		}
		try
		{
			_account.options[i] = new Option(acc[i], acc[i], false, false);
		}
		catch (err){}
	}
}
function loadAccount()
{
	// makeRequest('../servlets/AccountServlet?_a=acclist','loadAccount');
	parseLoadAccountResult ( _accountList );
}

function loadPortfolioCommand()
{
	document.getElementById('_enter').value = 'Load';
	document.getElementById('_enter').disabled = true;
	document.getElementById('_refresh').value = 'Loading..';
	document.getElementById('_refresh').disabled = true;
	loadPin();
	var account =  document.getElementById("_account").value;
	makeRequest('../servlets/AccountServlet?_a=portfolio&_account=' + account + '&_pin=' + _savedPin,'loadPortfolio');
}
function loadPortfolioResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadPortfolioResult(http_request.responseText);
		}
		else if ( http_request.status == 403 )
		{
			invalidPin ( 'Wrong PIN.' );
		}
		else if ( http_request.status == 404 )
		{
			invalidPin ( 'Wrong PIN.' );
		}
		else if ( http_request.status == 410 )
		{
			invalidPin ( 'System Consolidation.' );
			alert ('System Currently on Daily Consolidation. Please Try Again Later.');
		}
	}
}
function invalidPin ( msg )
{
	document.getElementById('sp_post').innerHTML = msg;
	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
	document.getElementById('_refresh').value = 'Refresh';
	document.getElementById('_refresh').disabled = false;
	document.getElementById('_rtl').innerHTML = '0';
	document.getElementById('_limitRatio').innerHTML = '0';
	document.getElementById('_cashBalance').innerHTML = '0';
	document.getElementById('_dueBalance').innerHTML = '0';
	document.getElementById('_dueOutstanding').innerHTML = '0';
	document.getElementById("_totalCurrentValue").innerHTML = '0';
	document.getElementById("_totalPotentialGL").innerHTML = '0';
	clearTable();
}
function clearTable()
{
	var tbody = document.getElementById("_portfolioTable");
	while (tbody.childNodes.length > 0)
	{
		tbody.removeChild(tbody.firstChild);
	}
}
function parseLoadPortfolioResult(result)
{
	var _data = result.split('|');
	var _subdata = _data[0].split('#');
	var _tBuy=0,_tSell=0;
	var _totalValue = parseInt(_subdata[0]);
	var _rtl = parseInt(_subdata[1]);
	var _limitRatio = parseFloat(_subdata[2]);
	var _cashBalance = parseInt(_subdata[3]);
	var _dueBalance = parseInt(_subdata[4]);
	var _dueOutstanding = parseInt(_subdata[5]);
	document.getElementById('_rtl').innerHTML = _rtl.formatMoney(0);
	if (_limitRatio>=0)
	{
		document.getElementById('_limitRatio').innerHTML = _limitRatio.formatMoney(2);
		document.getElementById('_limitRatio').className = "col_green";
	}
	else
	{
		document.getElementById('_limitRatio').innerHTML = "-"+Math.abs(_limitRatio).formatMoney(3);
		document.getElementById('_limitRatio').className = "col_red";
	}
	if (_cashBalance>=0)
	{
		document.getElementById('_cashBalance').innerHTML = _cashBalance.formatMoney(0);
		document.getElementById('_cashBalance').className = "col_green";
	}
	else
	{
		document.getElementById('_cashBalance').innerHTML = "-"+Math.abs(_cashBalance).formatMoney(0);
		document.getElementById('_cashBalance').className = "col_red";
	}
	if (_dueBalance>=0)
	{
		document.getElementById('_dueBalance').innerHTML = _dueBalance.formatMoney(0);
		document.getElementById('_dueBalance').className = "col_green";
	}
	else
	{
		document.getElementById('_dueBalance').innerHTML = "-"+Math.abs(_dueBalance).formatMoney(0);
		document.getElementById('_dueBalance').className = "col_red";
	}
	document.getElementById('_dueOutstanding').innerHTML = _dueOutstanding.formatMoney(0);
	var _totalCurrentValue = 0;
	var _totalPotentialGL = 0;
	document.getElementById('sp_post').innerHTML = _subdata[6];

	// ar ap part
	_subdata = _data[1].split('#');
	for (var i=0; i<_subdata.length;++i)
	{
		if (_subdata[i].length <= 3 ) continue;
		var _arap = _subdata[i].split('=');
		document.getElementById('_t'+_arap[0]).innerHTML = _arap[1];
		var _arapValue = parseInt(_arap[2]);
		if (_arapValue>=0)
		{
			document.getElementById('_nett'+_arap[0]).innerHTML = _arapValue.formatMoney(0);
			document.getElementById('_nett'+_arap[0]).className = "col_green";
		}
		else
		{
			document.getElementById('_nett'+_arap[0]).innerHTML = "-"+Math.abs(_arapValue).formatMoney(0);
			document.getElementById('_nett'+_arap[0]).className = "col_red";
		}
	}

	clearTable();
	var _pTable = document.getElementById("_portfolioTable");
	var _tr,_td;
	for(var i=2;i<_data.length;++i)
	{
		if (_data[i].length<=3) continue;
		_tr = _pTable.insertRow(_pTable.rows.length);

		_subdata = _data[i].split('#');
		if (i%2 == 0) _tr.setAttribute("class", "stock_running_detail_grey");
		else _tr.setAttribute("class", "stock_running_detail_white");

		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = "<div align='left'>"+_subdata[0]+"</div>";
		var _avg = parseInt(_subdata[1]);
		var _last = parseInt(_subdata[2]);
		var _lots = parseInt(_subdata[3]);
		var _volume = parseInt(_subdata[4]);

		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = _avg.formatMoney(0);
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = _last.formatMoney(0);
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = _lots.formatMoney(0);
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = _volume.formatMoney(0);
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = (_last*_volume).formatMoney(0);

		_totalCurrentValue += (_last*_volume);
		var _gl = (_last*_volume)-(_avg*_volume);
		_totalPotentialGL += _gl;

		if (_gl>=0)
		{
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = '<span class="col_green">' + _gl.formatMoney(0) + '</span>';

			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = '<span class="col_green">' + (_gl*100/(_avg*_volume)).formatMoney(2) + '</span>';

		}
		else
		{
			_gl = Math.abs(_gl);
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = '<span class="col_red">-' + _gl.formatMoney(0) + '</span>';
			_td = _tr.insertCell(_tr.cells.length);
			_td.innerHTML = '<span class="col_red">-' + (_gl*100/(_avg*_volume)).formatMoney(2) + '</span>';
		}

		_td = _tr.insertCell(_tr.cells.length);
		// _td.innerHTML = ((_avg*_volume)*100/_totalValue).formatMoney(2);
		_td.innerHTML = ((_last*_volume)*100/_totalValue).formatMoney(2);
		_td = _tr.insertCell(_tr.cells.length);
		_td.innerHTML = (parseFloat(_subdata[5])).formatMoney(2);
	}

	document.getElementById("_totalCurrentValue").innerHTML = _totalCurrentValue.formatMoney(0);
	if (_totalPotentialGL>=0)
	{
		document.getElementById("_totalPotentialGL").innerHTML = _totalPotentialGL.formatMoney(0);
		document.getElementById("_totalPotentialGL").className = "col_green";
	}
	else
	{
		_totalPotentialGL = Math.abs(_totalPotentialGL);
		document.getElementById("_totalPotentialGL").innerHTML = "-" + _totalPotentialGL.formatMoney(0);
		document.getElementById("_totalPotentialGL").className = "col_red";
	}

	document.getElementById('_enter').value = 'Enter';
	document.getElementById('_enter').disabled = false;
	document.getElementById('_refresh').value = 'Refresh';
	document.getElementById('_refresh').disabled = false;
}
function getColor(prev,last)
{
	if (prev>last) return 'col_red';
	else if (prev<last) return 'col_green';
	return 'col_yellow';
}

function printPortfolioCommand()
{
	loadPin();
	var account =  document.getElementById("_account").value;
	window.open('../servlets/AccountServlet?_a=printportfolio&_account=' + account + '&_pin=' + _savedPin,'Print Portfolio');
}
function loadPin()
{
	var _t = document.getElementById('_pin').value;
	if (_t&&_t.length>1) _savedPin = _t;
	document.getElementById('_pin').value = '';
}
function handlePortfolioKeyPress(e, obj)
{
	if (!e) e = window.event;
	if (e && e.keyCode == 13)
	{
		loadPortfolioCommand();
		return false;
	}
}
