var http_post = false;
function makePOSTRequest(url, parameters)
{
	http_post = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_post = new XMLHttpRequest();
		if (http_post.overrideMimeType)
		{
			http_post.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_post = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_post = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_post)
	{
		return false;
	}

	http_post.onreadystatechange = activateAccountResult;
	http_post.open('POST', url, true);
	http_post.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_post.setRequestHeader("Content-length", parameters.length);
	http_post.setRequestHeader("Connection", "close");
	http_post.send(parameters);
}
function activateAccount()
{
	var secretQuestion =  document.getElementById("_secretQuestion").value;
	if (secretQuestion.length < 6)
	{
		document.getElementById('_confirmError').innerHTML = '<span class="col_red"><strong>Secret Question at Least 6 Character</strong></span>';
		return;
	}

	var secretAnswer =  document.getElementById("_secretAnswer").value;
	if (secretAnswer.length < 6)
	{
		document.getElementById('_confirmError').innerHTML = '<span class="col_red"><strong>Secret Answer at Least 6 Character</strong></span>';
		return;
	}

	var email =  document.getElementById("_email").value;
	if (secretAnswer.length < 6)
	{
		document.getElementById('_confirmError').innerHTML = '<span class="col_red"><strong>Email at Least 6 Character</strong></span>';
		return;
	}

	document.getElementById('_confirm').disabled = true;

	var _url = "_a=secret" ;
	_url += "&squestion=" + encodeURI(secretQuestion);
	_url += "&sanswer=" + encodeURI(secretAnswer);
	_url += "&email=" + encodeURI(email);
	makePOSTRequest('../servlets/AccountServlet',_url);
}
function activateAccountResult()
{
	if (http_post.readyState == 4)
	{
		if (http_post.status == 200)
		{
			document.getElementById('_confirmError').innerHTML = '<span class="col_red"><strong>'+http_post.responseText+'</strong></span>';
			window.location = '../index/home.html';
		}
		else if ( http_post.status == 403 )
		{
			document.getElementById('_confirmError').innerHTML = '<span class="col_red"><strong>User Already Confirmed. Confirm Failed.</strong></span>';
		}
		else if ( http_post.status == 404 )
		{
			document.getElementById('_confirmError').innerHTML = '<span class="col_red"><strong>User Already Confirmed. Confirm Failed.</strong></span>';
		}
	}
}
