var http_request = false;
function makeRequest(url,callerId)
{
	http_request = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType)
		{
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_request)
	{
		return false;
	}
	if (callerId == 'loadAccount')
	{
		http_request.onreadystatechange = loadAccountResult;
	}
	http_request.open('GET', url, true);
	http_request.send(null);
}
function loadAccountResult()
{
	if (http_request.readyState == 4)
	{
		if (http_request.status == 200)
		{
			parseLoadAccountResult(http_request.responseText);
		}
		else if ( http_request.status == 302 )
		{
		}
		else
		{
		}
	}
}
function parseLoadAccountResult(result)
{
	var _account = document.getElementById('_account');
	var acc = result.split('#');
	_account.options.length = 0;
	for (i=0; i<acc.length; ++i)
	{
		if ( acc[i].length <= 0 )
		{
			continue;
		}
		try
		{
			// _account.add ( new Option(acc[i],acc[i]), null );
			_account.options[i] = new Option(acc[i], acc[i], false, false);
		}
		catch (err){}
	}
}
function loadAccount()
{
	makeRequest('../servlets/AccountServlet?_a=acclist','loadAccount');
}

var http_post = false;
function makePOSTRequest(url, parameters, callerId)
{
	http_post = false;
	if (window.XMLHttpRequest)
	{ // Mozilla, Safari,...
		http_post = new XMLHttpRequest();
		if (http_post.overrideMimeType)
		{
			http_post.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject)
	{ // IE
		try {
			http_post = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
		try {
			http_post = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
		}
	}

	if (!http_post)
	{
		return false;
	}

	if ( callerId == 'changePassword' )
	{
		http_post.onreadystatechange = changePasswordResult;
	}
	else if ( callerId == 'changePin' )
	{
		http_post.onreadystatechange = changePinResult;
	}
	http_post.open('POST', url, true);
	http_post.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_post.setRequestHeader("Content-length", parameters.length);
	http_post.setRequestHeader("Connection", "close");
	http_post.send(parameters);
}

function changeMyPassword()
{
	var old =  document.getElementById("_oldpassword").value;
	var pwd =  document.getElementById("_password").value;
	var rpwd =  document.getElementById("_repeatPassword").value;
	if (pwd == rpwd)
	{
		var _url = "_a=chgpwd" ;
		_url += "&oldpwd=" + encodeURI(old);
		_url += "&newpwd=" + encodeURI(pwd);
		document.getElementById('_changeMyPassword').value = 'Loading..';
		document.getElementById('_changeMyPassword').disabled = true;
		makePOSTRequest('../servlets/AccountServlet',_url,'changePassword');
	}
	else
	{
		document.getElementById('_passwordError').innerHTML = '<span class="col_red"><strong>New and Repeat password not same</strong></span>';
	}
}
function changePasswordResult()
{
	if (http_post.readyState == 4)
	{
		if (http_post.status == 200)
		{
			document.getElementById('_passwordError').innerHTML = '<span class="col_red"><strong>'+http_post.responseText+'</strong></span>';
			document.getElementById('_changeMyPassword').value = 'Change My Password';
			document.getElementById('_changeMyPassword').disabled = false;
		}
		// else if ( http_post.status == 403 )
		else if ( http_post.status == 404 )
		{
			document.getElementById('_passwordError').innerHTML = '<span class="col_red"><strong>Invalid Old Password</strong></span>';
			document.getElementById('_changeMyPassword').value = 'Change My Password';
			document.getElementById('_changeMyPassword').disabled = false;
		}
		else
		{
		}
	}
}
function changeMyPin()
{
	var old =  document.getElementById("_oldpin").value;
	var pwd =  document.getElementById("_pin").value;
	var rpwd =  document.getElementById("_repeatPin").value;
	var account =  document.getElementById("_account").value;
	if (pwd == rpwd)
	{
		var _url = "_a=chgpin" ;
		_url += "&oldpin=" + encodeURI(old);
		_url += "&newpin=" + encodeURI(pwd);
		_url += "&account=" + encodeURI(account);
		document.getElementById('_changeMyPin').value = 'Loading..';
		document.getElementById('_changeMyPin').disabled = true;
		// makePOSTRequest('../servlets/AccountServlet?_a=chgpin&oldpin=' + old + '&newpin=' + pwd + '&account=' + account,'changePin');
		makePOSTRequest('../servlets/AccountServlet',_url,'changePin');
	}
	else
	{
		document.getElementById('_pinError').innerHTML = '<span class="col_red"><strong>New and Repeat PIN not same</strong></span>';
	}
}
function changePinResult()
{
	if (http_post.readyState == 4)
	{
		if (http_post.status == 200)
		{
			document.getElementById('_pinError').innerHTML = '<span class="col_red"><strong>'+http_post.responseText+'</strong></span>';
			document.getElementById('_changeMyPin').value = 'Change My Pin';
			document.getElementById('_changeMyPin').disabled = false;
		}
		else if ( http_post.status == 403 )
		{
			document.getElementById('_pinError').innerHTML = '<span class="col_red"><strong>Invalid PIN</strong></span>';
			document.getElementById('_changeMyPin').value = 'Change My Pin';
			document.getElementById('_changeMyPin').disabled = false;
		}
		else if ( http_post.status == 404 )
		{
			document.getElementById('_pinError').innerHTML = '<span class="col_red"><strong>Invalid PIN</strong></span>';
			document.getElementById('_changeMyPin').value = 'Change My Pin';
			document.getElementById('_changeMyPin').disabled = false;
		}
		else
		{
		}
	}
}
