<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@include file="../include/header.inc" %>
<%@include file="../include/nocache.inc" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<title>POST - Panin Sekuritas Online Stock Trading</title>
<style type="text/css">
<!--
@import url("../style/style.css");
-->
</style>
<script type="text/javascript" src="../script/menu.js"></script>
<script language="javascript">
<!--
function openNews (obj)
{
	window.open (obj.href,'News','width=400,height=200,scrollbars=yes');
}
// -->
</script>
</head>
<body topmargin="3" leftmargin="0">

<f:view>

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="40">&nbsp;<img src="../images/logo.gif" alt="panin" width="335" height="30" /></td>
  </tr>
  <tr>
    <td height="42" bgcolor="#FFFFFF" style="background:url(../images/menu.jpg) no-repeat center"><div align="center">
    <span class="menu">
		[ &nbsp;
		<a href="#" onClick="loadSubMenu('Home','<%= request.getServerName() %>','<%= request.getContextPath() %>'); return false;">Home</a>&nbsp; | &nbsp;
		<a href="#" onClick="loadSubMenu('Account','<%= request.getServerName() %>','<%= request.getContextPath() %>'); return false;">Account</a>&nbsp; | &nbsp;
		<a href="#" onClick="loadSubMenu('Market Info','<%= request.getServerName() %>','<%= request.getContextPath() %>'); return false;">Market Info</a>&nbsp; | &nbsp;
		<a href="#" onClick="loadSubMenu('Summary','<%= request.getServerName() %>','<%= request.getContextPath() %>'); return false;">Summary</a>&nbsp; | &nbsp;
		<a href="#" onClick="loadSubMenu('Order','<%= request.getServerName() %>','<%= request.getContextPath() %>'); return false;">Order</a>&nbsp; | &nbsp;
		<a href="#" onClick="loadSubMenu('Chart','<%= request.getServerName() %>','<%= request.getContextPath() %>'); return false;">Chart</a>&nbsp; | &nbsp;
		<a href="../index/stock_list.faces" onClick="window.open('../index/stock_list.faces','IDX Stock List','width=335,height=300'); return false;">Stock List</a>&nbsp; | &nbsp;
		<a href="../logout.jsp">Logout</a>&nbsp; ]
    </span></div></td>
  </tr>
  <tr>
    <td><table width="996" border="0" align="center" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td width="172" valign="top" style="padding-top:10px">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><img src="../images/sub_menu_atas.jpg" width="172" height="13" /></td>
            </tr>
            <tr>
              <td class="submenu" style="background:url(../images/sub_menu_tengah.jpg) repeat-y left">
				<div id="tmenu">
                <table width="95%" border="0" cellspacing="4" cellpadding="2">
						<tr>
							<td height="20" bgcolor="#7B99CF">
								<div align="center" class="title_yellow">Home</div>
							</td>
						</tr>
                  <tr><td><img src="../images/point.jpg" align="absmiddle" /> <a href="../index/index.faces">Home</a></td></tr>
                  <tr><td><img src="../images/point.jpg" align="absmiddle" /> <a href="../index/idx_news.faces">IDX News History</a></td></tr>
                  <tr><td><img src="../images/point.jpg" align="absmiddle" /> <a href="../index/antara_news.faces">Antara History</a></td></tr>
                  <tr><td><img src="../images/point.jpg" align="absmiddle" /> <a href="../index/manual_book.pdf">Manual Book</a></td></tr>
                </table>
				</div>
                </td>
            </tr>
            <tr>
              <td><img src="../images/sub_menu_bawah.jpg" width="172" height="13" /></td>
            </tr>
          </table>
        <iframe name="I1" height="300px" width="100%" scrolling="no" border="0" frameborder="0"  src="../quick_order/quick_order.html"> Your browser does not support inline frames or is currently configured not to display inline frames.</iframe></td>
        <td width="808" valign="top" style="padding-top:10px">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td style="padding-left:20px;padding-right:20px">
    <fieldset style="padding:10px;width:750px">
    <legend  style="margin-bottom:5px"><strong>Enter Secret Answer</strong></legend>
		<h:form id="forgetForm_">
			<h:panelGrid
				columns="2"
				styleClass="normalText"
				columnClasses="align-right,align-left"
			>
				<h:outputText value="Trading Account" styleClass="blueText"/>
				<h:selectOneMenu id="tradingAccount" value="#{account}" style="font-family:Tahoma, Verdana, Arial;font-size:11px;font-weight:bold; width:150px;">
					<f:selectItems value="#{passwordBusinessProcess.accountList}" />
				</h:selectOneMenu>

				<h:outputText value="Question" styleClass="blueText"/>
				<h:outputText value="#{user.question}" styleClass="blueText"/>

				<h:outputText value="Answer" styleClass="blueText"/>
				<h:inputText
					value="#{answer}"
					required="true"
					size="15"
					maxlength="25"
					styleClass="formField"
				/>
			</h:panelGrid>
			<t:commandButton
				value="Continue"
				action="#{userBusinessProcess.requestNewPin}"
				styleClass="myButton"
			/>
		</h:form>
    </fieldset>
    </td>
  </tr>
</table>



	    </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="42" style="background:url(../images/menu.jpg) no-repeat center">&nbsp;</td>
  </tr>
</table>

</f:view>

</body>
</html>
